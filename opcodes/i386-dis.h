/* Print i386 instructions for GDB, the GNU debugger.
   Copyright 1988, 1989, 1991, 1993, 1994, 1995, 1996, 1997, 1998, 1999,
   2001, 2002, 2003, 2004, 2005, 2006, 2007, 2008, 2009, 2010
   Free Software Foundation, Inc.

   This file is part of the GNU opcodes library.

   This library is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3, or (at your option)
   any later version.

   It is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 51 Franklin Street - Fifth Floor, Boston,
   MA 02110-1301, USA.  */


/* 80386 instruction printer by Pace Willisson (pace@prep.ai.mit.edu)
   July 1988
    modified by John Hassey (hassey@dg-rtp.dg.com)
    x86-64 support added by Jan Hubicka (jh@suse.cz)
    VIA PadLock support by Michal Ludvig (mludvig@suse.cz).  */

/* The main tables describing the instructions is essentially a copy
   of the "Opcode Map" chapter (Appendix A) of the Intel 80386
   Programmers Manual.  Usually, there is a capital letter, followed
   by a small letter.  The capital letter tell the addressing mode,
   and the small letter tells about the operand size.  Refer to
   the Intel manual for details.  */

#include "sysdep.h"
#include "dis-asm.h"
#include "opintl.h"
#include "opcode/i386.h"
#include "libiberty.h"
#include <setjmp.h>
//#include <iostream>
#include <vector>

namespace binutils_objdump_opcodes_i386
{

int print_insn (bfd_vma, disassemble_info *);
void dofloat (int);
void OP_ST (int, int);
void OP_STi (int, int);
int putop (const char *, int);
void oappend (const char *);
void append_seg (void);
void OP_indirE (int, int);
void print_operand_value (char *, int, bfd_vma);
void OP_E_register (int, int);
void OP_E_memory (int, int);
void print_displacement (char *, bfd_vma);
void OP_E (int, int);
void OP_G (int, int);
bfd_vma get64 (void);
bfd_signed_vma get32 (void);
bfd_signed_vma get32s (void);
int get16 (void);
void set_op (bfd_vma, int);
void OP_Skip_MODRM (int, int);
void OP_REG (int, int);
void OP_IMREG (int, int);
void OP_I (int, int);
void OP_I64 (int, int);
void OP_sI (int, int);
void OP_J (int, int);
void OP_SEG (int, int);
void OP_DIR (int, int);
void OP_OFF (int, int);
void OP_OFF64 (int, int);
void ptr_reg (int, int);
void OP_ESreg (int, int);
void OP_DSreg (int, int);
void OP_C (int, int);
void OP_D (int, int);
void OP_T (int, int);
void OP_R (int, int);
void OP_MMX (int, int);
void OP_XMM (int, int);
void OP_EM (int, int);
void OP_EX (int, int);
void OP_EMC (int,int);
void OP_MXC (int,int);
void OP_MS (int, int);
void OP_XS (int, int);
void OP_M (int, int);
void OP_VEX (int, int);
void OP_EX_Vex (int, int);
void OP_EX_VexW (int, int);
void OP_EX_VexImmW (int, int);
void OP_XMM_Vex (int, int);
void OP_XMM_VexW (int, int);
void OP_REG_VexI4 (int, int);
void PCLMUL_Fixup (int, int);
void VEXI4_Fixup (int, int);
void VZERO_Fixup (int, int);
void VCMP_Fixup (int, int);
void OP_0f07 (int, int);
void OP_Monitor (int, int);
void OP_Mwait (int, int);
void NOP_Fixup1 (int, int);
void NOP_Fixup2 (int, int);
void OP_3DNowSuffix (int, int);
void CMP_Fixup (int, int);
void BadOp (void);
void REP_Fixup (int, int);
void CMPXCHG8B_Fixup (int, int);
void XMM_Fixup (int, int);
void CRC32_Fixup (int, int);
void FXSAVE_Fixup (int, int);
void OP_LWPCB_E (int, int);
void OP_LWP_E (int, int);
void OP_Vex_2src_1 (int, int);
void OP_Vex_2src_2 (int, int);
void MOVBE_Fixup (int, int);

struct dis_private {
  /* Points to first byte not fetched.  */
  bfd_byte *max_fetched;
  bfd_byte the_buffer[MAX_MNEM_SIZE];
  bfd_vma insn_start;
  int orig_sizeflag;
  jmp_buf bailout;
};

typedef enum address_mode
{
  mode_16bit,
  mode_32bit,
  mode_64bit
} t_address_mode;


/* Flags stored in PREFIXES.  */
const int PREFIX_REPZ =1;
const int PREFIX_REPNZ =2;
const int PREFIX_LOCK =4;
const int PREFIX_CS =8;
const int PREFIX_SS =0x10;
const int  PREFIX_DS= 0x20;
const int  PREFIX_ES=0x40;
const int  PREFIX_FS=0x80;
const int  PREFIX_GS=0x100;
const int  PREFIX_DATA=0x200;
const int  PREFIX_ADDR=0x400;
const int  PREFIX_FWAIT=0x800;

typedef enum t_operands
{
  /* byte operand */
  b_mode = 1,
  /* byte operand with operand swapped */
  b_swap_mode,
  /* operand size depends on prefixes */
  v_mode,
  /* operand size depends on prefixes with operand swapped */
  v_swap_mode,
  /* word operand */
  w_mode,
  /* double word operand  */
  d_mode,
  /* double word operand with operand swapped */
  d_swap_mode,
  /* quad word operand */
  q_mode,
  /* quad word operand with operand swapped */
  q_swap_mode,
  /* ten-byte operand */
  t_mode,
  /* 16-byte XMM or 32-byte YMM operand */
  x_mode,
  /* 16-byte XMM or 32-byte YMM operand with operand swapped */
  x_swap_mode,
  /* 16-byte XMM operand */
  xmm_mode,
  /* 16-byte XMM or quad word operand */
  xmmq_mode,
  /* 32-byte YMM or quad word operand */
  ymmq_mode,
  /* d_mode in 32bit, q_mode in 64bit mode.  */
  m_mode,
  /* pair of v_mode operands */
  a_mode,
  cond_jump_mode,
  loop_jcxz_mode,
  /* operand size depends on REX prefixes.  */
  dq_mode,
  /* registers like dq_mode, memory like w_mode.  */
  dqw_mode,
  /* 4- or 6-byte pointer operand */
  f_mode,
  const_1_mode,
  /* v_mode for stack-related opcodes.  */
  stack_v_mode,
  /* non-quad operand size depends on prefixes */
  z_mode,
  /* 16-byte operand */
  o_mode,
  /* registers like dq_mode, memory like b_mode.  */
  dqb_mode,
  /* registers like dq_mode, memory like d_mode.  */
  dqd_mode,
  /* normal vex mode */
  vex_mode,
  /* 128bit vex mode */
  vex128_mode,
  /* 256bit vex mode */
  vex256_mode,
  /* operand size depends on the VEX.W bit.  */
  vex_w_dq_mode,
  /* scalar, ignore vector length.  */
  scalar_mode,
  /* like d_mode, ignore vector length.  */
  d_scalar_mode,
  /* like d_swap_mode, ignore vector length.  */
  d_scalar_swap_mode,
  /* like q_mode, ignore vector length.  */
  q_scalar_mode,
  /* like q_swap_mode, ignore vector length.  */
  q_scalar_swap_mode,
  /* like vex_mode, ignore vector length.  */
  vex_scalar_mode,
  /* like vex_w_dq_mode, ignore vector length.  */
  vex_scalar_w_dq_mode,
  es_reg,
  cs_reg,
  ss_reg,
  ds_reg,
  fs_reg,
  gs_reg,
  eAX_reg,
  eCX_reg,
  eDX_reg,
  eBX_reg,
  eSP_reg,
  eBP_reg,
  eSI_reg,
  eDI_reg,
  al_reg,
  cl_reg,
  dl_reg,
  bl_reg,
  ah_reg,
  ch_reg,
  dh_reg,
  bh_reg,
  ax_reg,
  cx_reg,
  dx_reg,
  bx_reg,
  sp_reg,
  bp_reg,
  si_reg,
  di_reg,
  rAX_reg,
  rCX_reg,
  rDX_reg,
  rBX_reg,
  rSP_reg,
  rBP_reg,
  rSI_reg,
  rDI_reg,
  z_mode_ax_reg,
  indir_dx_reg
} T_operands;

typedef enum t_use_flags
{
  FLOATCODE = 1,
  USE_REG_TABLE,
  USE_MOD_TABLE,
  USE_RM_TABLE,
  USE_PREFIX_TABLE,
  USE_X86_64_TABLE,
  USE_3BYTE_TABLE,
  USE_XOP_8F_TABLE,
  USE_VEX_C4_TABLE,
  USE_VEX_C5_TABLE,
  USE_VEX_LEN_TABLE,
  USE_VEX_W_TABLE
} T_use_flags;

typedef enum t_enums
{
  REG_80 = 0,
  REG_81,
  REG_82,
  REG_8F,
  REG_C0,
  REG_C1,
  REG_C6,
  REG_C7,
  REG_D0,
  REG_D1,
  REG_D2,
  REG_D3,
  REG_F6,
  REG_F7,
  REG_FE,
  REG_FF,
  REG_0F00,
  REG_0F01,
  REG_0F0D,
  REG_0F18,
  REG_0F71,
  REG_0F72,
  REG_0F73,
  REG_0FA6,
  REG_0FA7,
  REG_0FAE,
  REG_0FBA,
  REG_0FC7,
  REG_VEX_0F71,
  REG_VEX_0F72,
  REG_VEX_0F73,
  REG_VEX_0FAE,
  REG_XOP_LWPCB,
  REG_XOP_LWP
} T_enums;

typedef enum t_mod_reg
{
  MOD_8D = 0,
  MOD_0F01_REG_0,
  MOD_0F01_REG_1,
  MOD_0F01_REG_2,
  MOD_0F01_REG_3,
  MOD_0F01_REG_7,
  MOD_0F12_PREFIX_0,
  MOD_0F13,
  MOD_0F16_PREFIX_0,
  MOD_0F17,
  MOD_0F18_REG_0,
  MOD_0F18_REG_1,
  MOD_0F18_REG_2,
  MOD_0F18_REG_3,
  MOD_0F20,
  MOD_0F21,
  MOD_0F22,
  MOD_0F23,
  MOD_0F24,
  MOD_0F26,
  MOD_0F2B_PREFIX_0,
  MOD_0F2B_PREFIX_1,
  MOD_0F2B_PREFIX_2,
  MOD_0F2B_PREFIX_3,
  MOD_0F51,
  MOD_0F71_REG_2,
  MOD_0F71_REG_4,
  MOD_0F71_REG_6,
  MOD_0F72_REG_2,
  MOD_0F72_REG_4,
  MOD_0F72_REG_6,
  MOD_0F73_REG_2,
  MOD_0F73_REG_3,
  MOD_0F73_REG_6,
  MOD_0F73_REG_7,
  MOD_0FAE_REG_0,
  MOD_0FAE_REG_1,
  MOD_0FAE_REG_2,
  MOD_0FAE_REG_3,
  MOD_0FAE_REG_4,
  MOD_0FAE_REG_5,
  MOD_0FAE_REG_6,
  MOD_0FAE_REG_7,
  MOD_0FB2,
  MOD_0FB4,
  MOD_0FB5,
  MOD_0FC7_REG_6,
  MOD_0FC7_REG_7,
  MOD_0FD7,
  MOD_0FE7_PREFIX_2,
  MOD_0FF0_PREFIX_3,
  MOD_0F382A_PREFIX_2,
  MOD_62_32BIT,
  MOD_C4_32BIT,
  MOD_C5_32BIT,
  MOD_VEX_0F12_PREFIX_0,
  MOD_VEX_0F13,
  MOD_VEX_0F16_PREFIX_0,
  MOD_VEX_0F17,
  MOD_VEX_0F2B,
  MOD_VEX_0F50,
  MOD_VEX_0F71_REG_2,
  MOD_VEX_0F71_REG_4,
  MOD_VEX_0F71_REG_6,
  MOD_VEX_0F72_REG_2,
  MOD_VEX_0F72_REG_4,
  MOD_VEX_0F72_REG_6,
  MOD_VEX_0F73_REG_2,
  MOD_VEX_0F73_REG_3,
  MOD_VEX_0F73_REG_6,
  MOD_VEX_0F73_REG_7,
  MOD_VEX_0FAE_REG_2,
  MOD_VEX_0FAE_REG_3,
  MOD_VEX_0FD7_PREFIX_2,
  MOD_VEX_0FE7_PREFIX_2,
  MOD_VEX_0FF0_PREFIX_3,
  MOD_VEX_0F3818_PREFIX_2,
  MOD_VEX_0F3819_PREFIX_2,
  MOD_VEX_0F381A_PREFIX_2,
  MOD_VEX_0F382A_PREFIX_2,
  MOD_VEX_0F382C_PREFIX_2,
  MOD_VEX_0F382D_PREFIX_2,
  MOD_VEX_0F382E_PREFIX_2,
  MOD_VEX_0F382F_PREFIX_2
} T_mod_reg;

typedef enum t_REG_stuff
{
  RM_0F01_REG_0 = 0,
  RM_0F01_REG_1,
  RM_0F01_REG_2,
  RM_0F01_REG_3,
  RM_0F01_REG_7,
  RM_0FAE_REG_5,
  RM_0FAE_REG_6,
  RM_0FAE_REG_7
} T_REG_stuff;

typedef enum t_prefix
{
  PREFIX_90 = 0,
  PREFIX_0F10,
  PREFIX_0F11,
  PREFIX_0F12,
  PREFIX_0F16,
  PREFIX_0F2A,
  PREFIX_0F2B,
  PREFIX_0F2C,
  PREFIX_0F2D,
  PREFIX_0F2E,
  PREFIX_0F2F,
  PREFIX_0F51,
  PREFIX_0F52,
  PREFIX_0F53,
  PREFIX_0F58,
  PREFIX_0F59,
  PREFIX_0F5A,
  PREFIX_0F5B,
  PREFIX_0F5C,
  PREFIX_0F5D,
  PREFIX_0F5E,
  PREFIX_0F5F,
  PREFIX_0F60,
  PREFIX_0F61,
  PREFIX_0F62,
  PREFIX_0F6C,
  PREFIX_0F6D,
  PREFIX_0F6F,
  PREFIX_0F70,
  PREFIX_0F73_REG_3,
  PREFIX_0F73_REG_7,
  PREFIX_0F78,
  PREFIX_0F79,
  PREFIX_0F7C,
  PREFIX_0F7D,
  PREFIX_0F7E,
  PREFIX_0F7F,
  PREFIX_0FAE_REG_0,
  PREFIX_0FAE_REG_1,
  PREFIX_0FAE_REG_2,
  PREFIX_0FAE_REG_3,
  PREFIX_0FB8,
  PREFIX_0FBD,
  PREFIX_0FC2,
  PREFIX_0FC3,
  PREFIX_0FC7_REG_6,
  PREFIX_0FD0,
  PREFIX_0FD6,
  PREFIX_0FE6,
  PREFIX_0FE7,
  PREFIX_0FF0,
  PREFIX_0FF7,
  PREFIX_0F3810,
  PREFIX_0F3814,
  PREFIX_0F3815,
  PREFIX_0F3817,
  PREFIX_0F3820,
  PREFIX_0F3821,
  PREFIX_0F3822,
  PREFIX_0F3823,
  PREFIX_0F3824,
  PREFIX_0F3825,
  PREFIX_0F3828,
  PREFIX_0F3829,
  PREFIX_0F382A,
  PREFIX_0F382B,
  PREFIX_0F3830,
  PREFIX_0F3831,
  PREFIX_0F3832,
  PREFIX_0F3833,
  PREFIX_0F3834,
  PREFIX_0F3835,
  PREFIX_0F3837,
  PREFIX_0F3838,
  PREFIX_0F3839,
  PREFIX_0F383A,
  PREFIX_0F383B,
  PREFIX_0F383C,
  PREFIX_0F383D,
  PREFIX_0F383E,
  PREFIX_0F383F,
  PREFIX_0F3840,
  PREFIX_0F3841,
  PREFIX_0F3880,
  PREFIX_0F3881,
  PREFIX_0F38DB,
  PREFIX_0F38DC,
  PREFIX_0F38DD,
  PREFIX_0F38DE,
  PREFIX_0F38DF,
  PREFIX_0F38F0,
  PREFIX_0F38F1,
  PREFIX_0F3A08,
  PREFIX_0F3A09,
  PREFIX_0F3A0A,
  PREFIX_0F3A0B,
  PREFIX_0F3A0C,
  PREFIX_0F3A0D,
  PREFIX_0F3A0E,
  PREFIX_0F3A14,
  PREFIX_0F3A15,
  PREFIX_0F3A16,
  PREFIX_0F3A17,
  PREFIX_0F3A20,
  PREFIX_0F3A21,
  PREFIX_0F3A22,
  PREFIX_0F3A40,
  PREFIX_0F3A41,
  PREFIX_0F3A42,
  PREFIX_0F3A44,
  PREFIX_0F3A60,
  PREFIX_0F3A61,
  PREFIX_0F3A62,
  PREFIX_0F3A63,
  PREFIX_0F3ADF,
  PREFIX_VEX_0F10,
  PREFIX_VEX_0F11,
  PREFIX_VEX_0F12,
  PREFIX_VEX_0F16,
  PREFIX_VEX_0F2A,
  PREFIX_VEX_0F2C,
  PREFIX_VEX_0F2D,
  PREFIX_VEX_0F2E,
  PREFIX_VEX_0F2F,
  PREFIX_VEX_0F51,
  PREFIX_VEX_0F52,
  PREFIX_VEX_0F53,
  PREFIX_VEX_0F58,
  PREFIX_VEX_0F59,
  PREFIX_VEX_0F5A,
  PREFIX_VEX_0F5B,
  PREFIX_VEX_0F5C,
  PREFIX_VEX_0F5D,
  PREFIX_VEX_0F5E,
  PREFIX_VEX_0F5F,
  PREFIX_VEX_0F60,
  PREFIX_VEX_0F61,
  PREFIX_VEX_0F62,
  PREFIX_VEX_0F63,
  PREFIX_VEX_0F64,
  PREFIX_VEX_0F65,
  PREFIX_VEX_0F66,
  PREFIX_VEX_0F67,
  PREFIX_VEX_0F68,
  PREFIX_VEX_0F69,
  PREFIX_VEX_0F6A,
  PREFIX_VEX_0F6B,
  PREFIX_VEX_0F6C,
  PREFIX_VEX_0F6D,
  PREFIX_VEX_0F6E,
  PREFIX_VEX_0F6F,
  PREFIX_VEX_0F70,
  PREFIX_VEX_0F71_REG_2,
  PREFIX_VEX_0F71_REG_4,
  PREFIX_VEX_0F71_REG_6,
  PREFIX_VEX_0F72_REG_2,
  PREFIX_VEX_0F72_REG_4,
  PREFIX_VEX_0F72_REG_6,
  PREFIX_VEX_0F73_REG_2,
  PREFIX_VEX_0F73_REG_3,
  PREFIX_VEX_0F73_REG_6,
  PREFIX_VEX_0F73_REG_7,
  PREFIX_VEX_0F74,
  PREFIX_VEX_0F75,
  PREFIX_VEX_0F76,
  PREFIX_VEX_0F77,
  PREFIX_VEX_0F7C,
  PREFIX_VEX_0F7D,
  PREFIX_VEX_0F7E,
  PREFIX_VEX_0F7F,
  PREFIX_VEX_0FC2,
  PREFIX_VEX_0FC4,
  PREFIX_VEX_0FC5,
  PREFIX_VEX_0FD0,
  PREFIX_VEX_0FD1,
  PREFIX_VEX_0FD2,
  PREFIX_VEX_0FD3,
  PREFIX_VEX_0FD4,
  PREFIX_VEX_0FD5,
  PREFIX_VEX_0FD6,
  PREFIX_VEX_0FD7,
  PREFIX_VEX_0FD8,
  PREFIX_VEX_0FD9,
  PREFIX_VEX_0FDA,
  PREFIX_VEX_0FDB,
  PREFIX_VEX_0FDC,
  PREFIX_VEX_0FDD,
  PREFIX_VEX_0FDE,
  PREFIX_VEX_0FDF,
  PREFIX_VEX_0FE0,
  PREFIX_VEX_0FE1,
  PREFIX_VEX_0FE2,
  PREFIX_VEX_0FE3,
  PREFIX_VEX_0FE4,
  PREFIX_VEX_0FE5,
  PREFIX_VEX_0FE6,
  PREFIX_VEX_0FE7,
  PREFIX_VEX_0FE8,
  PREFIX_VEX_0FE9,
  PREFIX_VEX_0FEA,
  PREFIX_VEX_0FEB,
  PREFIX_VEX_0FEC,
  PREFIX_VEX_0FED,
  PREFIX_VEX_0FEE,
  PREFIX_VEX_0FEF,
  PREFIX_VEX_0FF0,
  PREFIX_VEX_0FF1,
  PREFIX_VEX_0FF2,
  PREFIX_VEX_0FF3,
  PREFIX_VEX_0FF4,
  PREFIX_VEX_0FF5,
  PREFIX_VEX_0FF6,
  PREFIX_VEX_0FF7,
  PREFIX_VEX_0FF8,
  PREFIX_VEX_0FF9,
  PREFIX_VEX_0FFA,
  PREFIX_VEX_0FFB,
  PREFIX_VEX_0FFC,
  PREFIX_VEX_0FFD,
  PREFIX_VEX_0FFE,
  PREFIX_VEX_0F3800,
  PREFIX_VEX_0F3801,
  PREFIX_VEX_0F3802,
  PREFIX_VEX_0F3803,
  PREFIX_VEX_0F3804,
  PREFIX_VEX_0F3805,
  PREFIX_VEX_0F3806,
  PREFIX_VEX_0F3807,
  PREFIX_VEX_0F3808,
  PREFIX_VEX_0F3809,
  PREFIX_VEX_0F380A,
  PREFIX_VEX_0F380B,
  PREFIX_VEX_0F380C,
  PREFIX_VEX_0F380D,
  PREFIX_VEX_0F380E,
  PREFIX_VEX_0F380F,
  PREFIX_VEX_0F3813,
  PREFIX_VEX_0F3817,
  PREFIX_VEX_0F3818,
  PREFIX_VEX_0F3819,
  PREFIX_VEX_0F381A,
  PREFIX_VEX_0F381C,
  PREFIX_VEX_0F381D,
  PREFIX_VEX_0F381E,
  PREFIX_VEX_0F3820,
  PREFIX_VEX_0F3821,
  PREFIX_VEX_0F3822,
  PREFIX_VEX_0F3823,
  PREFIX_VEX_0F3824,
  PREFIX_VEX_0F3825,
  PREFIX_VEX_0F3828,
  PREFIX_VEX_0F3829,
  PREFIX_VEX_0F382A,
  PREFIX_VEX_0F382B,
  PREFIX_VEX_0F382C,
  PREFIX_VEX_0F382D,
  PREFIX_VEX_0F382E,
  PREFIX_VEX_0F382F,
  PREFIX_VEX_0F3830,
  PREFIX_VEX_0F3831,
  PREFIX_VEX_0F3832,
  PREFIX_VEX_0F3833,
  PREFIX_VEX_0F3834,
  PREFIX_VEX_0F3835,
  PREFIX_VEX_0F3837,
  PREFIX_VEX_0F3838,
  PREFIX_VEX_0F3839,
  PREFIX_VEX_0F383A,
  PREFIX_VEX_0F383B,
  PREFIX_VEX_0F383C,
  PREFIX_VEX_0F383D,
  PREFIX_VEX_0F383E,
  PREFIX_VEX_0F383F,
  PREFIX_VEX_0F3840,
  PREFIX_VEX_0F3841,
  PREFIX_VEX_0F3896,
  PREFIX_VEX_0F3897,
  PREFIX_VEX_0F3898,
  PREFIX_VEX_0F3899,
  PREFIX_VEX_0F389A,
  PREFIX_VEX_0F389B,
  PREFIX_VEX_0F389C,
  PREFIX_VEX_0F389D,
  PREFIX_VEX_0F389E,
  PREFIX_VEX_0F389F,
  PREFIX_VEX_0F38A6,
  PREFIX_VEX_0F38A7,
  PREFIX_VEX_0F38A8,
  PREFIX_VEX_0F38A9,
  PREFIX_VEX_0F38AA,
  PREFIX_VEX_0F38AB,
  PREFIX_VEX_0F38AC,
  PREFIX_VEX_0F38AD,
  PREFIX_VEX_0F38AE,
  PREFIX_VEX_0F38AF,
  PREFIX_VEX_0F38B6,
  PREFIX_VEX_0F38B7,
  PREFIX_VEX_0F38B8,
  PREFIX_VEX_0F38B9,
  PREFIX_VEX_0F38BA,
  PREFIX_VEX_0F38BB,
  PREFIX_VEX_0F38BC,
  PREFIX_VEX_0F38BD,
  PREFIX_VEX_0F38BE,
  PREFIX_VEX_0F38BF,
  PREFIX_VEX_0F38DB,
  PREFIX_VEX_0F38DC,
  PREFIX_VEX_0F38DD,
  PREFIX_VEX_0F38DE,
  PREFIX_VEX_0F38DF,
  PREFIX_VEX_0F3A04,
  PREFIX_VEX_0F3A05,
  PREFIX_VEX_0F3A06,
  PREFIX_VEX_0F3A08,
  PREFIX_VEX_0F3A09,
  PREFIX_VEX_0F3A0A,
  PREFIX_VEX_0F3A0B,
  PREFIX_VEX_0F3A0C,
  PREFIX_VEX_0F3A0D,
  PREFIX_VEX_0F3A0E,
  PREFIX_VEX_0F3A0F,
  PREFIX_VEX_0F3A14,
  PREFIX_VEX_0F3A15,
  PREFIX_VEX_0F3A16,
  PREFIX_VEX_0F3A17,
  PREFIX_VEX_0F3A18,
  PREFIX_VEX_0F3A19,
  PREFIX_VEX_0F3A1D,
  PREFIX_VEX_0F3A20,
  PREFIX_VEX_0F3A21,
  PREFIX_VEX_0F3A22,
  PREFIX_VEX_0F3A40,
  PREFIX_VEX_0F3A41,
  PREFIX_VEX_0F3A42,
  PREFIX_VEX_0F3A44,
  PREFIX_VEX_0F3A48,
  PREFIX_VEX_0F3A49,
  PREFIX_VEX_0F3A4A,
  PREFIX_VEX_0F3A4B,
  PREFIX_VEX_0F3A4C,
  PREFIX_VEX_0F3A5C,
  PREFIX_VEX_0F3A5D,
  PREFIX_VEX_0F3A5E,
  PREFIX_VEX_0F3A5F,
  PREFIX_VEX_0F3A60,
  PREFIX_VEX_0F3A61,
  PREFIX_VEX_0F3A62,
  PREFIX_VEX_0F3A63,
  PREFIX_VEX_0F3A68,
  PREFIX_VEX_0F3A69,
  PREFIX_VEX_0F3A6A,
  PREFIX_VEX_0F3A6B,
  PREFIX_VEX_0F3A6C,
  PREFIX_VEX_0F3A6D,
  PREFIX_VEX_0F3A6E,
  PREFIX_VEX_0F3A6F,
  PREFIX_VEX_0F3A78,
  PREFIX_VEX_0F3A79,
  PREFIX_VEX_0F3A7A,
  PREFIX_VEX_0F3A7B,
  PREFIX_VEX_0F3A7C,
  PREFIX_VEX_0F3A7D,
  PREFIX_VEX_0F3A7E,
  PREFIX_VEX_0F3A7F,
  PREFIX_VEX_0F3ADF
} T_prefix;

typedef enum t_X86_64_flags
{
  X86_64_06 = 0,
  X86_64_07,
  X86_64_0D,
  X86_64_16,
  X86_64_17,
  X86_64_1E,
  X86_64_1F,
  X86_64_27,
  X86_64_2F,
  X86_64_37,
  X86_64_3F,
  X86_64_60,
  X86_64_61,
  X86_64_62,
  X86_64_63,
  X86_64_6D,
  X86_64_6F,
  X86_64_9A,
  X86_64_C4,
  X86_64_C5,
  X86_64_CE,
  X86_64_D4,
  X86_64_D5,
  X86_64_EA,
  X86_64_0F01_REG_0,
  X86_64_0F01_REG_1,
  X86_64_0F01_REG_2,
  X86_64_0F01_REG_3
} R_X86_64_flags;

typedef enum  t_threebyte_enum
{
  THREE_BYTE_0F38 = 0,
  THREE_BYTE_0F3A,
  THREE_BYTE_0F7A
} T_threebyte_enum;

typedef enum t_xop_enum
{
  XOP_08 = 0,
  XOP_09,
  XOP_0A
} T_xop_enum;

typedef enum t_vex_enum
{
  VEX_0F = 0,
  VEX_0F38,
  VEX_0F3A
} T_vex_enum;

typedef enum t_vex_len_enum
{
  VEX_LEN_0F10_P_1 = 0,
  VEX_LEN_0F10_P_3,
  VEX_LEN_0F11_P_1,
  VEX_LEN_0F11_P_3,
  VEX_LEN_0F12_P_0_M_0,
  VEX_LEN_0F12_P_0_M_1,
  VEX_LEN_0F12_P_2,
  VEX_LEN_0F13_M_0,
  VEX_LEN_0F16_P_0_M_0,
  VEX_LEN_0F16_P_0_M_1,
  VEX_LEN_0F16_P_2,
  VEX_LEN_0F17_M_0,
  VEX_LEN_0F2A_P_1,
  VEX_LEN_0F2A_P_3,
  VEX_LEN_0F2C_P_1,
  VEX_LEN_0F2C_P_3,
  VEX_LEN_0F2D_P_1,
  VEX_LEN_0F2D_P_3,
  VEX_LEN_0F2E_P_0,
  VEX_LEN_0F2E_P_2,
  VEX_LEN_0F2F_P_0,
  VEX_LEN_0F2F_P_2,
  VEX_LEN_0F51_P_1,
  VEX_LEN_0F51_P_3,
  VEX_LEN_0F52_P_1,
  VEX_LEN_0F53_P_1,
  VEX_LEN_0F58_P_1,
  VEX_LEN_0F58_P_3,
  VEX_LEN_0F59_P_1,
  VEX_LEN_0F59_P_3,
  VEX_LEN_0F5A_P_1,
  VEX_LEN_0F5A_P_3,
  VEX_LEN_0F5C_P_1,
  VEX_LEN_0F5C_P_3,
  VEX_LEN_0F5D_P_1,
  VEX_LEN_0F5D_P_3,
  VEX_LEN_0F5E_P_1,
  VEX_LEN_0F5E_P_3,
  VEX_LEN_0F5F_P_1,
  VEX_LEN_0F5F_P_3,
  VEX_LEN_0F60_P_2,
  VEX_LEN_0F61_P_2,
  VEX_LEN_0F62_P_2,
  VEX_LEN_0F63_P_2,
  VEX_LEN_0F64_P_2,
  VEX_LEN_0F65_P_2,
  VEX_LEN_0F66_P_2,
  VEX_LEN_0F67_P_2,
  VEX_LEN_0F68_P_2,
  VEX_LEN_0F69_P_2,
  VEX_LEN_0F6A_P_2,
  VEX_LEN_0F6B_P_2,
  VEX_LEN_0F6C_P_2,
  VEX_LEN_0F6D_P_2,
  VEX_LEN_0F6E_P_2,
  VEX_LEN_0F70_P_1,
  VEX_LEN_0F70_P_2,
  VEX_LEN_0F70_P_3,
  VEX_LEN_0F71_R_2_P_2,
  VEX_LEN_0F71_R_4_P_2,
  VEX_LEN_0F71_R_6_P_2,
  VEX_LEN_0F72_R_2_P_2,
  VEX_LEN_0F72_R_4_P_2,
  VEX_LEN_0F72_R_6_P_2,
  VEX_LEN_0F73_R_2_P_2,
  VEX_LEN_0F73_R_3_P_2,
  VEX_LEN_0F73_R_6_P_2,
  VEX_LEN_0F73_R_7_P_2,
  VEX_LEN_0F74_P_2,
  VEX_LEN_0F75_P_2,
  VEX_LEN_0F76_P_2,
  VEX_LEN_0F7E_P_1,
  VEX_LEN_0F7E_P_2,
  VEX_LEN_0FAE_R_2_M_0,
  VEX_LEN_0FAE_R_3_M_0,
  VEX_LEN_0FC2_P_1,
  VEX_LEN_0FC2_P_3,
  VEX_LEN_0FC4_P_2,
  VEX_LEN_0FC5_P_2,
  VEX_LEN_0FD1_P_2,
  VEX_LEN_0FD2_P_2,
  VEX_LEN_0FD3_P_2,
  VEX_LEN_0FD4_P_2,
  VEX_LEN_0FD5_P_2,
  VEX_LEN_0FD6_P_2,
  VEX_LEN_0FD7_P_2_M_1,
  VEX_LEN_0FD8_P_2,
  VEX_LEN_0FD9_P_2,
  VEX_LEN_0FDA_P_2,
  VEX_LEN_0FDB_P_2,
  VEX_LEN_0FDC_P_2,
  VEX_LEN_0FDD_P_2,
  VEX_LEN_0FDE_P_2,
  VEX_LEN_0FDF_P_2,
  VEX_LEN_0FE0_P_2,
  VEX_LEN_0FE1_P_2,
  VEX_LEN_0FE2_P_2,
  VEX_LEN_0FE3_P_2,
  VEX_LEN_0FE4_P_2,
  VEX_LEN_0FE5_P_2,
  VEX_LEN_0FE8_P_2,
  VEX_LEN_0FE9_P_2,
  VEX_LEN_0FEA_P_2,
  VEX_LEN_0FEB_P_2,
  VEX_LEN_0FEC_P_2,
  VEX_LEN_0FED_P_2,
  VEX_LEN_0FEE_P_2,
  VEX_LEN_0FEF_P_2,
  VEX_LEN_0FF1_P_2,
  VEX_LEN_0FF2_P_2,
  VEX_LEN_0FF3_P_2,
  VEX_LEN_0FF4_P_2,
  VEX_LEN_0FF5_P_2,
  VEX_LEN_0FF6_P_2,
  VEX_LEN_0FF7_P_2,
  VEX_LEN_0FF8_P_2,
  VEX_LEN_0FF9_P_2,
  VEX_LEN_0FFA_P_2,
  VEX_LEN_0FFB_P_2,
  VEX_LEN_0FFC_P_2,
  VEX_LEN_0FFD_P_2,
  VEX_LEN_0FFE_P_2,
  VEX_LEN_0F3800_P_2,
  VEX_LEN_0F3801_P_2,
  VEX_LEN_0F3802_P_2,
  VEX_LEN_0F3803_P_2,
  VEX_LEN_0F3804_P_2,
  VEX_LEN_0F3805_P_2,
  VEX_LEN_0F3806_P_2,
  VEX_LEN_0F3807_P_2,
  VEX_LEN_0F3808_P_2,
  VEX_LEN_0F3809_P_2,
  VEX_LEN_0F380A_P_2,
  VEX_LEN_0F380B_P_2,
  VEX_LEN_0F3819_P_2_M_0,
  VEX_LEN_0F381A_P_2_M_0,
  VEX_LEN_0F381C_P_2,
  VEX_LEN_0F381D_P_2,
  VEX_LEN_0F381E_P_2,
  VEX_LEN_0F3820_P_2,
  VEX_LEN_0F3821_P_2,
  VEX_LEN_0F3822_P_2,
  VEX_LEN_0F3823_P_2,
  VEX_LEN_0F3824_P_2,
  VEX_LEN_0F3825_P_2,
  VEX_LEN_0F3828_P_2,
  VEX_LEN_0F3829_P_2,
  VEX_LEN_0F382A_P_2_M_0,
  VEX_LEN_0F382B_P_2,
  VEX_LEN_0F3830_P_2,
  VEX_LEN_0F3831_P_2,
  VEX_LEN_0F3832_P_2,
  VEX_LEN_0F3833_P_2,
  VEX_LEN_0F3834_P_2,
  VEX_LEN_0F3835_P_2,
  VEX_LEN_0F3837_P_2,
  VEX_LEN_0F3838_P_2,
  VEX_LEN_0F3839_P_2,
  VEX_LEN_0F383A_P_2,
  VEX_LEN_0F383B_P_2,
  VEX_LEN_0F383C_P_2,
  VEX_LEN_0F383D_P_2,
  VEX_LEN_0F383E_P_2,
  VEX_LEN_0F383F_P_2,
  VEX_LEN_0F3840_P_2,
  VEX_LEN_0F3841_P_2,
  VEX_LEN_0F38DB_P_2,
  VEX_LEN_0F38DC_P_2,
  VEX_LEN_0F38DD_P_2,
  VEX_LEN_0F38DE_P_2,
  VEX_LEN_0F38DF_P_2,
  VEX_LEN_0F3A06_P_2,
  VEX_LEN_0F3A0A_P_2,
  VEX_LEN_0F3A0B_P_2,
  VEX_LEN_0F3A0E_P_2,
  VEX_LEN_0F3A0F_P_2,
  VEX_LEN_0F3A14_P_2,
  VEX_LEN_0F3A15_P_2,
  VEX_LEN_0F3A16_P_2,
  VEX_LEN_0F3A17_P_2,
  VEX_LEN_0F3A18_P_2,
  VEX_LEN_0F3A19_P_2,
  VEX_LEN_0F3A20_P_2,
  VEX_LEN_0F3A21_P_2,
  VEX_LEN_0F3A22_P_2,
  VEX_LEN_0F3A41_P_2,
  VEX_LEN_0F3A42_P_2,
  VEX_LEN_0F3A44_P_2,
  VEX_LEN_0F3A4C_P_2,
  VEX_LEN_0F3A60_P_2,
  VEX_LEN_0F3A61_P_2,
  VEX_LEN_0F3A62_P_2,
  VEX_LEN_0F3A63_P_2,
  VEX_LEN_0F3A6A_P_2,
  VEX_LEN_0F3A6B_P_2,
  VEX_LEN_0F3A6E_P_2,
  VEX_LEN_0F3A6F_P_2,
  VEX_LEN_0F3A7A_P_2,
  VEX_LEN_0F3A7B_P_2,
  VEX_LEN_0F3A7E_P_2,
  VEX_LEN_0F3A7F_P_2,
  VEX_LEN_0F3ADF_P_2,
  VEX_LEN_0FXOP_09_80,
  VEX_LEN_0FXOP_09_81
} T_vex_len_enum;

typedef enum t_VEX_W_enum
{
  VEX_W_0F10_P_0 = 0,
  VEX_W_0F10_P_1,
  VEX_W_0F10_P_2,
  VEX_W_0F10_P_3,
  VEX_W_0F11_P_0,
  VEX_W_0F11_P_1,
  VEX_W_0F11_P_2,
  VEX_W_0F11_P_3,
  VEX_W_0F12_P_0_M_0,
  VEX_W_0F12_P_0_M_1,
  VEX_W_0F12_P_1,
  VEX_W_0F12_P_2,
  VEX_W_0F12_P_3,
  VEX_W_0F13_M_0,
  VEX_W_0F14,
  VEX_W_0F15,
  VEX_W_0F16_P_0_M_0,
  VEX_W_0F16_P_0_M_1,
  VEX_W_0F16_P_1,
  VEX_W_0F16_P_2,
  VEX_W_0F17_M_0,
  VEX_W_0F28,
  VEX_W_0F29,
  VEX_W_0F2B_M_0,
  VEX_W_0F2E_P_0,
  VEX_W_0F2E_P_2,
  VEX_W_0F2F_P_0,
  VEX_W_0F2F_P_2,
  VEX_W_0F50_M_0,
  VEX_W_0F51_P_0,
  VEX_W_0F51_P_1,
  VEX_W_0F51_P_2,
  VEX_W_0F51_P_3,
  VEX_W_0F52_P_0,
  VEX_W_0F52_P_1,
  VEX_W_0F53_P_0,
  VEX_W_0F53_P_1,
  VEX_W_0F58_P_0,
  VEX_W_0F58_P_1,
  VEX_W_0F58_P_2,
  VEX_W_0F58_P_3,
  VEX_W_0F59_P_0,
  VEX_W_0F59_P_1,
  VEX_W_0F59_P_2,
  VEX_W_0F59_P_3,
  VEX_W_0F5A_P_0,
  VEX_W_0F5A_P_1,
  VEX_W_0F5A_P_3,
  VEX_W_0F5B_P_0,
  VEX_W_0F5B_P_1,
  VEX_W_0F5B_P_2,
  VEX_W_0F5C_P_0,
  VEX_W_0F5C_P_1,
  VEX_W_0F5C_P_2,
  VEX_W_0F5C_P_3,
  VEX_W_0F5D_P_0,
  VEX_W_0F5D_P_1,
  VEX_W_0F5D_P_2,
  VEX_W_0F5D_P_3,
  VEX_W_0F5E_P_0,
  VEX_W_0F5E_P_1,
  VEX_W_0F5E_P_2,
  VEX_W_0F5E_P_3,
  VEX_W_0F5F_P_0,
  VEX_W_0F5F_P_1,
  VEX_W_0F5F_P_2,
  VEX_W_0F5F_P_3,
  VEX_W_0F60_P_2,
  VEX_W_0F61_P_2,
  VEX_W_0F62_P_2,
  VEX_W_0F63_P_2,
  VEX_W_0F64_P_2,
  VEX_W_0F65_P_2,
  VEX_W_0F66_P_2,
  VEX_W_0F67_P_2,
  VEX_W_0F68_P_2,
  VEX_W_0F69_P_2,
  VEX_W_0F6A_P_2,
  VEX_W_0F6B_P_2,
  VEX_W_0F6C_P_2,
  VEX_W_0F6D_P_2,
  VEX_W_0F6F_P_1,
  VEX_W_0F6F_P_2,
  VEX_W_0F70_P_1,
  VEX_W_0F70_P_2,
  VEX_W_0F70_P_3,
  VEX_W_0F71_R_2_P_2,
  VEX_W_0F71_R_4_P_2,
  VEX_W_0F71_R_6_P_2,
  VEX_W_0F72_R_2_P_2,
  VEX_W_0F72_R_4_P_2,
  VEX_W_0F72_R_6_P_2,
  VEX_W_0F73_R_2_P_2,
  VEX_W_0F73_R_3_P_2,
  VEX_W_0F73_R_6_P_2,
  VEX_W_0F73_R_7_P_2,
  VEX_W_0F74_P_2,
  VEX_W_0F75_P_2,
  VEX_W_0F76_P_2,
  VEX_W_0F77_P_0,
  VEX_W_0F7C_P_2,
  VEX_W_0F7C_P_3,
  VEX_W_0F7D_P_2,
  VEX_W_0F7D_P_3,
  VEX_W_0F7E_P_1,
  VEX_W_0F7F_P_1,
  VEX_W_0F7F_P_2,
  VEX_W_0FAE_R_2_M_0,
  VEX_W_0FAE_R_3_M_0,
  VEX_W_0FC2_P_0,
  VEX_W_0FC2_P_1,
  VEX_W_0FC2_P_2,
  VEX_W_0FC2_P_3,
  VEX_W_0FC4_P_2,
  VEX_W_0FC5_P_2,
  VEX_W_0FD0_P_2,
  VEX_W_0FD0_P_3,
  VEX_W_0FD1_P_2,
  VEX_W_0FD2_P_2,
  VEX_W_0FD3_P_2,
  VEX_W_0FD4_P_2,
  VEX_W_0FD5_P_2,
  VEX_W_0FD6_P_2,
  VEX_W_0FD7_P_2_M_1,
  VEX_W_0FD8_P_2,
  VEX_W_0FD9_P_2,
  VEX_W_0FDA_P_2,
  VEX_W_0FDB_P_2,
  VEX_W_0FDC_P_2,
  VEX_W_0FDD_P_2,
  VEX_W_0FDE_P_2,
  VEX_W_0FDF_P_2,
  VEX_W_0FE0_P_2,
  VEX_W_0FE1_P_2,
  VEX_W_0FE2_P_2,
  VEX_W_0FE3_P_2,
  VEX_W_0FE4_P_2,
  VEX_W_0FE5_P_2,
  VEX_W_0FE6_P_1,
  VEX_W_0FE6_P_2,
  VEX_W_0FE6_P_3,
  VEX_W_0FE7_P_2_M_0,
  VEX_W_0FE8_P_2,
  VEX_W_0FE9_P_2,
  VEX_W_0FEA_P_2,
  VEX_W_0FEB_P_2,
  VEX_W_0FEC_P_2,
  VEX_W_0FED_P_2,
  VEX_W_0FEE_P_2,
  VEX_W_0FEF_P_2,
  VEX_W_0FF0_P_3_M_0,
  VEX_W_0FF1_P_2,
  VEX_W_0FF2_P_2,
  VEX_W_0FF3_P_2,
  VEX_W_0FF4_P_2,
  VEX_W_0FF5_P_2,
  VEX_W_0FF6_P_2,
  VEX_W_0FF7_P_2,
  VEX_W_0FF8_P_2,
  VEX_W_0FF9_P_2,
  VEX_W_0FFA_P_2,
  VEX_W_0FFB_P_2,
  VEX_W_0FFC_P_2,
  VEX_W_0FFD_P_2,
  VEX_W_0FFE_P_2,
  VEX_W_0F3800_P_2,
  VEX_W_0F3801_P_2,
  VEX_W_0F3802_P_2,
  VEX_W_0F3803_P_2,
  VEX_W_0F3804_P_2,
  VEX_W_0F3805_P_2,
  VEX_W_0F3806_P_2,
  VEX_W_0F3807_P_2,
  VEX_W_0F3808_P_2,
  VEX_W_0F3809_P_2,
  VEX_W_0F380A_P_2,
  VEX_W_0F380B_P_2,
  VEX_W_0F380C_P_2,
  VEX_W_0F380D_P_2,
  VEX_W_0F380E_P_2,
  VEX_W_0F380F_P_2,
  VEX_W_0F3817_P_2,
  VEX_W_0F3818_P_2_M_0,
  VEX_W_0F3819_P_2_M_0,
  VEX_W_0F381A_P_2_M_0,
  VEX_W_0F381C_P_2,
  VEX_W_0F381D_P_2,
  VEX_W_0F381E_P_2,
  VEX_W_0F3820_P_2,
  VEX_W_0F3821_P_2,
  VEX_W_0F3822_P_2,
  VEX_W_0F3823_P_2,
  VEX_W_0F3824_P_2,
  VEX_W_0F3825_P_2,
  VEX_W_0F3828_P_2,
  VEX_W_0F3829_P_2,
  VEX_W_0F382A_P_2_M_0,
  VEX_W_0F382B_P_2,
  VEX_W_0F382C_P_2_M_0,
  VEX_W_0F382D_P_2_M_0,
  VEX_W_0F382E_P_2_M_0,
  VEX_W_0F382F_P_2_M_0,
  VEX_W_0F3830_P_2,
  VEX_W_0F3831_P_2,
  VEX_W_0F3832_P_2,
  VEX_W_0F3833_P_2,
  VEX_W_0F3834_P_2,
  VEX_W_0F3835_P_2,
  VEX_W_0F3837_P_2,
  VEX_W_0F3838_P_2,
  VEX_W_0F3839_P_2,
  VEX_W_0F383A_P_2,
  VEX_W_0F383B_P_2,
  VEX_W_0F383C_P_2,
  VEX_W_0F383D_P_2,
  VEX_W_0F383E_P_2,
  VEX_W_0F383F_P_2,
  VEX_W_0F3840_P_2,
  VEX_W_0F3841_P_2,
  VEX_W_0F38DB_P_2,
  VEX_W_0F38DC_P_2,
  VEX_W_0F38DD_P_2,
  VEX_W_0F38DE_P_2,
  VEX_W_0F38DF_P_2,
  VEX_W_0F3A04_P_2,
  VEX_W_0F3A05_P_2,
  VEX_W_0F3A06_P_2,
  VEX_W_0F3A08_P_2,
  VEX_W_0F3A09_P_2,
  VEX_W_0F3A0A_P_2,
  VEX_W_0F3A0B_P_2,
  VEX_W_0F3A0C_P_2,
  VEX_W_0F3A0D_P_2,
  VEX_W_0F3A0E_P_2,
  VEX_W_0F3A0F_P_2,
  VEX_W_0F3A14_P_2,
  VEX_W_0F3A15_P_2,
  VEX_W_0F3A18_P_2,
  VEX_W_0F3A19_P_2,
  VEX_W_0F3A20_P_2,
  VEX_W_0F3A21_P_2,
  VEX_W_0F3A40_P_2,
  VEX_W_0F3A41_P_2,
  VEX_W_0F3A42_P_2,
  VEX_W_0F3A44_P_2,
  VEX_W_0F3A48_P_2,
  VEX_W_0F3A49_P_2,
  VEX_W_0F3A4A_P_2,
  VEX_W_0F3A4B_P_2,
  VEX_W_0F3A4C_P_2,
  VEX_W_0F3A60_P_2,
  VEX_W_0F3A61_P_2,
  VEX_W_0F3A62_P_2,
  VEX_W_0F3A63_P_2,
  VEX_W_0F3ADF_P_2
} T_VEX_W_enum;

typedef void (*op_rtn) (int bytemode, int sizeflag);

class T_op
{
 public:
  T_op(op_rtn rtn, int bytemode);
  T_op(); // default construction

  op_rtn rtn;
  int bytemode;
};

struct dis386 {
  const char *name;
   
  T_op op[MAX_OPERANDS];

  //dis386(const char *name,T_op a);
  dis386(const char *name,T_op a, T_op b);
  dis386();
  dis386(const dis386& cp); // copy constructor
  dis386(const char *name,std::initializer_list<T_op> a);

};

const int MAX_CODE_LENGTH =15;
/* We can up to 14 prefixes since the maximum instruction length is
   15bytes.  */

typedef struct T_modrm
  {
    int mod;
    int reg;
    int rm;
  } t_modrm;

typedef struct T_sib
  {
    int scale;
    int index;
    int base;
  } t_sib;

typedef struct T_vex
  {
    int register_specifier;
    int length;
    int prefix;
    int w;
  }t_vex;


typedef struct op
  {
    const char *name;
    unsigned int len;
  } T_OP;




class StaticData
{

 public:
int
  putop (const char *in_template, int sizeflag);


  int ckprefix();
  const char * prefix_name (int pref, int sizeflag);
  int print_insn_i386_att (bfd_vma pc, disassemble_info *info);
  int print_insn_i386_intel (bfd_vma pc, disassemble_info *info);
  int print_insn_i386 (bfd_vma pc, disassemble_info *info);
  void print_i386_disassembler_options (FILE *stream);
  const struct dis386 *  get_valid_dis386 (const struct dis386 *dp, disassemble_info *info);
  void  get_sib (disassemble_info *info);
  int  print_insn (bfd_vma pc, disassemble_info *info);
  void  swap_operand (void);
  void OP_Skip_MODRM (int bytemode ATTRIBUTE_UNUSED,	       int sizeflag ATTRIBUTE_UNUSED);
  void  dofloat (int sizeflag);
  void OP_ST (int bytemode ATTRIBUTE_UNUSED, int sizeflag ATTRIBUTE_UNUSED);
  void OP_STi (int bytemode ATTRIBUTE_UNUSED, int sizeflag ATTRIBUTE_UNUSED);
  unsigned char get_vex_imm8 (int sizeflag, int opnum);


  //----------------------------------  
  t_vex vex;// used in the functions

int all_prefixes[MAX_CODE_LENGTH - 1];
disassemble_info *the_info;

unsigned char need_modrm;
const char **names64;
const char **names32;
const char **names16;
const char **names8;
const char **names8rex;
const char **names_seg;
const char *index64;
const char *index32;
const char **index16;

 char op_out[MAX_OPERANDS][100];
 int op_ad, op_index[MAX_OPERANDS];
 int two_source_ops;
 bfd_vma op_address[MAX_OPERANDS];
 bfd_vma op_riprel[MAX_OPERANDS];
 bfd_vma start_pc;

 char intel_syntax;
 char intel_mnemonic ;

 static char * INTERNAL_DISASSEMBLER_ERROR;

 void oappend (const char *s);
 void append_seg (void);
 void OP_indirE (int bytemode, int sizeflag);
 void print_operand_value (char *buf, int hex, bfd_vma disp);
 void print_displacement (char *buf, bfd_vma disp);
 void intel_operand_size (int bytemode, int sizeflag);
 void OP_E_register (int bytemode, int sizeflag);
 void OP_E_memory (int bytemode, int sizeflag);
 bfd_signed_vma get32();
 bfd_signed_vma get32s();
 bfd_vma get64();
 int get16();
 void BadOp();
 void CMPXCHG8B_Fixup(int, int);
 void CMP_Fixup(int, int);
 void CRC32_Fixup(int, int);
 void FXSAVE_Fixup(int, int);
 void MOVBE_Fixup(int, int);
 void NOP_Fixup1(int, int);
 void NOP_Fixup2(int, int);
 void OP_0f07(int, int);
 void OP_3DNowSuffix(int, int);
 void OP_C(int, int);
 void OP_D(int, int);
 void OP_DIR(int, int);
 void OP_DSreg(int, int);
 void OP_E(int, int);
 void OP_EM(int, int);
 void OP_EMC(int, int);
 void OP_ESreg(int, int);
 void OP_EX(int, int);
 void OP_EX_Vex(int, int);
 void OP_EX_VexImmW(int, int);
 void OP_EX_VexReg(int, int, int);
 void OP_EX_VexW(int, int);
 void OP_G(int, int);
 void OP_I(int, int);
 void OP_I64(int, int);
 void OP_IMREG(int, int);
 void OP_J(int, int);
 void OP_LWPCB_E(int, int);;
 void OP_LWP_E(int, int);;
 void OP_M(int, int);;
 void OP_MMX(int, int);;
 void OP_MS(int, int);
 void OP_MXC(int, int);
 void OP_Monitor(int, int);
 void OP_Mwait(int, int);
 void OP_OFF(int, int);
 void OP_OFF64(int, int);
 void OP_R(int, int);
 void OP_REG(int, int);
 void OP_REG_VexI4(int, int);
 void OP_SEG(int, int);
 void OP_T(int, int);
 void OP_VEX(int, int);
 void OP_Vex_2src(int, int);
 void OP_Vex_2src_1(int, int);
 void OP_Vex_2src_2(int, int);
 void OP_XMM(int, int);
 void OP_XMM_Vex(int, int);
 void OP_XMM_VexW(int, int);
 void OP_XS(int, int);
 void OP_sI(int, int);
 void PCLMUL_Fixup(int, int);
 void REP_Fixup(int, int);
 void VCMP_Fixup(int, int);
 void VEXI4_Fixup(int, int);
 void VZERO_Fixup(int, int);
 void XMM_Fixup(int, int);
 void ptr_reg(int, int);
 void set_op(bfd_vma, int);

 // constructor
 StaticData()
   {
     INTERNAL_DISASSEMBLER_ERROR=_("<internal disassembler error>");

     intel_mnemonic = !SYSV386_COMPAT;

   }

 char open_char;
 char close_char;
 char separator_char;
 char scale_char;

T_modrm modrm;
T_sib sib;
unsigned char need_vex;
unsigned char need_vex_reg;
unsigned char vex_w_done;
enum address_mode address_mode;
/* Flags for the prefixes for the current instruction.  See below.  */
int prefixes;
/* REX prefix the current instruction.  See below.  */
int rex;

/* Mark parts used in the REX prefix.  When we are testing for
   empty prefix (for 8bit register REX extension), just mask it
   out.  Otherwise test for REX bit is excuse for existence of REX
   only in case value is nonzero.  */

void DOUSED_REX(int value)					
  {							
    if (value)						
      {							
	if ((rex & value))				
	  rex_used |= (value) | REX_OPCODE;		
      }							
    else						
      rex_used |= REX_OPCODE;				
  }

#define USED_REX(value)	 DOUSED_REX(value)				    


/* Bits of REX we've already used.  */
int rex_used;
/* REX bits in original REX prefix ignored.  */
int rex_ignored;
/* Flags for prefixes which we somehow handled when printing the
   current instruction.  */
int used_prefixes;
char obuf[100];
char *obufp;
char *mnemonicendp;
char scratchbuf[100];
unsigned char *start_codep;
unsigned char *insn_codep;
unsigned char *codep;
int last_lock_prefix;
int last_repz_prefix;
int last_repnz_prefix;
int last_data_prefix;
int last_addr_prefix;
int last_rex_prefix;
int last_seg_prefix;

 static const char *fgrps[][8];
 static const char *float_mem[];
 static const char *intel_names64[];
 static const char *intel_names32[];
 static const char *intel_names16[];
 static const char *intel_names8[];
 static const char *intel_names8rex[];
 static const char *intel_names_seg[];
 static const char *intel_index64 ;
 static const char *intel_index32 ;
 static const char *intel_index16[];
 static const char *att_names64[] ;
 static const char *att_names32[] ;
 static const char *att_names16[] ;
 static const char *att_names8[] ;
 static const char *att_names8rex[] ;
 static const char *att_names_seg[] ;
 static const char *att_index64 ;
 static const char *att_index32 ;
 static const char *att_index16[] ;
 static const char **names_mm;
 static const char *intel_names_mm[] ;
 static const char *att_names_mm[] ;
 static const char **names_xmm;
 static const char *intel_names_xmm[] ;
 static const char *att_names_xmm[] ;
 static const char **names_ymm;
 static const char *intel_names_ymm[] ;
 static const char *att_names_ymm[] ;
 static struct op vex_cmp_op[];
 static const char *rexes [16];

 unsigned int ARRAY_SIZE_vex_cmp_op(); // ARRAY_SIZE(vex_cmp_op())
 
};

const dis386 Bad_Opcode (NULL, { { NULL, 0 } });



/* bits in sizeflag */
const int SUFFIX_ALWAYS =4;
const int AFLAG =2;
const int DFLAG =1;
const dis386 FLOAT( NULL, {T_op( NULL, FLOATCODE )} );

inline const dis386 mkdis386(const char *name,T_op a, T_op b) {
  return dis386(name,a,b);
}

#define DIS386(T, I) mkdis386(NULL,T_op( NULL,(T)), T_op( NULL,  (I) ) )
// template<class T, class I>
#define REG_TABLE(I)		DIS386 (USE_REG_TABLE, (I))
#define MOD_TABLE(I)		DIS386 (USE_MOD_TABLE, (I))
#define RM_TABLE(I)		DIS386 (USE_RM_TABLE, (I))
#define PREFIX_TABLE(I)		DIS386 (USE_PREFIX_TABLE, (I))
#define X86_64_TABLE(I)		DIS386 (USE_X86_64_TABLE, (I))
#define THREE_BYTE_TABLE(I)	DIS386 (USE_3BYTE_TABLE, (I))
#define XOP_8F_TABLE(I)		DIS386 (USE_XOP_8F_TABLE, (I))
#define VEX_C4_TABLE(I)		DIS386 (USE_VEX_C4_TABLE, (I))
#define VEX_C5_TABLE(I)		DIS386 (USE_VEX_C5_TABLE, (I))
#define VEX_LEN_TABLE(I)	DIS386 (USE_VEX_LEN_TABLE, (I))
#define VEX_W_TABLE(I)		DIS386 (USE_VEX_W_TABLE, (I))

#include "i386-dis-parts.h"
#include "i386-consts.h"
#include "i386-dis-floats.h"
#include "i386-dis-table_floatreg.h"
#include "i386-dis-table_dis386.h"
#include "i386-dis-table_dis386_twobyte.h"


#include "i386-dis-table_floatmemmode.h"
#include "i386-dis-table_hasmodrm.h"
#include "i386-dis-table_mod.h"

#include "i386-dis-table_pcl_mul_ops.h"
#include "i386-dis-table_prefix.h"
#include "i386-dis-table_reg.h"
#include "i386-dis-table_rm.h"
#include "i386-dis-table_threebyte.h"
#include "i386-dis-table_twobhasmodrm.h"

#include "i386-dis-table_vex.h"
#include "i386-dis-table_vex_len.h"
#include "i386-dis-table_vex_w.h"
#include "i386-dis-table_xop.h"

#include "i386-dis-table_threednow.h"
#include "i386-dis-table_simd_cmp_op.h"

int fetch_data (struct disassemble_info *info, bfd_byte *addr);

int DOFETCH_DATA(struct disassemble_info *info, bfd_byte *addr) ;
// Refactored this macro
#define FETCH_DATA(info, addr) DOFETCH_DATA(info, addr) 

int seg_prefix (int pref); // used by i386-dis-print-insn.cpp


};
