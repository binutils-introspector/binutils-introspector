#include "sysdep.h"
#include "dis-asm.h"
#include "opintl.h"
#include "opcode/i386.h"
#include "libiberty.h"

#include <setjmp.h>
#include "i386-dis.h"

int main()
{
  bfd_vma pc;
  disassemble_info info;
  info.mach=bfd_mach_i386_i8086; //set the machine

  int ret= print_insn_i386 (pc, &info);

  return ret;
}
