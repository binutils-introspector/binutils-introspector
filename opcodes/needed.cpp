#include "sysdep.h"
#include "dis-asm.h"
#include "opintl.h"
#include "opcode/i386.h"
#include "libiberty.h"

#include <setjmp.h>
#include "i386-dis.h"

#define makef(X)   \
  void X (int a, int b) \
  { \
  return glob.X(a,b); \
  }

namespace binutils_objdump_opcodes_i386
{
  StaticData glob;

  makef (CMP_Fixup)
  makef (CMPXCHG8B_Fixup)
  makef (CRC32_Fixup)
  makef(FXSAVE_Fixup)
  makef(MOVBE_Fixup)
  makef(NOP_Fixup1)
  makef(NOP_Fixup2)
  makef(OP_0f07)
  makef(OP_3DNowSuffix)
  makef(OP_C)
  makef(OP_D)
  makef(OP_DIR)
  makef(  OP_DSreg)
  makef(  OP_E)
  makef(  OP_EMC)
  makef(  OP_EM)
  makef(  OP_ESreg)
  makef(  OP_EX)
  makef(  OP_EX_VexImmW)
  makef(  OP_EX_Vex)
  makef(  OP_EX_VexW)
  makef(  OP_G)
  makef(  OP_I64)
  makef(  OP_I)
  makef(  OP_IMREG)
  makef(  OP_indirE)
  makef(  OP_J)
  makef(  OP_LWPCB_E)
  makef(  OP_LWP_E)
  makef(  OP_M)
  makef(  OP_MMX)
  makef(  OP_Monitor)
  makef(  OP_MS)
  makef(  OP_Mwait)
  makef(  OP_MXC)
  makef(  OP_OFF64)
  makef(  OP_REG)
  makef(  OP_REG_VexI4)
  makef(  OP_R)
  makef(  OP_SEG)
  makef(  OP_sI)
  makef(  OP_Skip_MODRM)
  makef(  OP_STi)
  makef(  OP_ST)
  makef(  OP_T)
  makef(  OP_Vex_2src_1)
  makef(  OP_Vex_2src_2)
  makef(  OP_VEX)
  makef(  OP_XMM)
  makef(  OP_XMM_Vex)
  makef(  OP_XMM_VexW)
  makef(  OP_XS)
  makef(  PCLMUL_Fixup)
  makef(  REP_Fixup)
  makef(  VCMP_Fixup)
  makef(  VEXI4_Fixup)
  makef(  VZERO_Fixup)
  makef(  XMM_Fixup)
  //void (*)(int, int), int)
  //  T_op::T_op(void (*)(int, int), int){}
  dis386::dis386()
  {}

  dis386::dis386(const dis386& cp){}
  dis386::dis386(const char* name, std::initializer_list<binutils_objdump_opcodes_i386::T_op> list){}

  dis386::dis386(const char*, binutils_objdump_opcodes_i386::T_op, binutils_objdump_opcodes_i386::T_op){}

  T_op::T_op(void (* fun)(int, int), int)
  {
  }

  T_op::T_op()
  {
  }


  // allocate the space.
  char * StaticData::INTERNAL_DISASSEMBLER_ERROR;

  #include "i386-dis-table_fgrps.h"

};
/*
int main(int argc, char ** argv)
{
  return 0;
  }
*/
