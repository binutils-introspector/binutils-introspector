const struct dis386 prefix_table[][4] = {
  /* PREFIX_90 */
  {
    { "xchgS", { { NOP_Fixup1, eAX_reg }, { NOP_Fixup2, eAX_reg } } },
    { "pause", { XX } },
    { "xchgS", { { NOP_Fixup1, eAX_reg }, { NOP_Fixup2, eAX_reg } } },
  },

  /* PREFIX_0F10 */
  {
    { "movups",	{ XM, EXx } },
    { "movss",	{ XM, EXd } },
    { "movupd",	{ XM, EXx } },
    { "movsd",	{ XM, EXq } },
  },

  /* PREFIX_0F11 */
  {
    { "movups",	{ EXxS, XM } },
    { "movss",	{ EXdS, XM } },
    { "movupd",	{ EXxS, XM } },
    { "movsd",	{ EXqS, XM } },
  },

  /* PREFIX_0F12 */
  {
    { MOD_TABLE (MOD_0F12_PREFIX_0) },
    { "movsldup", { XM, EXx } },
    { "movlpd",	{ XM, EXq } },
    { "movddup", { XM, EXq } },
  },

  /* PREFIX_0F16 */
  {
    { MOD_TABLE (MOD_0F16_PREFIX_0) },
    { "movshdup", { XM, EXx } },
    { "movhpd",	{ XM, EXq } },
  },

  /* PREFIX_0F2A */
  {
    { "cvtpi2ps", { XM, EMCq } },
    { "cvtsi2ss%LQ", { XM, Ev } },
    { "cvtpi2pd", { XM, EMCq } },
    { "cvtsi2sd%LQ", { XM, Ev } },
  },

  /* PREFIX_0F2B */
  {
    { MOD_TABLE (MOD_0F2B_PREFIX_0) },
    { MOD_TABLE (MOD_0F2B_PREFIX_1) },
    { MOD_TABLE (MOD_0F2B_PREFIX_2) },
    { MOD_TABLE (MOD_0F2B_PREFIX_3) },
  },

  /* PREFIX_0F2C */
  {
    { "cvttps2pi", { MXC, EXq } },
    { "cvttss2siY", { Gv, EXd } },
    { "cvttpd2pi", { MXC, EXx } },
    { "cvttsd2siY", { Gv, EXq } },
  },

  /* PREFIX_0F2D */
  {
    { "cvtps2pi", { MXC, EXq } },
    { "cvtss2siY", { Gv, EXd } },
    { "cvtpd2pi", { MXC, EXx } },
    { "cvtsd2siY", { Gv, EXq } },
  },

  /* PREFIX_0F2E */
  {
    { "ucomiss",{ XM, EXd } }, 
    { Bad_Opcode },
    { "ucomisd",{ XM, EXq } }, 
  },

  /* PREFIX_0F2F */
  {
    { "comiss",	{ XM, EXd } },
    { Bad_Opcode },
    { "comisd",	{ XM, EXq } },
  },

  /* PREFIX_0F51 */
  {
    { "sqrtps", { XM, EXx } },
    { "sqrtss", { XM, EXd } },
    { "sqrtpd", { XM, EXx } },
    { "sqrtsd",	{ XM, EXq } },
  },

  /* PREFIX_0F52 */
  {
    { "rsqrtps",{ XM, EXx } },
    { "rsqrtss",{ XM, EXd } },
  },

  /* PREFIX_0F53 */
  {
    { "rcpps",	{ XM, EXx } },
    { "rcpss",	{ XM, EXd } },
  },

  /* PREFIX_0F58 */
  {
    { "addps", { XM, EXx } },
    { "addss", { XM, EXd } },
    { "addpd", { XM, EXx } },
    { "addsd", { XM, EXq } },
  },

  /* PREFIX_0F59 */
  {
    { "mulps",	{ XM, EXx } },
    { "mulss",	{ XM, EXd } },
    { "mulpd",	{ XM, EXx } },
    { "mulsd",	{ XM, EXq } },
  },

  /* PREFIX_0F5A */
  {
    { "cvtps2pd", { XM, EXq } },
    { "cvtss2sd", { XM, EXd } },
    { "cvtpd2ps", { XM, EXx } },
    { "cvtsd2ss", { XM, EXq } },
  },

  /* PREFIX_0F5B */
  {
    { "cvtdq2ps", { XM, EXx } },
    { "cvttps2dq", { XM, EXx } },
    { "cvtps2dq", { XM, EXx } },
  },

  /* PREFIX_0F5C */
  {
    { "subps",	{ XM, EXx } },
    { "subss",	{ XM, EXd } },
    { "subpd",	{ XM, EXx } },
    { "subsd",	{ XM, EXq } },
  },

  /* PREFIX_0F5D */
  {
    { "minps",	{ XM, EXx } },
    { "minss",	{ XM, EXd } },
    { "minpd",	{ XM, EXx } },
    { "minsd",	{ XM, EXq } },
  },

  /* PREFIX_0F5E */
  {
    { "divps",	{ XM, EXx } },
    { "divss",	{ XM, EXd } },
    { "divpd",	{ XM, EXx } },
    { "divsd",	{ XM, EXq } },
  },

  /* PREFIX_0F5F */
  {
    { "maxps",	{ XM, EXx } },
    { "maxss",	{ XM, EXd } },
    { "maxpd",	{ XM, EXx } },
    { "maxsd",	{ XM, EXq } },
  },

  /* PREFIX_0F60 */
  {
    { "punpcklbw",{ MX, EMd } },
    { Bad_Opcode },
    { "punpcklbw",{ MX, EMx } },
  },

  /* PREFIX_0F61 */
  {
    { "punpcklwd",{ MX, EMd } },
    { Bad_Opcode },
    { "punpcklwd",{ MX, EMx } },
  },

  /* PREFIX_0F62 */
  {
    { "punpckldq",{ MX, EMd } },
    { Bad_Opcode },
    { "punpckldq",{ MX, EMx } },
  },

  /* PREFIX_0F6C */
  {
    { Bad_Opcode },
    { Bad_Opcode },
    { "punpcklqdq", { XM, EXx } },
  },

  /* PREFIX_0F6D */
  {
    { Bad_Opcode },
    { Bad_Opcode },
    { "punpckhqdq", { XM, EXx } },
  },

  /* PREFIX_0F6F */
  {
    { "movq",	{ MX, EM } },
    { "movdqu",	{ XM, EXx } },
    { "movdqa",	{ XM, EXx } },
  },

  /* PREFIX_0F70 */
  {
    { "pshufw",	{ MX, EM, Ib } },
    { "pshufhw",{ XM, EXx, Ib } },
    { "pshufd",	{ XM, EXx, Ib } },
    { "pshuflw",{ XM, EXx, Ib } },
  },

  /* PREFIX_0F73_REG_3 */
  {
    { Bad_Opcode },
    { Bad_Opcode },
    { "psrldq",	{ XS, Ib } },
  },

  /* PREFIX_0F73_REG_7 */
  {
    { Bad_Opcode },
    { Bad_Opcode },
    { "pslldq",	{ XS, Ib } },
  },

  /* PREFIX_0F78 */
  {
    {"vmread",	{ Em, Gm } },
    { Bad_Opcode },
    {"extrq",	{ XS, Ib, Ib } },
    {"insertq",	{ XM, XS, Ib, Ib } },
  },

  /* PREFIX_0F79 */
  {
    {"vmwrite",	{ Gm, Em } },
    { Bad_Opcode },
    {"extrq",	{ XM, XS } },
    {"insertq",	{ XM, XS } },
  },

  /* PREFIX_0F7C */
  {
    { Bad_Opcode },
    { Bad_Opcode },
    { "haddpd",	{ XM, EXx } },
    { "haddps",	{ XM, EXx } },
  },

  /* PREFIX_0F7D */
  {
    { Bad_Opcode },
    { Bad_Opcode },
    { "hsubpd",	{ XM, EXx } },
    { "hsubps",	{ XM, EXx } },
  },

  /* PREFIX_0F7E */
  {
    { "movK",	{ Edq, MX } },
    { "movq",	{ XM, EXq } },
    { "movK",	{ Edq, XM } },
  },

  /* PREFIX_0F7F */
  {
    { "movq",	{ EMS, MX } },
    { "movdqu",	{ EXxS, XM } },
    { "movdqa",	{ EXxS, XM } },
  },

  /* PREFIX_0FAE_REG_0 */
  {
    { Bad_Opcode },
    { "rdfsbase", { Ev } },
  },

  /* PREFIX_0FAE_REG_1 */
  {
    { Bad_Opcode },
    { "rdgsbase", { Ev } },
  },

  /* PREFIX_0FAE_REG_2 */
  {
    { Bad_Opcode },
    { "wrfsbase", { Ev } },
  },

  /* PREFIX_0FAE_REG_3 */
  {
    { Bad_Opcode },
    { "wrgsbase", { Ev } },
  },

  /* PREFIX_0FB8 */
  {
    { Bad_Opcode },
    { "popcntS", { Gv, Ev } },
  },

  /* PREFIX_0FBD */
  {
    { "bsrS",	{ Gv, Ev } },
    { "lzcntS",	{ Gv, Ev } },
    { "bsrS",	{ Gv, Ev } },
  },

  /* PREFIX_0FC2 */
  {
    { "cmpps",	{ XM, EXx, CMP } },
    { "cmpss",	{ XM, EXd, CMP } },
    { "cmppd",	{ XM, EXx, CMP } },
    { "cmpsd",	{ XM, EXq, CMP } },
  },

  /* PREFIX_0FC3 */
  {
    { "movntiS", { Ma, Gv } },
  },

  /* PREFIX_0FC7_REG_6 */
  {
    { "vmptrld",{ Mq } },
    { "vmxon",	{ Mq } },
    { "vmclear",{ Mq } },
  },

  /* PREFIX_0FD0 */
  {
    { Bad_Opcode },
    { Bad_Opcode },
    { "addsubpd", { XM, EXx } },
    { "addsubps", { XM, EXx } },
  },

  /* PREFIX_0FD6 */
  {
    { Bad_Opcode },
    { "movq2dq",{ XM, MS } },
    { "movq",	{ EXqS, XM } },
    { "movdq2q",{ MX, XS } },
  },

  /* PREFIX_0FE6 */
  {
    { Bad_Opcode },
    { "cvtdq2pd", { XM, EXq } },
    { "cvttpd2dq", { XM, EXx } },
    { "cvtpd2dq", { XM, EXx } },
  },

  /* PREFIX_0FE7 */
  {
    { "movntq",	{ Mq, MX } },
    { Bad_Opcode },
    { MOD_TABLE (MOD_0FE7_PREFIX_2) },
  },

  /* PREFIX_0FF0 */
  {
    { Bad_Opcode },
    { Bad_Opcode },
    { Bad_Opcode },
    { MOD_TABLE (MOD_0FF0_PREFIX_3) },
  },

  /* PREFIX_0FF7 */
  {
    { "maskmovq", { MX, MS } },
    { Bad_Opcode },
    { "maskmovdqu", { XM, XS } },
  },

  /* PREFIX_0F3810 */
  {
    { Bad_Opcode },
    { Bad_Opcode },
    { "pblendvb", { XM, EXx, XMM0 } },
  },

  /* PREFIX_0F3814 */
  {
    { Bad_Opcode },
    { Bad_Opcode },
    { "blendvps", { XM, EXx, XMM0 } },
  },

  /* PREFIX_0F3815 */
  {
    { Bad_Opcode },
    { Bad_Opcode },
    { "blendvpd", { XM, EXx, XMM0 } },
  },

  /* PREFIX_0F3817 */
  {
    { Bad_Opcode },
    { Bad_Opcode },
    { "ptest",  { XM, EXx } },
  },

  /* PREFIX_0F3820 */
  {
    { Bad_Opcode },
    { Bad_Opcode },
    { "pmovsxbw", { XM, EXq } },
  },

  /* PREFIX_0F3821 */
  {
    { Bad_Opcode },
    { Bad_Opcode },
    { "pmovsxbd", { XM, EXd } },
  },

  /* PREFIX_0F3822 */
  {
    { Bad_Opcode },
    { Bad_Opcode },
    { "pmovsxbq", { XM, EXw } },
  },

  /* PREFIX_0F3823 */
  {
    { Bad_Opcode },
    { Bad_Opcode },
    { "pmovsxwd", { XM, EXq } },
  },

  /* PREFIX_0F3824 */
  {
    { Bad_Opcode },
    { Bad_Opcode },
    { "pmovsxwq", { XM, EXd } },
  },

  /* PREFIX_0F3825 */
  {
    { Bad_Opcode },
    { Bad_Opcode },
    { "pmovsxdq", { XM, EXq } },
  },

  /* PREFIX_0F3828 */
  {
    { Bad_Opcode },
    { Bad_Opcode },
    { "pmuldq", { XM, EXx } },
  },

  /* PREFIX_0F3829 */
  {
    { Bad_Opcode },
    { Bad_Opcode },
    { "pcmpeqq", { XM, EXx } },
  },

  /* PREFIX_0F382A */
  {
    { Bad_Opcode },
    { Bad_Opcode },
    { MOD_TABLE (MOD_0F382A_PREFIX_2) },
  },

  /* PREFIX_0F382B */
  {
    { Bad_Opcode },
    { Bad_Opcode },
    { "packusdw", { XM, EXx } },
  },

  /* PREFIX_0F3830 */
  {
    { Bad_Opcode },
    { Bad_Opcode },
    { "pmovzxbw", { XM, EXq } },
  },

  /* PREFIX_0F3831 */
  {
    { Bad_Opcode },
    { Bad_Opcode },
    { "pmovzxbd", { XM, EXd } },
  },

  /* PREFIX_0F3832 */
  {
    { Bad_Opcode },
    { Bad_Opcode },
    { "pmovzxbq", { XM, EXw } },
  },

  /* PREFIX_0F3833 */
  {
    { Bad_Opcode },
    { Bad_Opcode },
    { "pmovzxwd", { XM, EXq } },
  },

  /* PREFIX_0F3834 */
  {
    { Bad_Opcode },
    { Bad_Opcode },
    { "pmovzxwq", { XM, EXd } },
  },

  /* PREFIX_0F3835 */
  {
    { Bad_Opcode },
    { Bad_Opcode },
    { "pmovzxdq", { XM, EXq } },
  },

  /* PREFIX_0F3837 */
  {
    { Bad_Opcode },
    { Bad_Opcode },
    { "pcmpgtq", { XM, EXx } },
  },

  /* PREFIX_0F3838 */
  {
    { Bad_Opcode },
    { Bad_Opcode },
    { "pminsb",	{ XM, EXx } },
  },

  /* PREFIX_0F3839 */
  {
    { Bad_Opcode },
    { Bad_Opcode },
    { "pminsd",	{ XM, EXx } },
  },

  /* PREFIX_0F383A */
  {
    { Bad_Opcode },
    { Bad_Opcode },
    { "pminuw",	{ XM, EXx } },
  },

  /* PREFIX_0F383B */
  {
    { Bad_Opcode },
    { Bad_Opcode },
    { "pminud",	{ XM, EXx } },
  },

  /* PREFIX_0F383C */
  {
    { Bad_Opcode },
    { Bad_Opcode },
    { "pmaxsb",	{ XM, EXx } },
  },

  /* PREFIX_0F383D */
  {
    { Bad_Opcode },
    { Bad_Opcode },
    { "pmaxsd",	{ XM, EXx } },
  },

  /* PREFIX_0F383E */
  {
    { Bad_Opcode },
    { Bad_Opcode },
    { "pmaxuw", { XM, EXx } },
  },

  /* PREFIX_0F383F */
  {
    { Bad_Opcode },
    { Bad_Opcode },
    { "pmaxud", { XM, EXx } },
  },

  /* PREFIX_0F3840 */
  {
    { Bad_Opcode },
    { Bad_Opcode },
    { "pmulld", { XM, EXx } },
  },

  /* PREFIX_0F3841 */
  {
    { Bad_Opcode },
    { Bad_Opcode },
    { "phminposuw", { XM, EXx } },
  },

  /* PREFIX_0F3880 */
  {
    { Bad_Opcode },
    { Bad_Opcode },
    { "invept",	{ Gm, Mo } },
  },

  /* PREFIX_0F3881 */
  {
    { Bad_Opcode },
    { Bad_Opcode },
    { "invvpid", { Gm, Mo } },
  },

  /* PREFIX_0F38DB */
  {
    { Bad_Opcode },
    { Bad_Opcode },
    { "aesimc", { XM, EXx } },
  },

  /* PREFIX_0F38DC */
  {
    { Bad_Opcode },
    { Bad_Opcode },
    { "aesenc", { XM, EXx } },
  },

  /* PREFIX_0F38DD */
  {
    { Bad_Opcode },
    { Bad_Opcode },
    { "aesenclast", { XM, EXx } },
  },

  /* PREFIX_0F38DE */
  {
    { Bad_Opcode },
    { Bad_Opcode },
    { "aesdec", { XM, EXx } },
  },

  /* PREFIX_0F38DF */
  {
    { Bad_Opcode },
    { Bad_Opcode },
    { "aesdeclast", { XM, EXx } },
  },

  /* PREFIX_0F38F0 */
  {
    { "movbeS",	{ Gv, { MOVBE_Fixup, v_mode } } },
    { Bad_Opcode },
    { "movbeS",	{ Gv, { MOVBE_Fixup, v_mode } } },
    { "crc32",	{ Gdq, { CRC32_Fixup, b_mode } } },	
  },

  /* PREFIX_0F38F1 */
  {
    { "movbeS",	{ { MOVBE_Fixup, v_mode }, Gv } },
    { Bad_Opcode },
    { "movbeS",	{ { MOVBE_Fixup, v_mode }, Gv } },
    { "crc32",	{ Gdq, { CRC32_Fixup, v_mode } } },	
  },

  /* PREFIX_0F3A08 */
  {
    { Bad_Opcode },
    { Bad_Opcode },
    { "roundps", { XM, EXx, Ib } },
  },

  /* PREFIX_0F3A09 */
  {
    { Bad_Opcode },
    { Bad_Opcode },
    { "roundpd", { XM, EXx, Ib } },
  },

  /* PREFIX_0F3A0A */
  {
    { Bad_Opcode },
    { Bad_Opcode },
    { "roundss", { XM, EXd, Ib } },
  },

  /* PREFIX_0F3A0B */
  {
    { Bad_Opcode },
    { Bad_Opcode },
    { "roundsd", { XM, EXq, Ib } },
  },

  /* PREFIX_0F3A0C */
  {
    { Bad_Opcode },
    { Bad_Opcode },
    { "blendps", { XM, EXx, Ib } },
  },

  /* PREFIX_0F3A0D */
  {
    { Bad_Opcode },
    { Bad_Opcode },
    { "blendpd", { XM, EXx, Ib } },
  },

  /* PREFIX_0F3A0E */
  {
    { Bad_Opcode },
    { Bad_Opcode },
    { "pblendw", { XM, EXx, Ib } },
  },

  /* PREFIX_0F3A14 */
  {
    { Bad_Opcode },
    { Bad_Opcode },
    { "pextrb",	{ Edqb, XM, Ib } },
  },

  /* PREFIX_0F3A15 */
  {
    { Bad_Opcode },
    { Bad_Opcode },
    { "pextrw",	{ Edqw, XM, Ib } },
  },

  /* PREFIX_0F3A16 */
  {
    { Bad_Opcode },
    { Bad_Opcode },
    { "pextrK",	{ Edq, XM, Ib } },
  },

  /* PREFIX_0F3A17 */
  {
    { Bad_Opcode },
    { Bad_Opcode },
    { "extractps", { Edqd, XM, Ib } },
  },

  /* PREFIX_0F3A20 */
  {
    { Bad_Opcode },
    { Bad_Opcode },
    { "pinsrb",	{ XM, Edqb, Ib } },
  },

  /* PREFIX_0F3A21 */
  {
    { Bad_Opcode },
    { Bad_Opcode },
    { "insertps", { XM, EXd, Ib } },
  },

  /* PREFIX_0F3A22 */
  {
    { Bad_Opcode },
    { Bad_Opcode },
    { "pinsrK",	{ XM, Edq, Ib } },
  },

  /* PREFIX_0F3A40 */
  {
    { Bad_Opcode },
    { Bad_Opcode },
    { "dpps",	{ XM, EXx, Ib } },
  },

  /* PREFIX_0F3A41 */
  {
    { Bad_Opcode },
    { Bad_Opcode },
    { "dppd",	{ XM, EXx, Ib } },
  },

  /* PREFIX_0F3A42 */
  {
    { Bad_Opcode },
    { Bad_Opcode },
    { "mpsadbw", { XM, EXx, Ib } },
  },

  /* PREFIX_0F3A44 */
  {
    { Bad_Opcode },
    { Bad_Opcode },
    { "pclmulqdq", { XM, EXx, PCLMUL } },
  },

  /* PREFIX_0F3A60 */
  {
    { Bad_Opcode },
    { Bad_Opcode },
    { "pcmpestrm", { XM, EXx, Ib } },
  },

  /* PREFIX_0F3A61 */
  {
    { Bad_Opcode },
    { Bad_Opcode },
    { "pcmpestri", { XM, EXx, Ib } },
  },

  /* PREFIX_0F3A62 */
  {
    { Bad_Opcode },
    { Bad_Opcode },
    { "pcmpistrm", { XM, EXx, Ib } },
  },

  /* PREFIX_0F3A63 */
  {
    { Bad_Opcode },
    { Bad_Opcode },
    { "pcmpistri", { XM, EXx, Ib } },
  },

  /* PREFIX_0F3ADF */
  {
    { Bad_Opcode },
    { Bad_Opcode },
    { "aeskeygenassist", { XM, EXx, Ib } },
  },

  /* PREFIX_VEX_0F10 */
  {
    { VEX_W_TABLE (VEX_W_0F10_P_0) },
    { VEX_LEN_TABLE (VEX_LEN_0F10_P_1) },
    { VEX_W_TABLE (VEX_W_0F10_P_2) },
    { VEX_LEN_TABLE (VEX_LEN_0F10_P_3) },
  },

  /* PREFIX_VEX_0F11 */
  {
    { VEX_W_TABLE (VEX_W_0F11_P_0) },
    { VEX_LEN_TABLE (VEX_LEN_0F11_P_1) },
    { VEX_W_TABLE (VEX_W_0F11_P_2) },
    { VEX_LEN_TABLE (VEX_LEN_0F11_P_3) },
  },

  /* PREFIX_VEX_0F12 */
  {
    { MOD_TABLE (MOD_VEX_0F12_PREFIX_0) },
    { VEX_W_TABLE (VEX_W_0F12_P_1) },
    { VEX_LEN_TABLE (VEX_LEN_0F12_P_2) },
    { VEX_W_TABLE (VEX_W_0F12_P_3) },
  },

  /* PREFIX_VEX_0F16 */
  {
    { MOD_TABLE (MOD_VEX_0F16_PREFIX_0) },
    { VEX_W_TABLE (VEX_W_0F16_P_1) },
    { VEX_LEN_TABLE (VEX_LEN_0F16_P_2) },
  },

  /* PREFIX_VEX_0F2A */
  {
    { Bad_Opcode },
    { VEX_LEN_TABLE (VEX_LEN_0F2A_P_1) },
    { Bad_Opcode },
    { VEX_LEN_TABLE (VEX_LEN_0F2A_P_3) },
  },

  /* PREFIX_VEX_0F2C */
  {
    { Bad_Opcode },
    { VEX_LEN_TABLE (VEX_LEN_0F2C_P_1) },
    { Bad_Opcode },
    { VEX_LEN_TABLE (VEX_LEN_0F2C_P_3) },
  },

  /* PREFIX_VEX_0F2D */
  {
    { Bad_Opcode },
    { VEX_LEN_TABLE (VEX_LEN_0F2D_P_1) },
    { Bad_Opcode },
    { VEX_LEN_TABLE (VEX_LEN_0F2D_P_3) },
  },

  /* PREFIX_VEX_0F2E */
  {
    { VEX_LEN_TABLE (VEX_LEN_0F2E_P_0) },
    { Bad_Opcode },
    { VEX_LEN_TABLE (VEX_LEN_0F2E_P_2) },
  },

  /* PREFIX_VEX_0F2F */
  {
    { VEX_LEN_TABLE (VEX_LEN_0F2F_P_0) },
    { Bad_Opcode },
    { VEX_LEN_TABLE (VEX_LEN_0F2F_P_2) },
  },

  /* PREFIX_VEX_0F51 */
  {
    { VEX_W_TABLE (VEX_W_0F51_P_0) },
    { VEX_LEN_TABLE (VEX_LEN_0F51_P_1) },
    { VEX_W_TABLE (VEX_W_0F51_P_2) },
    { VEX_LEN_TABLE (VEX_LEN_0F51_P_3) },
  },

  /* PREFIX_VEX_0F52 */
  {
    { VEX_W_TABLE (VEX_W_0F52_P_0) },
    { VEX_LEN_TABLE (VEX_LEN_0F52_P_1) },
  },

  /* PREFIX_VEX_0F53 */
  {
    { VEX_W_TABLE (VEX_W_0F53_P_0) },
    { VEX_LEN_TABLE (VEX_LEN_0F53_P_1) },
  },

  /* PREFIX_VEX_0F58 */
  {
    { VEX_W_TABLE (VEX_W_0F58_P_0) },
    { VEX_LEN_TABLE (VEX_LEN_0F58_P_1) },
    { VEX_W_TABLE (VEX_W_0F58_P_2) },
    { VEX_LEN_TABLE (VEX_LEN_0F58_P_3) },
  },

  /* PREFIX_VEX_0F59 */
  {
    { VEX_W_TABLE (VEX_W_0F59_P_0) },
    { VEX_LEN_TABLE (VEX_LEN_0F59_P_1) },
    { VEX_W_TABLE (VEX_W_0F59_P_2) },
    { VEX_LEN_TABLE (VEX_LEN_0F59_P_3) },
  },

  /* PREFIX_VEX_0F5A */
  {
    { VEX_W_TABLE (VEX_W_0F5A_P_0) },
    { VEX_LEN_TABLE (VEX_LEN_0F5A_P_1) },
    { "vcvtpd2ps%XY", { XMM, EXx } },
    { VEX_LEN_TABLE (VEX_LEN_0F5A_P_3) },
  },

  /* PREFIX_VEX_0F5B */
  {
    { VEX_W_TABLE (VEX_W_0F5B_P_0) },
    { VEX_W_TABLE (VEX_W_0F5B_P_1) },
    { VEX_W_TABLE (VEX_W_0F5B_P_2) },
  },

  /* PREFIX_VEX_0F5C */
  {
    { VEX_W_TABLE (VEX_W_0F5C_P_0) },
    { VEX_LEN_TABLE (VEX_LEN_0F5C_P_1) },
    { VEX_W_TABLE (VEX_W_0F5C_P_2) },
    { VEX_LEN_TABLE (VEX_LEN_0F5C_P_3) },
  },

  /* PREFIX_VEX_0F5D */
  {
    { VEX_W_TABLE (VEX_W_0F5D_P_0) },
    { VEX_LEN_TABLE (VEX_LEN_0F5D_P_1) },
    { VEX_W_TABLE (VEX_W_0F5D_P_2) },
    { VEX_LEN_TABLE (VEX_LEN_0F5D_P_3) },
  },

  /* PREFIX_VEX_0F5E */
  {
    { VEX_W_TABLE (VEX_W_0F5E_P_0) },
    { VEX_LEN_TABLE (VEX_LEN_0F5E_P_1) },
    { VEX_W_TABLE (VEX_W_0F5E_P_2) },
    { VEX_LEN_TABLE (VEX_LEN_0F5E_P_3) },
  },

  /* PREFIX_VEX_0F5F */
  {
    { VEX_W_TABLE (VEX_W_0F5F_P_0) },
    { VEX_LEN_TABLE (VEX_LEN_0F5F_P_1) },
    { VEX_W_TABLE (VEX_W_0F5F_P_2) },
    { VEX_LEN_TABLE (VEX_LEN_0F5F_P_3) },
  },

  /* PREFIX_VEX_0F60 */
  {
    { Bad_Opcode },
    { Bad_Opcode },
    { VEX_LEN_TABLE (VEX_LEN_0F60_P_2) },
  },

  /* PREFIX_VEX_0F61 */
  {
    { Bad_Opcode },
    { Bad_Opcode },
    { VEX_LEN_TABLE (VEX_LEN_0F61_P_2) },
  },

  /* PREFIX_VEX_0F62 */
  {
    { Bad_Opcode },
    { Bad_Opcode },
    { VEX_LEN_TABLE (VEX_LEN_0F62_P_2) },
  },

  /* PREFIX_VEX_0F63 */
  {
    { Bad_Opcode },
    { Bad_Opcode },
    { VEX_LEN_TABLE (VEX_LEN_0F63_P_2) },
  },

  /* PREFIX_VEX_0F64 */
  {
    { Bad_Opcode },
    { Bad_Opcode },
    { VEX_LEN_TABLE (VEX_LEN_0F64_P_2) },
  },

  /* PREFIX_VEX_0F65 */
  {
    { Bad_Opcode },
    { Bad_Opcode },
    { VEX_LEN_TABLE (VEX_LEN_0F65_P_2) },
  },

  /* PREFIX_VEX_0F66 */
  {
    { Bad_Opcode },
    { Bad_Opcode },
    { VEX_LEN_TABLE (VEX_LEN_0F66_P_2) },
  },

  /* PREFIX_VEX_0F67 */
  {
    { Bad_Opcode },
    { Bad_Opcode },
    { VEX_LEN_TABLE (VEX_LEN_0F67_P_2) },
  },

  /* PREFIX_VEX_0F68 */
  {
    { Bad_Opcode },
    { Bad_Opcode },
    { VEX_LEN_TABLE (VEX_LEN_0F68_P_2) },
  },

  /* PREFIX_VEX_0F69 */
  {
    { Bad_Opcode },
    { Bad_Opcode },
    { VEX_LEN_TABLE (VEX_LEN_0F69_P_2) },
  },

  /* PREFIX_VEX_0F6A */
  {
    { Bad_Opcode },
    { Bad_Opcode },
    { VEX_LEN_TABLE (VEX_LEN_0F6A_P_2) },
  },

  /* PREFIX_VEX_0F6B */
  {
    { Bad_Opcode },
    { Bad_Opcode },
    { VEX_LEN_TABLE (VEX_LEN_0F6B_P_2) },
  },

  /* PREFIX_VEX_0F6C */
  {
    { Bad_Opcode },
    { Bad_Opcode },
    { VEX_LEN_TABLE (VEX_LEN_0F6C_P_2) },
  },

  /* PREFIX_VEX_0F6D */
  {
    { Bad_Opcode },
    { Bad_Opcode },
    { VEX_LEN_TABLE (VEX_LEN_0F6D_P_2) },
  },

  /* PREFIX_VEX_0F6E */
  {
    { Bad_Opcode },
    { Bad_Opcode },
    { VEX_LEN_TABLE (VEX_LEN_0F6E_P_2) },
  },

  /* PREFIX_VEX_0F6F */
  {
    { Bad_Opcode },
    { VEX_W_TABLE (VEX_W_0F6F_P_1) },
    { VEX_W_TABLE (VEX_W_0F6F_P_2) },
  },

  /* PREFIX_VEX_0F70 */
  {
    { Bad_Opcode },
    { VEX_LEN_TABLE (VEX_LEN_0F70_P_1) },
    { VEX_LEN_TABLE (VEX_LEN_0F70_P_2) },
    { VEX_LEN_TABLE (VEX_LEN_0F70_P_3) },
  },

  /* PREFIX_VEX_0F71_REG_2 */
  {
    { Bad_Opcode },
    { Bad_Opcode },
    { VEX_LEN_TABLE (VEX_LEN_0F71_R_2_P_2) },
  },

  /* PREFIX_VEX_0F71_REG_4 */
  {
    { Bad_Opcode },
    { Bad_Opcode },
    { VEX_LEN_TABLE (VEX_LEN_0F71_R_4_P_2) },
  },

  /* PREFIX_VEX_0F71_REG_6 */
  {
    { Bad_Opcode },
    { Bad_Opcode },
    { VEX_LEN_TABLE (VEX_LEN_0F71_R_6_P_2) },
  },

  /* PREFIX_VEX_0F72_REG_2 */
  {
    { Bad_Opcode },
    { Bad_Opcode },
    { VEX_LEN_TABLE (VEX_LEN_0F72_R_2_P_2) },
  },

  /* PREFIX_VEX_0F72_REG_4 */
  {
    { Bad_Opcode },
    { Bad_Opcode },
    { VEX_LEN_TABLE (VEX_LEN_0F72_R_4_P_2) },
  },

  /* PREFIX_VEX_0F72_REG_6 */
  {
    { Bad_Opcode },
    { Bad_Opcode },
    { VEX_LEN_TABLE (VEX_LEN_0F72_R_6_P_2) },
  },

  /* PREFIX_VEX_0F73_REG_2 */
  {
    { Bad_Opcode },
    { Bad_Opcode },
    { VEX_LEN_TABLE (VEX_LEN_0F73_R_2_P_2) },
  },

  /* PREFIX_VEX_0F73_REG_3 */
  {
    { Bad_Opcode },
    { Bad_Opcode },
    { VEX_LEN_TABLE (VEX_LEN_0F73_R_3_P_2) },
  },

  /* PREFIX_VEX_0F73_REG_6 */
  {
    { Bad_Opcode },
    { Bad_Opcode },
    { VEX_LEN_TABLE (VEX_LEN_0F73_R_6_P_2) },
  },

  /* PREFIX_VEX_0F73_REG_7 */
  {
    { Bad_Opcode },
    { Bad_Opcode },
    { VEX_LEN_TABLE (VEX_LEN_0F73_R_7_P_2) },
  },

  /* PREFIX_VEX_0F74 */
  {
    { Bad_Opcode },
    { Bad_Opcode },
    { VEX_LEN_TABLE (VEX_LEN_0F74_P_2) },
  },

  /* PREFIX_VEX_0F75 */
  {
    { Bad_Opcode },
    { Bad_Opcode },
    { VEX_LEN_TABLE (VEX_LEN_0F75_P_2) },
  },

  /* PREFIX_VEX_0F76 */
  {
    { Bad_Opcode },
    { Bad_Opcode },
    { VEX_LEN_TABLE (VEX_LEN_0F76_P_2) },
  },

  /* PREFIX_VEX_0F77 */
  {
    { VEX_W_TABLE (VEX_W_0F77_P_0) },
  },

  /* PREFIX_VEX_0F7C */
  {
    { Bad_Opcode },
    { Bad_Opcode },
    { VEX_W_TABLE (VEX_W_0F7C_P_2) },
    { VEX_W_TABLE (VEX_W_0F7C_P_3) },
  },

  /* PREFIX_VEX_0F7D */
  {
    { Bad_Opcode },
    { Bad_Opcode },
    { VEX_W_TABLE (VEX_W_0F7D_P_2) },
    { VEX_W_TABLE (VEX_W_0F7D_P_3) },
  },

  /* PREFIX_VEX_0F7E */
  {
    { Bad_Opcode },
    { VEX_LEN_TABLE (VEX_LEN_0F7E_P_1) },
    { VEX_LEN_TABLE (VEX_LEN_0F7E_P_2) },
  },

  /* PREFIX_VEX_0F7F */
  {
    { Bad_Opcode },
    { VEX_W_TABLE (VEX_W_0F7F_P_1) },
    { VEX_W_TABLE (VEX_W_0F7F_P_2) },
  },

  /* PREFIX_VEX_0FC2 */
  {
    { VEX_W_TABLE (VEX_W_0FC2_P_0) },
    { VEX_LEN_TABLE (VEX_LEN_0FC2_P_1) },
    { VEX_W_TABLE (VEX_W_0FC2_P_2) },
    { VEX_LEN_TABLE (VEX_LEN_0FC2_P_3) },
  },

  /* PREFIX_VEX_0FC4 */
  {
    { Bad_Opcode },
    { Bad_Opcode },
    { VEX_LEN_TABLE (VEX_LEN_0FC4_P_2) },
  },

  /* PREFIX_VEX_0FC5 */
  {
    { Bad_Opcode },
    { Bad_Opcode },
    { VEX_LEN_TABLE (VEX_LEN_0FC5_P_2) },
  },

  /* PREFIX_VEX_0FD0 */
  {
    { Bad_Opcode },
    { Bad_Opcode },
    { VEX_W_TABLE (VEX_W_0FD0_P_2) },
    { VEX_W_TABLE (VEX_W_0FD0_P_3) },
  },

  /* PREFIX_VEX_0FD1 */
  {
    { Bad_Opcode },
    { Bad_Opcode },
    { VEX_LEN_TABLE (VEX_LEN_0FD1_P_2) },
  },

  /* PREFIX_VEX_0FD2 */
  {
    { Bad_Opcode },
    { Bad_Opcode },
    { VEX_LEN_TABLE (VEX_LEN_0FD2_P_2) },
  },

  /* PREFIX_VEX_0FD3 */
  {
    { Bad_Opcode },
    { Bad_Opcode },
    { VEX_LEN_TABLE (VEX_LEN_0FD3_P_2) },
  },

  /* PREFIX_VEX_0FD4 */
  {
    { Bad_Opcode },
    { Bad_Opcode },
    { VEX_LEN_TABLE (VEX_LEN_0FD4_P_2) },
  },

  /* PREFIX_VEX_0FD5 */
  {
    { Bad_Opcode },
    { Bad_Opcode },
    { VEX_LEN_TABLE (VEX_LEN_0FD5_P_2) },
  },

  /* PREFIX_VEX_0FD6 */
  {
    { Bad_Opcode },
    { Bad_Opcode },
    { VEX_LEN_TABLE (VEX_LEN_0FD6_P_2) },
  },

  /* PREFIX_VEX_0FD7 */
  {
    { Bad_Opcode },
    { Bad_Opcode },
    { MOD_TABLE (MOD_VEX_0FD7_PREFIX_2) },
  },

  /* PREFIX_VEX_0FD8 */
  {
    { Bad_Opcode },
    { Bad_Opcode },
    { VEX_LEN_TABLE (VEX_LEN_0FD8_P_2) },
  },

  /* PREFIX_VEX_0FD9 */
  {
    { Bad_Opcode },
    { Bad_Opcode },
    { VEX_LEN_TABLE (VEX_LEN_0FD9_P_2) },
  },

  /* PREFIX_VEX_0FDA */
  {
    { Bad_Opcode },
    { Bad_Opcode },
    { VEX_LEN_TABLE (VEX_LEN_0FDA_P_2) },
  },

  /* PREFIX_VEX_0FDB */
  {
    { Bad_Opcode },
    { Bad_Opcode },
    { VEX_LEN_TABLE (VEX_LEN_0FDB_P_2) },
  },

  /* PREFIX_VEX_0FDC */
  {
    { Bad_Opcode },
    { Bad_Opcode },
    { VEX_LEN_TABLE (VEX_LEN_0FDC_P_2) },
  },

  /* PREFIX_VEX_0FDD */
  {
    { Bad_Opcode },
    { Bad_Opcode },
    { VEX_LEN_TABLE (VEX_LEN_0FDD_P_2) },
  },

  /* PREFIX_VEX_0FDE */
  {
    { Bad_Opcode },
    { Bad_Opcode },
    { VEX_LEN_TABLE (VEX_LEN_0FDE_P_2) },
  },

  /* PREFIX_VEX_0FDF */
  {
    { Bad_Opcode },
    { Bad_Opcode },
    { VEX_LEN_TABLE (VEX_LEN_0FDF_P_2) },
  },

  /* PREFIX_VEX_0FE0 */
  {
    { Bad_Opcode },
    { Bad_Opcode },
    { VEX_LEN_TABLE (VEX_LEN_0FE0_P_2) },
  },

  /* PREFIX_VEX_0FE1 */
  {
    { Bad_Opcode },
    { Bad_Opcode },
    { VEX_LEN_TABLE (VEX_LEN_0FE1_P_2) },
  },

  /* PREFIX_VEX_0FE2 */
  {
    { Bad_Opcode },
    { Bad_Opcode },
    { VEX_LEN_TABLE (VEX_LEN_0FE2_P_2) },
  },

  /* PREFIX_VEX_0FE3 */
  {
    { Bad_Opcode },
    { Bad_Opcode },
    { VEX_LEN_TABLE (VEX_LEN_0FE3_P_2) },
  },

  /* PREFIX_VEX_0FE4 */
  {
    { Bad_Opcode },
    { Bad_Opcode },
    { VEX_LEN_TABLE (VEX_LEN_0FE4_P_2) },
  },

  /* PREFIX_VEX_0FE5 */
  {
    { Bad_Opcode },
    { Bad_Opcode },
    { VEX_LEN_TABLE (VEX_LEN_0FE5_P_2) },
  },

  /* PREFIX_VEX_0FE6 */
  {
    { Bad_Opcode },
    { VEX_W_TABLE (VEX_W_0FE6_P_1) },
    { VEX_W_TABLE (VEX_W_0FE6_P_2) },
    { VEX_W_TABLE (VEX_W_0FE6_P_3) },
  },

  /* PREFIX_VEX_0FE7 */
  {
    { Bad_Opcode },
    { Bad_Opcode },
    { MOD_TABLE (MOD_VEX_0FE7_PREFIX_2) },
  },

  /* PREFIX_VEX_0FE8 */
  {
    { Bad_Opcode },
    { Bad_Opcode },
    { VEX_LEN_TABLE (VEX_LEN_0FE8_P_2) },
  },

  /* PREFIX_VEX_0FE9 */
  {
    { Bad_Opcode },
    { Bad_Opcode },
    { VEX_LEN_TABLE (VEX_LEN_0FE9_P_2) },
  },

  /* PREFIX_VEX_0FEA */
  {
    { Bad_Opcode },
    { Bad_Opcode },
    { VEX_LEN_TABLE (VEX_LEN_0FEA_P_2) },
  },

  /* PREFIX_VEX_0FEB */
  {
    { Bad_Opcode },
    { Bad_Opcode },
    { VEX_LEN_TABLE (VEX_LEN_0FEB_P_2) },
  },

  /* PREFIX_VEX_0FEC */
  {
    { Bad_Opcode },
    { Bad_Opcode },
    { VEX_LEN_TABLE (VEX_LEN_0FEC_P_2) },
  },

  /* PREFIX_VEX_0FED */
  {
    { Bad_Opcode },
    { Bad_Opcode },
    { VEX_LEN_TABLE (VEX_LEN_0FED_P_2) },
  },

  /* PREFIX_VEX_0FEE */
  {
    { Bad_Opcode },
    { Bad_Opcode },
    { VEX_LEN_TABLE (VEX_LEN_0FEE_P_2) },
  },

  /* PREFIX_VEX_0FEF */
  {
    { Bad_Opcode },
    { Bad_Opcode },
    { VEX_LEN_TABLE (VEX_LEN_0FEF_P_2) },
  },

  /* PREFIX_VEX_0FF0 */
  {
    { Bad_Opcode },
    { Bad_Opcode },
    { Bad_Opcode },
    { MOD_TABLE (MOD_VEX_0FF0_PREFIX_3) },
  },

  /* PREFIX_VEX_0FF1 */
  {
    { Bad_Opcode },
    { Bad_Opcode },
    { VEX_LEN_TABLE (VEX_LEN_0FF1_P_2) },
  },

  /* PREFIX_VEX_0FF2 */
  {
    { Bad_Opcode },
    { Bad_Opcode },
    { VEX_LEN_TABLE (VEX_LEN_0FF2_P_2) },
  },

  /* PREFIX_VEX_0FF3 */
  {
    { Bad_Opcode },
    { Bad_Opcode },
    { VEX_LEN_TABLE (VEX_LEN_0FF3_P_2) },
  },

  /* PREFIX_VEX_0FF4 */
  {
    { Bad_Opcode },
    { Bad_Opcode },
    { VEX_LEN_TABLE (VEX_LEN_0FF4_P_2) },
  },

  /* PREFIX_VEX_0FF5 */
  {
    { Bad_Opcode },
    { Bad_Opcode },
    { VEX_LEN_TABLE (VEX_LEN_0FF5_P_2) },
  },

  /* PREFIX_VEX_0FF6 */
  {
    { Bad_Opcode },
    { Bad_Opcode },
    { VEX_LEN_TABLE (VEX_LEN_0FF6_P_2) },
  },

  /* PREFIX_VEX_0FF7 */
  {
    { Bad_Opcode },
    { Bad_Opcode },
    { VEX_LEN_TABLE (VEX_LEN_0FF7_P_2) },
  },

  /* PREFIX_VEX_0FF8 */
  {
    { Bad_Opcode },
    { Bad_Opcode },
    { VEX_LEN_TABLE (VEX_LEN_0FF8_P_2) },
  },

  /* PREFIX_VEX_0FF9 */
  {
    { Bad_Opcode },
    { Bad_Opcode },
    { VEX_LEN_TABLE (VEX_LEN_0FF9_P_2) },
  },

  /* PREFIX_VEX_0FFA */
  {
    { Bad_Opcode },
    { Bad_Opcode },
    { VEX_LEN_TABLE (VEX_LEN_0FFA_P_2) },
  },

  /* PREFIX_VEX_0FFB */
  {
    { Bad_Opcode },
    { Bad_Opcode },
    { VEX_LEN_TABLE (VEX_LEN_0FFB_P_2) },
  },

  /* PREFIX_VEX_0FFC */
  {
    { Bad_Opcode },
    { Bad_Opcode },
    { VEX_LEN_TABLE (VEX_LEN_0FFC_P_2) },
  },

  /* PREFIX_VEX_0FFD */
  {
    { Bad_Opcode },
    { Bad_Opcode },
    { VEX_LEN_TABLE (VEX_LEN_0FFD_P_2) },
  },

  /* PREFIX_VEX_0FFE */
  {
    { Bad_Opcode },
    { Bad_Opcode },
    { VEX_LEN_TABLE (VEX_LEN_0FFE_P_2) },
  },

  /* PREFIX_VEX_0F3800 */
  {
    { Bad_Opcode },
    { Bad_Opcode },
    { VEX_LEN_TABLE (VEX_LEN_0F3800_P_2) },
  },

  /* PREFIX_VEX_0F3801 */
  {
    { Bad_Opcode },
    { Bad_Opcode },
    { VEX_LEN_TABLE (VEX_LEN_0F3801_P_2) },
  },

  /* PREFIX_VEX_0F3802 */
  {
    { Bad_Opcode },
    { Bad_Opcode },
    { VEX_LEN_TABLE (VEX_LEN_0F3802_P_2) },
  },

  /* PREFIX_VEX_0F3803 */
  {
    { Bad_Opcode },
    { Bad_Opcode },
    { VEX_LEN_TABLE (VEX_LEN_0F3803_P_2) },
  },

  /* PREFIX_VEX_0F3804 */
  {
    { Bad_Opcode },
    { Bad_Opcode },
    { VEX_LEN_TABLE (VEX_LEN_0F3804_P_2) },
  },

  /* PREFIX_VEX_0F3805 */
  {
    { Bad_Opcode },
    { Bad_Opcode },
    { VEX_LEN_TABLE (VEX_LEN_0F3805_P_2) },
  },

  /* PREFIX_VEX_0F3806 */
  {
    { Bad_Opcode },
    { Bad_Opcode },
    { VEX_LEN_TABLE (VEX_LEN_0F3806_P_2) },
  },

  /* PREFIX_VEX_0F3807 */
  {
    { Bad_Opcode },
    { Bad_Opcode },
    { VEX_LEN_TABLE (VEX_LEN_0F3807_P_2) },
  },

  /* PREFIX_VEX_0F3808 */
  {
    { Bad_Opcode },
    { Bad_Opcode },
    { VEX_LEN_TABLE (VEX_LEN_0F3808_P_2) },
  },

  /* PREFIX_VEX_0F3809 */
  {
    { Bad_Opcode },
    { Bad_Opcode },
    { VEX_LEN_TABLE (VEX_LEN_0F3809_P_2) },
  },

  /* PREFIX_VEX_0F380A */
  {
    { Bad_Opcode },
    { Bad_Opcode },
    { VEX_LEN_TABLE (VEX_LEN_0F380A_P_2) },
  },

  /* PREFIX_VEX_0F380B */
  {
    { Bad_Opcode },
    { Bad_Opcode },
    { VEX_LEN_TABLE (VEX_LEN_0F380B_P_2) },
  },

  /* PREFIX_VEX_0F380C */
  {
    { Bad_Opcode },
    { Bad_Opcode },
    { VEX_W_TABLE (VEX_W_0F380C_P_2) },
  },

  /* PREFIX_VEX_0F380D */
  {
    { Bad_Opcode },
    { Bad_Opcode },
    { VEX_W_TABLE (VEX_W_0F380D_P_2) },
  },

  /* PREFIX_VEX_0F380E */
  {
    { Bad_Opcode },
    { Bad_Opcode },
    { VEX_W_TABLE (VEX_W_0F380E_P_2) },
  },

  /* PREFIX_VEX_0F380F */
  {
    { Bad_Opcode },
    { Bad_Opcode },
    { VEX_W_TABLE (VEX_W_0F380F_P_2) },
  },

  /* PREFIX_VEX_0F3813 */
  {
    { Bad_Opcode },
    { Bad_Opcode },
    { "vcvtph2ps", { XM, EXxmmq } },
  },

  /* PREFIX_VEX_0F3817 */
  {
    { Bad_Opcode },
    { Bad_Opcode },
    { VEX_W_TABLE (VEX_W_0F3817_P_2) },
  },

  /* PREFIX_VEX_0F3818 */
  {
    { Bad_Opcode },
    { Bad_Opcode },
    { MOD_TABLE (MOD_VEX_0F3818_PREFIX_2) },
  },

  /* PREFIX_VEX_0F3819 */
  {
    { Bad_Opcode },
    { Bad_Opcode },
    { MOD_TABLE (MOD_VEX_0F3819_PREFIX_2) },
  },

  /* PREFIX_VEX_0F381A */
  {
    { Bad_Opcode },
    { Bad_Opcode },
    { MOD_TABLE (MOD_VEX_0F381A_PREFIX_2) },
  },

  /* PREFIX_VEX_0F381C */
  {
    { Bad_Opcode },
    { Bad_Opcode },
    { VEX_LEN_TABLE (VEX_LEN_0F381C_P_2) },
  },

  /* PREFIX_VEX_0F381D */
  {
    { Bad_Opcode },
    { Bad_Opcode },
    { VEX_LEN_TABLE (VEX_LEN_0F381D_P_2) },
  },

  /* PREFIX_VEX_0F381E */
  {
    { Bad_Opcode },
    { Bad_Opcode },
    { VEX_LEN_TABLE (VEX_LEN_0F381E_P_2) },
  },

  /* PREFIX_VEX_0F3820 */
  {
    { Bad_Opcode },
    { Bad_Opcode },
    { VEX_LEN_TABLE (VEX_LEN_0F3820_P_2) },
  },

  /* PREFIX_VEX_0F3821 */
  {
    { Bad_Opcode },
    { Bad_Opcode },
    { VEX_LEN_TABLE (VEX_LEN_0F3821_P_2) },
  },

  /* PREFIX_VEX_0F3822 */
  {
    { Bad_Opcode },
    { Bad_Opcode },
    { VEX_LEN_TABLE (VEX_LEN_0F3822_P_2) },
  },

  /* PREFIX_VEX_0F3823 */
  {
    { Bad_Opcode },
    { Bad_Opcode },
    { VEX_LEN_TABLE (VEX_LEN_0F3823_P_2) },
  },

  /* PREFIX_VEX_0F3824 */
  {
    { Bad_Opcode },
    { Bad_Opcode },
    { VEX_LEN_TABLE (VEX_LEN_0F3824_P_2) },
  },

  /* PREFIX_VEX_0F3825 */
  {
    { Bad_Opcode },
    { Bad_Opcode },
    { VEX_LEN_TABLE (VEX_LEN_0F3825_P_2) },
  },

  /* PREFIX_VEX_0F3828 */
  {
    { Bad_Opcode },
    { Bad_Opcode },
    { VEX_LEN_TABLE (VEX_LEN_0F3828_P_2) },
  },

  /* PREFIX_VEX_0F3829 */
  {
    { Bad_Opcode },
    { Bad_Opcode },
    { VEX_LEN_TABLE (VEX_LEN_0F3829_P_2) },
  },

  /* PREFIX_VEX_0F382A */
  {
    { Bad_Opcode },
    { Bad_Opcode },
    { MOD_TABLE (MOD_VEX_0F382A_PREFIX_2) },
  },

  /* PREFIX_VEX_0F382B */
  {
    { Bad_Opcode },
    { Bad_Opcode },
    { VEX_LEN_TABLE (VEX_LEN_0F382B_P_2) },
  },

  /* PREFIX_VEX_0F382C */
  {
    { Bad_Opcode },
    { Bad_Opcode },
     { MOD_TABLE (MOD_VEX_0F382C_PREFIX_2) },
  },

  /* PREFIX_VEX_0F382D */
  {
    { Bad_Opcode },
    { Bad_Opcode },
     { MOD_TABLE (MOD_VEX_0F382D_PREFIX_2) },
  },

  /* PREFIX_VEX_0F382E */
  {
    { Bad_Opcode },
    { Bad_Opcode },
     { MOD_TABLE (MOD_VEX_0F382E_PREFIX_2) },
  },

  /* PREFIX_VEX_0F382F */
  {
    { Bad_Opcode },
    { Bad_Opcode },
     { MOD_TABLE (MOD_VEX_0F382F_PREFIX_2) },
  },

  /* PREFIX_VEX_0F3830 */
  {
    { Bad_Opcode },
    { Bad_Opcode },
    { VEX_LEN_TABLE (VEX_LEN_0F3830_P_2) },
  },

  /* PREFIX_VEX_0F3831 */
  {
    { Bad_Opcode },
    { Bad_Opcode },
    { VEX_LEN_TABLE (VEX_LEN_0F3831_P_2) },
  },

  /* PREFIX_VEX_0F3832 */
  {
    { Bad_Opcode },
    { Bad_Opcode },
    { VEX_LEN_TABLE (VEX_LEN_0F3832_P_2) },
  },

  /* PREFIX_VEX_0F3833 */
  {
    { Bad_Opcode },
    { Bad_Opcode },
    { VEX_LEN_TABLE (VEX_LEN_0F3833_P_2) },
  },

  /* PREFIX_VEX_0F3834 */
  {
    { Bad_Opcode },
    { Bad_Opcode },
    { VEX_LEN_TABLE (VEX_LEN_0F3834_P_2) },
  },

  /* PREFIX_VEX_0F3835 */
  {
    { Bad_Opcode },
    { Bad_Opcode },
    { VEX_LEN_TABLE (VEX_LEN_0F3835_P_2) },
  },

  /* PREFIX_VEX_0F3837 */
  {
    { Bad_Opcode },
    { Bad_Opcode },
    { VEX_LEN_TABLE (VEX_LEN_0F3837_P_2) },
  },

  /* PREFIX_VEX_0F3838 */
  {
    { Bad_Opcode },
    { Bad_Opcode },
    { VEX_LEN_TABLE (VEX_LEN_0F3838_P_2) },
  },

  /* PREFIX_VEX_0F3839 */
  {
    { Bad_Opcode },
    { Bad_Opcode },
    { VEX_LEN_TABLE (VEX_LEN_0F3839_P_2) },
  },

  /* PREFIX_VEX_0F383A */
  {
    { Bad_Opcode },
    { Bad_Opcode },
    { VEX_LEN_TABLE (VEX_LEN_0F383A_P_2) },
  },

  /* PREFIX_VEX_0F383B */
  {
    { Bad_Opcode },
    { Bad_Opcode },
    { VEX_LEN_TABLE (VEX_LEN_0F383B_P_2) },
  },

  /* PREFIX_VEX_0F383C */
  {
    { Bad_Opcode },
    { Bad_Opcode },
    { VEX_LEN_TABLE (VEX_LEN_0F383C_P_2) },
  },

  /* PREFIX_VEX_0F383D */
  {
    { Bad_Opcode },
    { Bad_Opcode },
    { VEX_LEN_TABLE (VEX_LEN_0F383D_P_2) },
  },

  /* PREFIX_VEX_0F383E */
  {
    { Bad_Opcode },
    { Bad_Opcode },
    { VEX_LEN_TABLE (VEX_LEN_0F383E_P_2) },
  },

  /* PREFIX_VEX_0F383F */
  {
    { Bad_Opcode },
    { Bad_Opcode },
    { VEX_LEN_TABLE (VEX_LEN_0F383F_P_2) },
  },

  /* PREFIX_VEX_0F3840 */
  {
    { Bad_Opcode },
    { Bad_Opcode },
    { VEX_LEN_TABLE (VEX_LEN_0F3840_P_2) },
  },

  /* PREFIX_VEX_0F3841 */
  {
    { Bad_Opcode },
    { Bad_Opcode },
    { VEX_LEN_TABLE (VEX_LEN_0F3841_P_2) },
  },

  /* PREFIX_VEX_0F3896 */
  {
    { Bad_Opcode },
    { Bad_Opcode },
    { "vfmaddsub132p%XW", { XM, Vex, EXx } },
  },

  /* PREFIX_VEX_0F3897 */
  {
    { Bad_Opcode },
    { Bad_Opcode },
    { "vfmsubadd132p%XW", { XM, Vex, EXx } },
  },

  /* PREFIX_VEX_0F3898 */
  {
    { Bad_Opcode },
    { Bad_Opcode },
    { "vfmadd132p%XW", { XM, Vex, EXx } },
  },

  /* PREFIX_VEX_0F3899 */
  {
    { Bad_Opcode },
    { Bad_Opcode },
    { "vfmadd132s%XW", { XMScalar, VexScalar, EXVexWdqScalar } },
  },

  /* PREFIX_VEX_0F389A */
  {
    { Bad_Opcode },
    { Bad_Opcode },
    { "vfmsub132p%XW", { XM, Vex, EXx } },
  },

  /* PREFIX_VEX_0F389B */
  {
    { Bad_Opcode },
    { Bad_Opcode },
    { "vfmsub132s%XW", { XMScalar, VexScalar, EXVexWdqScalar } },
  },

  /* PREFIX_VEX_0F389C */
  {
    { Bad_Opcode },
    { Bad_Opcode },
    { "vfnmadd132p%XW", { XM, Vex, EXx } },
  },

  /* PREFIX_VEX_0F389D */
  {
    { Bad_Opcode },
    { Bad_Opcode },
    { "vfnmadd132s%XW", { XMScalar, VexScalar, EXVexWdqScalar } },
  },

  /* PREFIX_VEX_0F389E */
  {
    { Bad_Opcode },
    { Bad_Opcode },
    { "vfnmsub132p%XW", { XM, Vex, EXx } },
  },

  /* PREFIX_VEX_0F389F */
  {
    { Bad_Opcode },
    { Bad_Opcode },
    { "vfnmsub132s%XW", { XMScalar, VexScalar, EXVexWdqScalar } },
  },

  /* PREFIX_VEX_0F38A6 */
  {
    { Bad_Opcode },
    { Bad_Opcode },
    { "vfmaddsub213p%XW", { XM, Vex, EXx } },
    { Bad_Opcode },
  },

  /* PREFIX_VEX_0F38A7 */
  {
    { Bad_Opcode },
    { Bad_Opcode },
    { "vfmsubadd213p%XW", { XM, Vex, EXx } },
  },

  /* PREFIX_VEX_0F38A8 */
  {
    { Bad_Opcode },
    { Bad_Opcode },
    { "vfmadd213p%XW", { XM, Vex, EXx } },
  },

  /* PREFIX_VEX_0F38A9 */
  {
    { Bad_Opcode },
    { Bad_Opcode },
    { "vfmadd213s%XW", { XMScalar, VexScalar, EXVexWdqScalar } },
  },

  /* PREFIX_VEX_0F38AA */
  {
    { Bad_Opcode },
    { Bad_Opcode },
    { "vfmsub213p%XW", { XM, Vex, EXx } },
  },

  /* PREFIX_VEX_0F38AB */
  {
    { Bad_Opcode },
    { Bad_Opcode },
    { "vfmsub213s%XW", { XMScalar, VexScalar, EXVexWdqScalar } },
  },

  /* PREFIX_VEX_0F38AC */
  {
    { Bad_Opcode },
    { Bad_Opcode },
    { "vfnmadd213p%XW", { XM, Vex, EXx } },
  },

  /* PREFIX_VEX_0F38AD */
  {
    { Bad_Opcode },
    { Bad_Opcode },
    { "vfnmadd213s%XW", { XMScalar, VexScalar, EXVexWdqScalar } },
  },

  /* PREFIX_VEX_0F38AE */
  {
    { Bad_Opcode },
    { Bad_Opcode },
    { "vfnmsub213p%XW", { XM, Vex, EXx } },
  },

  /* PREFIX_VEX_0F38AF */
  {
    { Bad_Opcode },
    { Bad_Opcode },
    { "vfnmsub213s%XW", { XMScalar, VexScalar, EXVexWdqScalar } },
  },

  /* PREFIX_VEX_0F38B6 */
  {
    { Bad_Opcode },
    { Bad_Opcode },
    { "vfmaddsub231p%XW", { XM, Vex, EXx } },
  },

  /* PREFIX_VEX_0F38B7 */
  {
    { Bad_Opcode },
    { Bad_Opcode },
    { "vfmsubadd231p%XW", { XM, Vex, EXx } },
  },

  /* PREFIX_VEX_0F38B8 */
  {
    { Bad_Opcode },
    { Bad_Opcode },
    { "vfmadd231p%XW", { XM, Vex, EXx } },
  },

  /* PREFIX_VEX_0F38B9 */
  {
    { Bad_Opcode },
    { Bad_Opcode },
    { "vfmadd231s%XW", { XMScalar, VexScalar, EXVexWdqScalar } },
  },

  /* PREFIX_VEX_0F38BA */
  {
    { Bad_Opcode },
    { Bad_Opcode },
    { "vfmsub231p%XW", { XM, Vex, EXx } },
  },

  /* PREFIX_VEX_0F38BB */
  {
    { Bad_Opcode },
    { Bad_Opcode },
    { "vfmsub231s%XW", { XMScalar, VexScalar, EXVexWdqScalar } },
  },

  /* PREFIX_VEX_0F38BC */
  {
    { Bad_Opcode },
    { Bad_Opcode },
    { "vfnmadd231p%XW", { XM, Vex, EXx } },
  },

  /* PREFIX_VEX_0F38BD */
  {
    { Bad_Opcode },
    { Bad_Opcode },
    { "vfnmadd231s%XW", { XMScalar, VexScalar, EXVexWdqScalar } },
  },

  /* PREFIX_VEX_0F38BE */
  {
    { Bad_Opcode },
    { Bad_Opcode },
    { "vfnmsub231p%XW", { XM, Vex, EXx } },
  },

  /* PREFIX_VEX_0F38BF */
  {
    { Bad_Opcode },
    { Bad_Opcode },
    { "vfnmsub231s%XW", { XMScalar, VexScalar, EXVexWdqScalar } },
  },

  /* PREFIX_VEX_0F38DB */
  {
    { Bad_Opcode },
    { Bad_Opcode },
    { VEX_LEN_TABLE (VEX_LEN_0F38DB_P_2) },
  },

  /* PREFIX_VEX_0F38DC */
  {
    { Bad_Opcode },
    { Bad_Opcode },
    { VEX_LEN_TABLE (VEX_LEN_0F38DC_P_2) },
  },

  /* PREFIX_VEX_0F38DD */
  {
    { Bad_Opcode },
    { Bad_Opcode },
    { VEX_LEN_TABLE (VEX_LEN_0F38DD_P_2) },
  },

  /* PREFIX_VEX_0F38DE */
  {
    { Bad_Opcode },
    { Bad_Opcode },
    { VEX_LEN_TABLE (VEX_LEN_0F38DE_P_2) },
  },

  /* PREFIX_VEX_0F38DF */
  {
    { Bad_Opcode },
    { Bad_Opcode },
    { VEX_LEN_TABLE (VEX_LEN_0F38DF_P_2) },
  },

  /* PREFIX_VEX_0F3A04 */
  {
    { Bad_Opcode },
    { Bad_Opcode },
    { VEX_W_TABLE (VEX_W_0F3A04_P_2) },
  },

  /* PREFIX_VEX_0F3A05 */
  {
    { Bad_Opcode },
    { Bad_Opcode },
    { VEX_W_TABLE (VEX_W_0F3A05_P_2) },
  },

  /* PREFIX_VEX_0F3A06 */
  {
    { Bad_Opcode },
    { Bad_Opcode },
    { VEX_LEN_TABLE (VEX_LEN_0F3A06_P_2) },
  },

  /* PREFIX_VEX_0F3A08 */
  {
    { Bad_Opcode },
    { Bad_Opcode },
    { VEX_W_TABLE (VEX_W_0F3A08_P_2) },
  },

  /* PREFIX_VEX_0F3A09 */
  {
    { Bad_Opcode },
    { Bad_Opcode },
    { VEX_W_TABLE (VEX_W_0F3A09_P_2) },
  },

  /* PREFIX_VEX_0F3A0A */
  {
    { Bad_Opcode },
    { Bad_Opcode },
    { VEX_LEN_TABLE (VEX_LEN_0F3A0A_P_2) },
  },

  /* PREFIX_VEX_0F3A0B */
  {
    { Bad_Opcode },
    { Bad_Opcode },
    { VEX_LEN_TABLE (VEX_LEN_0F3A0B_P_2) },
  },

  /* PREFIX_VEX_0F3A0C */
  {
    { Bad_Opcode },
    { Bad_Opcode },
    { VEX_W_TABLE (VEX_W_0F3A0C_P_2) },
  },

  /* PREFIX_VEX_0F3A0D */
  {
    { Bad_Opcode },
    { Bad_Opcode },
    { VEX_W_TABLE (VEX_W_0F3A0D_P_2) },
  },

  /* PREFIX_VEX_0F3A0E */
  {
    { Bad_Opcode },
    { Bad_Opcode },
    { VEX_LEN_TABLE (VEX_LEN_0F3A0E_P_2) },
  },

  /* PREFIX_VEX_0F3A0F */
  {
    { Bad_Opcode },
    { Bad_Opcode },
    { VEX_LEN_TABLE (VEX_LEN_0F3A0F_P_2) },
  },

  /* PREFIX_VEX_0F3A14 */
  {
    { Bad_Opcode },
    { Bad_Opcode },
    { VEX_LEN_TABLE (VEX_LEN_0F3A14_P_2) },
  },

  /* PREFIX_VEX_0F3A15 */
  {
    { Bad_Opcode },
    { Bad_Opcode },
    { VEX_LEN_TABLE (VEX_LEN_0F3A15_P_2) },
  },

  /* PREFIX_VEX_0F3A16 */
  {
    { Bad_Opcode },
    { Bad_Opcode },
    { VEX_LEN_TABLE (VEX_LEN_0F3A16_P_2) },
  },

  /* PREFIX_VEX_0F3A17 */
  {
    { Bad_Opcode },
    { Bad_Opcode },
    { VEX_LEN_TABLE (VEX_LEN_0F3A17_P_2) },
  },

  /* PREFIX_VEX_0F3A18 */
  {
    { Bad_Opcode },
    { Bad_Opcode },
    { VEX_LEN_TABLE (VEX_LEN_0F3A18_P_2) },
  },

  /* PREFIX_VEX_0F3A19 */
  {
    { Bad_Opcode },
    { Bad_Opcode },
    { VEX_LEN_TABLE (VEX_LEN_0F3A19_P_2) },
  },

  /* PREFIX_VEX_0F3A1D */
  {
    { Bad_Opcode },
    { Bad_Opcode },
    { "vcvtps2ph", { EXxmmq, XM, Ib } },
  },

  /* PREFIX_VEX_0F3A20 */
  {
    { Bad_Opcode },
    { Bad_Opcode },
    { VEX_LEN_TABLE (VEX_LEN_0F3A20_P_2) },
  },

  /* PREFIX_VEX_0F3A21 */
  {
    { Bad_Opcode },
    { Bad_Opcode },
    { VEX_LEN_TABLE (VEX_LEN_0F3A21_P_2) },
  },

  /* PREFIX_VEX_0F3A22 */
  {
    { Bad_Opcode },
    { Bad_Opcode },
    { VEX_LEN_TABLE (VEX_LEN_0F3A22_P_2) },
  },

  /* PREFIX_VEX_0F3A40 */
  {
    { Bad_Opcode },
    { Bad_Opcode },
    { VEX_W_TABLE (VEX_W_0F3A40_P_2) },
  },

  /* PREFIX_VEX_0F3A41 */
  {
    { Bad_Opcode },
    { Bad_Opcode },
    { VEX_LEN_TABLE (VEX_LEN_0F3A41_P_2) },
  },

  /* PREFIX_VEX_0F3A42 */
  {
    { Bad_Opcode },
    { Bad_Opcode },
    { VEX_LEN_TABLE (VEX_LEN_0F3A42_P_2) },
  },

  /* PREFIX_VEX_0F3A44 */
  {
    { Bad_Opcode },
    { Bad_Opcode },
    { VEX_LEN_TABLE (VEX_LEN_0F3A44_P_2) },
  },

  /* PREFIX_VEX_0F3A48 */
  {
    { Bad_Opcode },
    { Bad_Opcode },
    { VEX_W_TABLE (VEX_W_0F3A48_P_2) },
  },

  /* PREFIX_VEX_0F3A49 */
  {
    { Bad_Opcode },
    { Bad_Opcode },
    { VEX_W_TABLE (VEX_W_0F3A49_P_2) },
  },

  /* PREFIX_VEX_0F3A4A */
  {
    { Bad_Opcode },
    { Bad_Opcode },
    { VEX_W_TABLE (VEX_W_0F3A4A_P_2) },
  },

  /* PREFIX_VEX_0F3A4B */
  {
    { Bad_Opcode },
    { Bad_Opcode },
    { VEX_W_TABLE (VEX_W_0F3A4B_P_2) },
  },

  /* PREFIX_VEX_0F3A4C */
  {
    { Bad_Opcode },
    { Bad_Opcode },
    { VEX_LEN_TABLE (VEX_LEN_0F3A4C_P_2) },
  },

  /* PREFIX_VEX_0F3A5C */
  {
    { Bad_Opcode },
    { Bad_Opcode },
    { "vfmaddsubps", { XMVexW, Vex, EXVexW, EXVexW, VexI4 } },
  },

  /* PREFIX_VEX_0F3A5D */
  {
    { Bad_Opcode },
    { Bad_Opcode },
    { "vfmaddsubpd", { XMVexW, Vex, EXVexW, EXVexW, VexI4 } },
  },

  /* PREFIX_VEX_0F3A5E */
  {
    { Bad_Opcode },
    { Bad_Opcode },
    { "vfmsubaddps", { XMVexW, Vex, EXVexW, EXVexW, VexI4 } },
  },

  /* PREFIX_VEX_0F3A5F */
  {
    { Bad_Opcode },
    { Bad_Opcode },
    { "vfmsubaddpd", { XMVexW, Vex, EXVexW, EXVexW, VexI4 } },
  },

  /* PREFIX_VEX_0F3A60 */
  {
    { Bad_Opcode },
    { Bad_Opcode },
    { VEX_LEN_TABLE (VEX_LEN_0F3A60_P_2) },
    { Bad_Opcode },
  },

  /* PREFIX_VEX_0F3A61 */
  {
    { Bad_Opcode },
    { Bad_Opcode },
    { VEX_LEN_TABLE (VEX_LEN_0F3A61_P_2) },
  },

  /* PREFIX_VEX_0F3A62 */
  {
    { Bad_Opcode },
    { Bad_Opcode },
    { VEX_LEN_TABLE (VEX_LEN_0F3A62_P_2) },
  },

  /* PREFIX_VEX_0F3A63 */
  {
    { Bad_Opcode },
    { Bad_Opcode },
    { VEX_LEN_TABLE (VEX_LEN_0F3A63_P_2) },
  },

  /* PREFIX_VEX_0F3A68 */
  {
    { Bad_Opcode },
    { Bad_Opcode },
    { "vfmaddps", { XMVexW, Vex, EXVexW, EXVexW, VexI4 } },
  },

  /* PREFIX_VEX_0F3A69 */
  {
    { Bad_Opcode },
    { Bad_Opcode },
    { "vfmaddpd", { XMVexW, Vex, EXVexW, EXVexW, VexI4 } },
  },

  /* PREFIX_VEX_0F3A6A */
  {
    { Bad_Opcode },
    { Bad_Opcode },
    { VEX_LEN_TABLE (VEX_LEN_0F3A6A_P_2) },
  },

  /* PREFIX_VEX_0F3A6B */
  {
    { Bad_Opcode },
    { Bad_Opcode },
    { VEX_LEN_TABLE (VEX_LEN_0F3A6B_P_2) },
  },

  /* PREFIX_VEX_0F3A6C */
  {
    { Bad_Opcode },
    { Bad_Opcode },
    { "vfmsubps", { XMVexW, Vex, EXVexW, EXVexW, VexI4 } },
  },

  /* PREFIX_VEX_0F3A6D */
  {
    { Bad_Opcode },
    { Bad_Opcode },
    { "vfmsubpd", { XMVexW, Vex, EXVexW, EXVexW, VexI4 } },
  },

  /* PREFIX_VEX_0F3A6E */
  {
    { Bad_Opcode },
    { Bad_Opcode },
    { VEX_LEN_TABLE (VEX_LEN_0F3A6E_P_2) },
  },

  /* PREFIX_VEX_0F3A6F */
  {
    { Bad_Opcode },
    { Bad_Opcode },
    { VEX_LEN_TABLE (VEX_LEN_0F3A6F_P_2) },
  },

  /* PREFIX_VEX_0F3A78 */
  {
    { Bad_Opcode },
    { Bad_Opcode },
    { "vfnmaddps", { XMVexW, Vex, EXVexW, EXVexW, VexI4 } },
  },

  /* PREFIX_VEX_0F3A79 */
  {
    { Bad_Opcode },
    { Bad_Opcode },
    { "vfnmaddpd", { XMVexW, Vex, EXVexW, EXVexW, VexI4 } },
  },

  /* PREFIX_VEX_0F3A7A */
  {
    { Bad_Opcode },
    { Bad_Opcode },
    { VEX_LEN_TABLE (VEX_LEN_0F3A7A_P_2) },
  },

  /* PREFIX_VEX_0F3A7B */
  {
    { Bad_Opcode },
    { Bad_Opcode },
    { VEX_LEN_TABLE (VEX_LEN_0F3A7B_P_2) },
  },

  /* PREFIX_VEX_0F3A7C */
  {
    { Bad_Opcode },
    { Bad_Opcode },
    { "vfnmsubps", { XMVexW, Vex, EXVexW, EXVexW, VexI4 } },
    { Bad_Opcode },
  },

  /* PREFIX_VEX_0F3A7D */
  {
    { Bad_Opcode },
    { Bad_Opcode },
    { "vfnmsubpd", { XMVexW, Vex, EXVexW, EXVexW, VexI4 } },
  },

  /* PREFIX_VEX_0F3A7E */
  {
    { Bad_Opcode },
    { Bad_Opcode },
    { VEX_LEN_TABLE (VEX_LEN_0F3A7E_P_2) },
  },

  /* PREFIX_VEX_0F3A7F */
  {
    { Bad_Opcode },
    { Bad_Opcode },
    { VEX_LEN_TABLE (VEX_LEN_0F3A7F_P_2) },
  },

  /* PREFIX_VEX_0F3ADF */
  {
    { Bad_Opcode },
    { Bad_Opcode },
    { VEX_LEN_TABLE (VEX_LEN_0F3ADF_P_2) },
  },
};

const struct dis386 x86_64_table[][2] = {
  /* X86_64_06 */
  {
    { "pushP", { es } },
  },

  /* X86_64_07 */
  {
    { "popP", { es } },
  },

  /* X86_64_0D */
  {
    { "pushP", { cs } },
  },

  /* X86_64_16 */
  {
    { "pushP", { ss } },
  },

  /* X86_64_17 */
  {
    { "popP", { ss } },
  },

  /* X86_64_1E */
  {
    { "pushP", { ds } },
  },

  /* X86_64_1F */
  {
    { "popP", { ds } },
  },

  /* X86_64_27 */
  {
    { "daa", { XX } },
  },

  /* X86_64_2F */
  {
    { "das", { XX } },
  },

  /* X86_64_37 */
  {
    { "aaa", { XX } },
  },

  /* X86_64_3F */
  {
    { "aas", { XX } },
  },

  /* X86_64_60 */
  {
    { "pushaP", { XX } },
  },

  /* X86_64_61 */
  {
    { "popaP", { XX } },
  },

  /* X86_64_62 */
  {
    { MOD_TABLE (MOD_62_32BIT) },
  },

  /* X86_64_63 */
  {
    { "arpl", { Ew, Gw } },
    { "movs{lq|xd}", { Gv, Ed } },
  },

  /* X86_64_6D */
  {
    { "ins{R|}", { Yzr, indirDX } },
    { "ins{G|}", { Yzr, indirDX } },
  },

  /* X86_64_6F */
  {
    { "outs{R|}", { indirDXr, Xz } },
    { "outs{G|}", { indirDXr, Xz } },
  },

  /* X86_64_9A */
  {
    { "Jcall{T|}", { Ap } },
  },

  /* X86_64_C4 */
  {
    { MOD_TABLE (MOD_C4_32BIT) },
    { VEX_C4_TABLE (VEX_0F) },
  },

  /* X86_64_C5 */
  {
    { MOD_TABLE (MOD_C5_32BIT) },
    { VEX_C5_TABLE (VEX_0F) },
  },

  /* X86_64_CE */
  {
    { "into", { XX } },
  },

  /* X86_64_D4 */
  {
    { "aam", { sIb } },
  },

  /* X86_64_D5 */
  {
    { "aad", { sIb } },
  },

  /* X86_64_EA */
  {
    { "Jjmp{T|}", { Ap } },
  },

  /* X86_64_0F01_REG_0 */
  {
    { "sgdt{Q|IQ}", { M } },
    { "sgdt", { M } },
  },

  /* X86_64_0F01_REG_1 */
  {
    { "sidt{Q|IQ}", { M } },
    { "sidt", { M } },
  },

  /* X86_64_0F01_REG_2 */
  {
    { "lgdt{Q|Q}", { M } },
    { "lgdt", { M } },
  },

  /* X86_64_0F01_REG_3 */
  {
    { "lidt{Q|Q}", { M } },
    { "lidt", { M } },
  },
};
