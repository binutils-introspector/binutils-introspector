#include "i386-dis-std-inc.h"
using namespace std;


#include <algorithm>
#include <fstream>
#include <iterator>
#include <iostream>
#include <vector>

namespace binutils_objdump_opcodes_i386
{

  // refactoring of macro
  void DOSAVE_LAST(array<char,4> & last , unsigned int & l,unsigned int len, char c )				
  {
    if (l < len && l < last.size())					
      last[l++] = c;							
    else									
      {									
	cerr << "bad length " << l << " and " << len << " and " << last.size() <<endl; 
	abort ();								
      }
  }

#define SAVE_LAST(c)   DOSAVE_LAST(last,l,len,c);

/* Capital letters in template are macros.  */
int
StaticData::putop (const char *in_template, int sizeflag)
{
  const char *p;
  int alt = 0;
  int cond = 1;
  unsigned int l = 0, len = 1;

  typedef array<char,4> t_last_r;
  t_last_r last;

  cerr << "checking :" << in_template << endl;
  for (p = in_template; *p; p++)
    {

      switch (*p)
	{
	default:
	  *obufp++ = *p;
	  break;
	case '%':
	  len++;
	  break;
	case '!':
	  cond = 0;
	  break;
	case '{':
	  alt = 0;
	  if (intel_syntax)
	    {
	      while (*++p != '|')
		if (*p == '}' || *p == '\0')
		  {
		    cerr << "bad syntax" << endl;
		    abort ();
		  }
	    }
	  /* Fall through.  */
	case 'I':
	  alt = 1;
	  continue;
	case '|':
	  while (*++p != '}')
	    {
	      if (*p == '\0')
		{
		  cerr << "bad data" << endl;
		  abort ();
		}
	    }
	  break;
	case '}':
	  break;
	case 'A':
	  if (intel_syntax)
	    break;
	  if (modrm.mod != 3 || (sizeflag & SUFFIX_ALWAYS))
	    *obufp++ = 'b';
	  break;
	case 'B':
	  if (l == 0 && len == 1)
	    {
case_B:
	      if (intel_syntax)
		break;
	      if (sizeflag & SUFFIX_ALWAYS)
		*obufp++ = 'b';
	    }
	  else
	    {
	      if (l != 1
		  || len != 2
		  || last[0] != 'L')
		{
		  SAVE_LAST (*p);
		  break;
		}

	      if (address_mode == mode_64bit
		  && !(prefixes & PREFIX_ADDR))
		{
		  *obufp++ = 'a';
		  *obufp++ = 'b';
		  *obufp++ = 's';
		}

	      goto case_B;
	    }
	  break;
	case 'C':
	  if (intel_syntax && !alt)
	    break;
	  if ((prefixes & PREFIX_DATA) || (sizeflag & SUFFIX_ALWAYS))
	    {
	      if (sizeflag & DFLAG)
		*obufp++ = intel_syntax ? 'd' : 'l';
	      else
		*obufp++ = intel_syntax ? 'w' : 's';
	      used_prefixes |= (prefixes & PREFIX_DATA);
	    }
	  break;
	case 'D':
	  if (intel_syntax || !(sizeflag & SUFFIX_ALWAYS))
	    break;
	  USED_REX (REX_W);
	  if (modrm.mod == 3)
	    {
	      if (rex & REX_W)
		*obufp++ = 'q';
	      else
		{
		  if (sizeflag & DFLAG)
		    *obufp++ = intel_syntax ? 'd' : 'l';
		  else
		    *obufp++ = 'w';
		  used_prefixes |= (prefixes & PREFIX_DATA);
		}
	    }
	  else
	    *obufp++ = 'w';
	  break;
	case 'E':		/* For jcxz/jecxz */
	  if (address_mode == mode_64bit)
	    {
	      if (sizeflag & AFLAG)
		*obufp++ = 'r';
	      else
		*obufp++ = 'e';
	    }
	  else
	    if (sizeflag & AFLAG)
	      *obufp++ = 'e';
	  used_prefixes |= (prefixes & PREFIX_ADDR);
	  break;
	case 'F':
	  if (intel_syntax)
	    break;
	  if ((prefixes & PREFIX_ADDR) || (sizeflag & SUFFIX_ALWAYS))
	    {
	      if (sizeflag & AFLAG)
		*obufp++ = address_mode == mode_64bit ? 'q' : 'l';
	      else
		*obufp++ = address_mode == mode_64bit ? 'l' : 'w';
	      used_prefixes |= (prefixes & PREFIX_ADDR);
	    }
	  break;
	case 'G':
	  if (intel_syntax || (obufp[-1] != 's' && !(sizeflag & SUFFIX_ALWAYS)))
	    break;
	  if ((rex & REX_W) || (sizeflag & DFLAG))
	    *obufp++ = 'l';
	  else
	    *obufp++ = 'w';
	  if (!(rex & REX_W))
	    used_prefixes |= (prefixes & PREFIX_DATA);
	  break;
	case 'H':
	  if (intel_syntax)
	    break;
	  if ((prefixes & (PREFIX_CS | PREFIX_DS)) == PREFIX_CS
	      || (prefixes & (PREFIX_CS | PREFIX_DS)) == PREFIX_DS)
	    {
	      used_prefixes |= prefixes & (PREFIX_CS | PREFIX_DS);
	      *obufp++ = ',';
	      *obufp++ = 'p';
	      if (prefixes & PREFIX_DS)
		*obufp++ = 't';
	      else
		*obufp++ = 'n';
	    }
	  break;
	case 'J':
	  if (intel_syntax)
	    break;
	  *obufp++ = 'l';
	  break;
	case 'K':
	  USED_REX (REX_W);
	  if (rex & REX_W)
	    *obufp++ = 'q';
	  else
	    *obufp++ = 'd';
	  break;
	case 'Z':
	  if (intel_syntax)
	    break;
	  if (address_mode == mode_64bit && (sizeflag & SUFFIX_ALWAYS))
	    {
	      *obufp++ = 'q';
	      break;
	    }
	  /* Fall through.  */
	  goto case_L;
	case 'L':
	  if (l != 0 || len != 1)
	    {
	      SAVE_LAST (*p);
	      break;
	    }
case_L:
	  if (intel_syntax)
	    break;
	  if (sizeflag & SUFFIX_ALWAYS)
	    *obufp++ = 'l';
	  break;
	case 'M':
	  if (intel_mnemonic != cond)
	    *obufp++ = 'r';
	  break;
	case 'N':
	  if ((prefixes & PREFIX_FWAIT) == 0)
	    *obufp++ = 'n';
	  else
	    used_prefixes |= PREFIX_FWAIT;
	  break;
	case 'O':
	  USED_REX (REX_W);
	  if (rex & REX_W)
	    *obufp++ = 'o';
	  else if (intel_syntax && (sizeflag & DFLAG))
	    *obufp++ = 'q';
	  else
	    *obufp++ = 'd';
	  if (!(rex & REX_W))
	    used_prefixes |= (prefixes & PREFIX_DATA);
	  break;
	case 'T':
	  if (!intel_syntax
	      && address_mode == mode_64bit
	      && (sizeflag & DFLAG))
	    {
	      *obufp++ = 'q';
	      break;
	    }
	  /* Fall through.  */
	case 'P':
	  if (intel_syntax)
	    {
	      if ((rex & REX_W) == 0
		  && (prefixes & PREFIX_DATA))
		{
		  if ((sizeflag & DFLAG) == 0)
		    *obufp++ = 'w';
		   used_prefixes |= (prefixes & PREFIX_DATA);
		}
	      break;
	    }
	  if ((prefixes & PREFIX_DATA)
	      || (rex & REX_W)
	      || (sizeflag & SUFFIX_ALWAYS))
	    {
	      USED_REX (REX_W);
	      if (rex & REX_W)
		*obufp++ = 'q';
	      else
		{
		   if (sizeflag & DFLAG)
		      *obufp++ = 'l';
		   else
		     *obufp++ = 'w';
		   used_prefixes |= (prefixes & PREFIX_DATA);
		}
	    }
	  break;
	case 'U':
	  if (intel_syntax)
	    break;
	  if (address_mode == mode_64bit && (sizeflag & DFLAG))
	    {
	      if (modrm.mod != 3 || (sizeflag & SUFFIX_ALWAYS))
		*obufp++ = 'q';
	      break;
	    }
	  /* Fall through.  */
	  goto case_Q;
	case 'Q':
	  if (l == 0 && len == 1)
	    {
case_Q:
	      if (intel_syntax && !alt)
		break;
	      USED_REX (REX_W);
	      if (modrm.mod != 3 || (sizeflag & SUFFIX_ALWAYS))
		{
		  if (rex & REX_W)
		    *obufp++ = 'q';
		  else
		    {
		      if (sizeflag & DFLAG)
			*obufp++ = intel_syntax ? 'd' : 'l';
		      else
			*obufp++ = 'w';
		      used_prefixes |= (prefixes & PREFIX_DATA);
		    }
		}
	    }
	  else
	    {
	      if (l != 1 || len != 2 || last[0] != 'L')
		{
		  SAVE_LAST (*p);
		  break;
		}
	      if (intel_syntax
		  || (modrm.mod == 3 && !(sizeflag & SUFFIX_ALWAYS)))
		break;
	      if ((rex & REX_W))
		{
		  USED_REX (REX_W);
		  *obufp++ = 'q';
		}
	      else
		*obufp++ = 'l';
	    }
	  break;
	case 'R':
	  USED_REX (REX_W);
	  if (rex & REX_W)
	    *obufp++ = 'q';
	  else if (sizeflag & DFLAG)
	    {
	      if (intel_syntax)
		  *obufp++ = 'd';
	      else
		  *obufp++ = 'l';
	    }
	  else
	    *obufp++ = 'w';
	  if (intel_syntax && !p[1]
	      && ((rex & REX_W) || (sizeflag & DFLAG)))
	    *obufp++ = 'e';
	  if (!(rex & REX_W))
	    used_prefixes |= (prefixes & PREFIX_DATA);
	  break;
	case 'V':
	  if (l == 0 && len == 1)
	    {
	      if (intel_syntax)
		break;
	      if (address_mode == mode_64bit && (sizeflag & DFLAG))
		{
		  if (sizeflag & SUFFIX_ALWAYS)
		    *obufp++ = 'q';
		  break;
		}
	    }
	  else
	    {
	      if (l != 1
		  || len != 2
		  || last[0] != 'L')
		{
		  SAVE_LAST (*p);
		  break;
		}

	      if (rex & REX_W)
		{
		  *obufp++ = 'a';
		  *obufp++ = 'b';
		  *obufp++ = 's';
		}
	    }
	  /* Fall through.  */
	  goto case_S;
	case 'S':
	  if (l == 0 && len == 1)
	    {
case_S:
	      if (intel_syntax)
		break;
	      if (sizeflag & SUFFIX_ALWAYS)
		{
		  if (rex & REX_W)
		    *obufp++ = 'q';
		  else
		    {
		      if (sizeflag & DFLAG)
			*obufp++ = 'l';
		      else
			*obufp++ = 'w';
		      used_prefixes |= (prefixes & PREFIX_DATA);
		    }
		}
	    }
	  else
	    {
	      if (l != 1
		  || len != 2
		  || last[0] != 'L')
		{
		  SAVE_LAST (*p);
		  break;
		}

	      if (address_mode == mode_64bit
		  && !(prefixes & PREFIX_ADDR))
		{
		  *obufp++ = 'a';
		  *obufp++ = 'b';
		  *obufp++ = 's';
		}

	      goto case_S;
	    }
	  break;
	case 'X':
	  if (l != 0 || len != 1)
	    {
	      SAVE_LAST (*p);
	      break;
	    }
	  if (need_vex && vex.prefix)
	    {
	      if (vex.prefix == DATA_PREFIX_OPCODE)
		*obufp++ = 'd';
	      else
		*obufp++ = 's';
	    }
	  else
	    {
	      if (prefixes & PREFIX_DATA)
		*obufp++ = 'd';
	      else
		*obufp++ = 's';
	      used_prefixes |= (prefixes & PREFIX_DATA);
	    }
	  break;
	case 'Y':
	  if (l == 0 && len == 1)
	    {
	      if (intel_syntax || !(sizeflag & SUFFIX_ALWAYS))
		break;
	      if (rex & REX_W)
		{
		  USED_REX (REX_W);
		  *obufp++ = 'q';
		}
	      break;
	    }
	  else
	    {
	      if (l != 1 || len != 2 || last[0] != 'X')
		{
		  SAVE_LAST (*p);
		  break;
		}
	      if (!need_vex)
		{
		  cerr << "!need_vex" << endl;
		  abort ();
		}
	      if (intel_syntax
		  || (modrm.mod == 3 && !(sizeflag & SUFFIX_ALWAYS)))
		break;
	      switch (vex.length)
		{
		case 128:
		  *obufp++ = 'x';
		  break;
		case 256:
		  *obufp++ = 'y';
		  break;
		default:
		  {
		    cerr << "bad length "<< vex.length << endl;
		    abort ();
		  }
		}
	    }
	  break;
	case 'W':
	  if (l == 0 && len == 1)
	    {
	      /* operand size flag for cwtl, cbtw */
	      USED_REX (REX_W);
	      if (rex & REX_W)
		{
		  if (intel_syntax)
		    *obufp++ = 'd';
		  else
		    *obufp++ = 'l';
		}
	      else if (sizeflag & DFLAG)
		*obufp++ = 'w';
	      else
		*obufp++ = 'b';
	      if (!(rex & REX_W))
		used_prefixes |= (prefixes & PREFIX_DATA);
	    }
	  else
	    {
	      if (l != 1 || len != 2 || last[0] != 'X')
		{
		  SAVE_LAST (*p);
		  break;
		}
	      if (!need_vex)
		{
		  cerr << "!need_vex" << endl;
		  abort ();
		}
	      *obufp++ = vex.w ? 'd': 's';
	    }
	  break;
	}
      alt = 0;
    }
  *obufp = 0;


  cerr << "found :";

  for (t_last_r::iterator i= last.begin();i <last.end(); i++)
    {
      std::cerr << i;
    }
  cerr << endl;

  //  cerr << "checking :" << in_template << endl;

  mnemonicendp = obufp;
  return 0;
}

};
