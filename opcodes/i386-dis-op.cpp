/* Print i386 instructions for GDB, the GNU debugger.
   Copyright 1988, 1989, 1991, 1993, 1994, 1995, 1996, 1997, 1998, 1999,
   2001, 2002, 2003, 2004, 2005, 2006, 2007, 2008, 2009, 2010
   Free Software Foundation, Inc.

   This file is part of the GNU opcodes library.

   This library is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3, or (at your option)
   any later version.

   It is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 51 Franklin Street - Fifth Floor, Boston,
   MA 02110-1301, USA.  */


/* 80386 instruction printer by Pace Willisson (pace@prep.ai.mit.edu)
   July 1988
    modified by John Hassey (hassey@dg-rtp.dg.com)
    x86-64 support added by Jan Hubicka (jh@suse.cz)
    VIA PadLock support by Michal Ludvig (mludvig@suse.cz).  */

/* The main tables describing the instructions is essentially a copy
   of the "Opcode Map" chapter (Appendix A) of the Intel 80386
   Programmers Manual.  Usually, there is a capital letter, followed
   by a small letter.  The capital letter tell the addressing mode,
   and the small letter tells about the operand size.  Refer to
   the Intel manual for details.  */

#include "i386-dis-std-inc.h"

using namespace std;



namespace binutils_objdump_opcodes_i386
{


  T_op::T_op(void (* fun)(int, int), int bm)
  {
    rtn=fun;
    bytemode=bm;
    // this is called before system is setup cerr << "TODO T_op(fun pointer,int)" << endl;
  }

  T_op::T_op()
  {
    //    cerr << "TODO T_op()" << endl;
  }

  dis386::dis386()
  {
    //    cerr << "TODO dis386::dis386" << endl;
  }

  
  void copy(T_op (*a)[5], const T_op (*b)[5] )
  {
    for (int i=0; i < sizeof(*a); i++)
      {
	*a[i]=*b[i];
      }
  }
  void copy(T_op (*a)[5], std::vector<T_op> b)
  {
    for (int i=0; i < sizeof(*a); i++)
      {
      *a[i]=b[i];
      }

  }

  void copy(T_op (a)[5], std::vector<T_op> b)
  {
    for (int i=0; i < sizeof(*a); i++)
      {
	a[i]=b[i];
      }

  }

  template <class T> void copy(T **a, T ** b)
  {
    for (int i=0; i < 5; i++)
      {
	a[i]=b[i];
      }

  }

  template <class T> void copy(T **a , std::initializer_list<T> b)
  {
    for (int i=0; i < 5; i++)
      {
	a[i]=b[i];
      }

  }
  


  dis386::dis386(const dis386& cp){
    //    cerr << "TODO dis386::dis386(copy)" << endl;
    name=cp.name;
    copy(&op,&cp.op);

}
  dis386::dis386(const char* n, std::initializer_list<T_op> list){
    name=n;
    copy(&op,list);
    //    cerr << "TODO dis386::dis386(name, list)" << endl;
  }

  dis386::dis386(const char* n, T_op o0, T_op o1){
    //    cerr << "TODO dis386::dis386(name, op,op)" << endl;
    name=n;
    op[0]=o0;
    op[1]=o1;
  }



}; // end of namespace

