const struct dis386 rm_table[][8] = {
  {
    /* RM_0F01_REG_0 */
    { Bad_Opcode },
    { "vmcall",		{ Skip_MODRM } },
    { "vmlaunch",	{ Skip_MODRM } },
    { "vmresume",	{ Skip_MODRM } },
    { "vmxoff",		{ Skip_MODRM } },
  },
  {
    /* RM_0F01_REG_1 */
    { "monitor",	{ { OP_Monitor, 0 } } },
    { "mwait",		{ { OP_Mwait, 0 } } },
  },
  {
    /* RM_0F01_REG_2 */
    { "xgetbv",		{ Skip_MODRM } },
    { "xsetbv",		{ Skip_MODRM } },
  },
  {
    /* RM_0F01_REG_3 */
    { "vmrun",		{ Skip_MODRM } },
    { "vmmcall",	{ Skip_MODRM } },
    { "vmload",		{ Skip_MODRM } },
    { "vmsave",		{ Skip_MODRM } },
    { "stgi",		{ Skip_MODRM } },
    { "clgi",		{ Skip_MODRM } },
    { "skinit",		{ Skip_MODRM } },
    { "invlpga",	{ Skip_MODRM } },
  },
  {
    /* RM_0F01_REG_7 */
    { "swapgs",		{ Skip_MODRM } },
    { "rdtscp",		{ Skip_MODRM } },
  },
  {
    /* RM_0FAE_REG_5 */
    { "lfence",		{ Skip_MODRM } },
  },
  {
    /* RM_0FAE_REG_6 */
    { "mfence",		{ Skip_MODRM } },
  },
  {
    /* RM_0FAE_REG_7 */
    { "sfence",		{ Skip_MODRM } },
  },
};
