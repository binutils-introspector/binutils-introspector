const struct dis386 mod_table[][2] = {
  {
    /* MOD_8D */
    { "leaS",		{ Gv, M } },
  },
  {
    /* MOD_0F01_REG_0 */
    { X86_64_TABLE (X86_64_0F01_REG_0) },
    { RM_TABLE (RM_0F01_REG_0) },
  },
  {
    /* MOD_0F01_REG_1 */
    { X86_64_TABLE (X86_64_0F01_REG_1) },
    { RM_TABLE (RM_0F01_REG_1) },
  },
  {
    /* MOD_0F01_REG_2 */
    { X86_64_TABLE (X86_64_0F01_REG_2) },
    { RM_TABLE (RM_0F01_REG_2) },
  },
  {
    /* MOD_0F01_REG_3 */
    { X86_64_TABLE (X86_64_0F01_REG_3) },
    { RM_TABLE (RM_0F01_REG_3) },
  },
  {
    /* MOD_0F01_REG_7 */
    { "invlpg",		{ Mb } },
    { RM_TABLE (RM_0F01_REG_7) },
  },
  {
    /* MOD_0F12_PREFIX_0 */
    { "movlps",		{ XM, EXq } },
    { "movhlps",	{ XM, EXq } },
  },
  {
    /* MOD_0F13 */
    { "movlpX",		{ EXq, XM } },
  },
  {
    /* MOD_0F16_PREFIX_0 */
    { "movhps",		{ XM, EXq } },
    { "movlhps",	{ XM, EXq } },
  },
  {
    /* MOD_0F17 */
    { "movhpX",		{ EXq, XM } },
  },
  {
    /* MOD_0F18_REG_0 */
    { "prefetchnta",	{ Mb } },
  },
  {
    /* MOD_0F18_REG_1 */
    { "prefetcht0",	{ Mb } },
  },
  {
    /* MOD_0F18_REG_2 */
    { "prefetcht1",	{ Mb } },
  },
  {
    /* MOD_0F18_REG_3 */
    { "prefetcht2",	{ Mb } },
  },
  {
    /* MOD_0F20 */
    { Bad_Opcode },
    { "movZ",		{ Rm, Cm } },
  },
  {
    /* MOD_0F21 */
    { Bad_Opcode },
    { "movZ",		{ Rm, Dm } },
  },
  {
    /* MOD_0F22 */
    { Bad_Opcode },
    { "movZ",		{ Cm, Rm } },
  },
  {
    /* MOD_0F23 */
    { Bad_Opcode },
    { "movZ",		{ Dm, Rm } },
  },
  {
    /* MOD_0F24 */
    { Bad_Opcode },    
    { "movL",		{ Rd, Td } },
  },
  {
    /* MOD_0F26 */
    { Bad_Opcode },
    { "movL",		{ Td, Rd } },
  },
  {
    /* MOD_0F2B_PREFIX_0 */
    {"movntps",		{ Mx, XM } },
  },
  {
    /* MOD_0F2B_PREFIX_1 */
    {"movntss",		{ Md, XM } },
  },
  {
    /* MOD_0F2B_PREFIX_2 */
    {"movntpd",		{ Mx, XM } },
  },
  {
    /* MOD_0F2B_PREFIX_3 */
    {"movntsd",		{ Mq, XM } },
  },
  {
    /* MOD_0F51 */
    { Bad_Opcode },
    { "movmskpX",	{ Gdq, XS } },
  },
  {
    /* MOD_0F71_REG_2 */
    { Bad_Opcode },
    { "psrlw",		{ MS, Ib } },
  },
  {
    /* MOD_0F71_REG_4 */
    { Bad_Opcode },
    { "psraw",		{ MS, Ib } },
  },
  {
    /* MOD_0F71_REG_6 */
    { Bad_Opcode },
    { "psllw",		{ MS, Ib } },
  },
  {
    /* MOD_0F72_REG_2 */
    { Bad_Opcode },
    { "psrld",		{ MS, Ib } },
  },
  {
    /* MOD_0F72_REG_4 */
    { Bad_Opcode },
    { "psrad",		{ MS, Ib } },
  },
  {
    /* MOD_0F72_REG_6 */
    { Bad_Opcode },
    { "pslld",		{ MS, Ib } },
  },
  {
    /* MOD_0F73_REG_2 */
    { Bad_Opcode },
    { "psrlq",		{ MS, Ib } },
  },
  {
    /* MOD_0F73_REG_3 */
    { Bad_Opcode },
    { PREFIX_TABLE (PREFIX_0F73_REG_3) },
  },
  {
    /* MOD_0F73_REG_6 */
    { Bad_Opcode },
    { "psllq",		{ MS, Ib } },
  },
  {
    /* MOD_0F73_REG_7 */
    { Bad_Opcode },
    { PREFIX_TABLE (PREFIX_0F73_REG_7) },
  },
  {
    /* MOD_0FAE_REG_0 */
    { "fxsave",		{ FXSAVE } },
    { PREFIX_TABLE (PREFIX_0FAE_REG_0) },
  },
  {
    /* MOD_0FAE_REG_1 */
    { "fxrstor",	{ FXSAVE } },
    { PREFIX_TABLE (PREFIX_0FAE_REG_1) },
  },
  {
    /* MOD_0FAE_REG_2 */
    { "ldmxcsr",	{ Md } },
    { PREFIX_TABLE (PREFIX_0FAE_REG_2) },
  },
  {
    /* MOD_0FAE_REG_3 */
    { "stmxcsr",	{ Md } },
    { PREFIX_TABLE (PREFIX_0FAE_REG_3) },
  },
  {
    /* MOD_0FAE_REG_4 */
    { "xsave",		{ FXSAVE } },
  },
  {
    /* MOD_0FAE_REG_5 */
    { "xrstor",		{ FXSAVE } },
    { RM_TABLE (RM_0FAE_REG_5) },
  },
  {
    /* MOD_0FAE_REG_6 */
    { "xsaveopt",	{ FXSAVE } },
    { RM_TABLE (RM_0FAE_REG_6) },
  },
  {
    /* MOD_0FAE_REG_7 */
    { "clflush",	{ Mb } },
    { RM_TABLE (RM_0FAE_REG_7) },
  },
  {
    /* MOD_0FB2 */
    { "lssS",		{ Gv, Mp } },
  },
  {
    /* MOD_0FB4 */
    { "lfsS",		{ Gv, Mp } },
  },
  {
    /* MOD_0FB5 */
    { "lgsS",		{ Gv, Mp } },
  },
  {
    /* MOD_0FC7_REG_6 */
    { PREFIX_TABLE (PREFIX_0FC7_REG_6) },
    { "rdrand",		{ Ev } },
  },
  {
    /* MOD_0FC7_REG_7 */
    { "vmptrst",	{ Mq } },
  },
  {
    /* MOD_0FD7 */
    { Bad_Opcode },
    { "pmovmskb",	{ Gdq, MS } },
  },
  {
    /* MOD_0FE7_PREFIX_2 */
    { "movntdq",	{ Mx, XM } },
  },
  {
    /* MOD_0FF0_PREFIX_3 */
    { "lddqu",		{ XM, M } },
  },
  {
    /* MOD_0F382A_PREFIX_2 */
    { "movntdqa",	{ XM, Mx } },
  },
  {
    /* MOD_62_32BIT */
    { "bound{S|}",	{ Gv, Ma } },
  },
  {
    /* MOD_C4_32BIT */
    { "lesS",		{ Gv, Mp } },
    { VEX_C4_TABLE (VEX_0F) },
  },
  {
    /* MOD_C5_32BIT */
    { "ldsS",		{ Gv, Mp } },
    { VEX_C5_TABLE (VEX_0F) },
  },
  {
    /* MOD_VEX_0F12_PREFIX_0 */
    { VEX_LEN_TABLE (VEX_LEN_0F12_P_0_M_0) },
    { VEX_LEN_TABLE (VEX_LEN_0F12_P_0_M_1) },
  },
  {
    /* MOD_VEX_0F13 */
    { VEX_LEN_TABLE (VEX_LEN_0F13_M_0) },
  },
  {
    /* MOD_VEX_0F16_PREFIX_0 */
    { VEX_LEN_TABLE (VEX_LEN_0F16_P_0_M_0) },
    { VEX_LEN_TABLE (VEX_LEN_0F16_P_0_M_1) },
  },
  {
    /* MOD_VEX_0F17 */
    { VEX_LEN_TABLE (VEX_LEN_0F17_M_0) },
  },
  {
    /* MOD_VEX_0F2B */
    { VEX_W_TABLE (VEX_W_0F2B_M_0) },
  },
  {
    /* MOD_VEX_0F50 */
    { Bad_Opcode },
    { VEX_W_TABLE (VEX_W_0F50_M_0) },
  },
  {
    /* MOD_VEX_0F71_REG_2 */
    { Bad_Opcode },
    { PREFIX_TABLE (PREFIX_VEX_0F71_REG_2) },
  },
  {
    /* MOD_VEX_0F71_REG_4 */
    { Bad_Opcode },
    { PREFIX_TABLE (PREFIX_VEX_0F71_REG_4) },
  },
  {
    /* MOD_VEX_0F71_REG_6 */
    { Bad_Opcode },
    { PREFIX_TABLE (PREFIX_VEX_0F71_REG_6) },
  },
  {
    /* MOD_VEX_0F72_REG_2 */
    { Bad_Opcode },
    { PREFIX_TABLE (PREFIX_VEX_0F72_REG_2) },
  },
  {
    /* MOD_VEX_0F72_REG_4 */
    { Bad_Opcode },
    { PREFIX_TABLE (PREFIX_VEX_0F72_REG_4) },
  },
  {
    /* MOD_VEX_0F72_REG_6 */
    { Bad_Opcode },
    { PREFIX_TABLE (PREFIX_VEX_0F72_REG_6) },
  },
  {
    /* MOD_VEX_0F73_REG_2 */
    { Bad_Opcode },
    { PREFIX_TABLE (PREFIX_VEX_0F73_REG_2) },
  },
  {
    /* MOD_VEX_0F73_REG_3 */
    { Bad_Opcode },
    { PREFIX_TABLE (PREFIX_VEX_0F73_REG_3) },
  },
  {
    /* MOD_VEX_0F73_REG_6 */
    { Bad_Opcode },
    { PREFIX_TABLE (PREFIX_VEX_0F73_REG_6) },
  },
  {
    /* MOD_VEX_0F73_REG_7 */
    { Bad_Opcode },
    { PREFIX_TABLE (PREFIX_VEX_0F73_REG_7) },
  },
  {
    /* MOD_VEX_0FAE_REG_2 */
    { VEX_LEN_TABLE (VEX_LEN_0FAE_R_2_M_0) },
  },
  {
    /* MOD_VEX_0FAE_REG_3 */
    { VEX_LEN_TABLE (VEX_LEN_0FAE_R_3_M_0) },
  },
  {
    /* MOD_VEX_0FD7_PREFIX_2 */
    { Bad_Opcode },
    { VEX_LEN_TABLE (VEX_LEN_0FD7_P_2_M_1) },
  },
  {
    /* MOD_VEX_0FE7_PREFIX_2 */
    { VEX_W_TABLE (VEX_W_0FE7_P_2_M_0) },
  },
  {
    /* MOD_VEX_0FF0_PREFIX_3 */
    { VEX_W_TABLE (VEX_W_0FF0_P_3_M_0) },
  },
  {
    /* MOD_VEX_0F3818_PREFIX_2 */
    { VEX_W_TABLE (VEX_W_0F3818_P_2_M_0) },
  },
  {
    /* MOD_VEX_0F3819_PREFIX_2 */
    { VEX_LEN_TABLE (VEX_LEN_0F3819_P_2_M_0) },
  },
  {
    /* MOD_VEX_0F381A_PREFIX_2 */
    { VEX_LEN_TABLE (VEX_LEN_0F381A_P_2_M_0) },
  },
  {
    /* MOD_VEX_0F382A_PREFIX_2 */
    { VEX_LEN_TABLE (VEX_LEN_0F382A_P_2_M_0) },
  },
  {
    /* MOD_VEX_0F382C_PREFIX_2 */
    { VEX_W_TABLE (VEX_W_0F382C_P_2_M_0) },
  },
  {
    /* MOD_VEX_0F382D_PREFIX_2 */
    { VEX_W_TABLE (VEX_W_0F382D_P_2_M_0) },
  },
  {
    /* MOD_VEX_0F382E_PREFIX_2 */
    { VEX_W_TABLE (VEX_W_0F382E_P_2_M_0) },
  },
  {
    /* MOD_VEX_0F382F_PREFIX_2 */
    { VEX_W_TABLE (VEX_W_0F382F_P_2_M_0) },
  },
};
