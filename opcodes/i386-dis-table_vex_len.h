const struct dis386 vex_len_table[][2] = {
  /* VEX_LEN_0F10_P_1 */
  {
    { VEX_W_TABLE (VEX_W_0F10_P_1) },
    { VEX_W_TABLE (VEX_W_0F10_P_1) },
  },

  /* VEX_LEN_0F10_P_3 */
  {
    { VEX_W_TABLE (VEX_W_0F10_P_3) },
    { VEX_W_TABLE (VEX_W_0F10_P_3) },
  },

  /* VEX_LEN_0F11_P_1 */
  {
    { VEX_W_TABLE (VEX_W_0F11_P_1) },
    { VEX_W_TABLE (VEX_W_0F11_P_1) },
  },

  /* VEX_LEN_0F11_P_3 */
  {
    { VEX_W_TABLE (VEX_W_0F11_P_3) },
    { VEX_W_TABLE (VEX_W_0F11_P_3) },
  },

  /* VEX_LEN_0F12_P_0_M_0 */
  {
    { VEX_W_TABLE (VEX_W_0F12_P_0_M_0) },
  },

  /* VEX_LEN_0F12_P_0_M_1 */
  {
    { VEX_W_TABLE (VEX_W_0F12_P_0_M_1) },
  },

  /* VEX_LEN_0F12_P_2 */
  {
    { VEX_W_TABLE (VEX_W_0F12_P_2) },
  },

  /* VEX_LEN_0F13_M_0 */
  {
    { VEX_W_TABLE (VEX_W_0F13_M_0) },
  },

  /* VEX_LEN_0F16_P_0_M_0 */
  {
    { VEX_W_TABLE (VEX_W_0F16_P_0_M_0) },
  },

  /* VEX_LEN_0F16_P_0_M_1 */
  {
    { VEX_W_TABLE (VEX_W_0F16_P_0_M_1) },
  },

  /* VEX_LEN_0F16_P_2 */
  {
    { VEX_W_TABLE (VEX_W_0F16_P_2) },
  },

  /* VEX_LEN_0F17_M_0 */
  {
    { VEX_W_TABLE (VEX_W_0F17_M_0) },
  },

  /* VEX_LEN_0F2A_P_1 */
  {
    { "vcvtsi2ss%LQ",	{ XMScalar, VexScalar, Ev } },
    { "vcvtsi2ss%LQ",	{ XMScalar, VexScalar, Ev } },
  },

  /* VEX_LEN_0F2A_P_3 */
  {
    { "vcvtsi2sd%LQ",	{ XMScalar, VexScalar, Ev } },
    { "vcvtsi2sd%LQ",	{ XMScalar, VexScalar, Ev } },
  },

  /* VEX_LEN_0F2C_P_1 */
  {
    { "vcvttss2siY",	{ Gv, EXdScalar } },
    { "vcvttss2siY",	{ Gv, EXdScalar } },
  },

  /* VEX_LEN_0F2C_P_3 */
  {
    { "vcvttsd2siY",	{ Gv, EXqScalar } },
    { "vcvttsd2siY",	{ Gv, EXqScalar } },
  },

  /* VEX_LEN_0F2D_P_1 */
  {
    { "vcvtss2siY",	{ Gv, EXdScalar } },
    { "vcvtss2siY",	{ Gv, EXdScalar } },
  },

  /* VEX_LEN_0F2D_P_3 */
  {
    { "vcvtsd2siY",	{ Gv, EXqScalar } },
    { "vcvtsd2siY",	{ Gv, EXqScalar } },
  },

  /* VEX_LEN_0F2E_P_0 */
  {
    { VEX_W_TABLE (VEX_W_0F2E_P_0) },
    { VEX_W_TABLE (VEX_W_0F2E_P_0) },
  },

  /* VEX_LEN_0F2E_P_2 */
  {
    { VEX_W_TABLE (VEX_W_0F2E_P_2) },
    { VEX_W_TABLE (VEX_W_0F2E_P_2) },
  },

  /* VEX_LEN_0F2F_P_0 */
  {
    { VEX_W_TABLE (VEX_W_0F2F_P_0) },
    { VEX_W_TABLE (VEX_W_0F2F_P_0) },
  },

  /* VEX_LEN_0F2F_P_2 */
  {
    { VEX_W_TABLE (VEX_W_0F2F_P_2) },
    { VEX_W_TABLE (VEX_W_0F2F_P_2) },
  },

  /* VEX_LEN_0F51_P_1 */
  {
    { VEX_W_TABLE (VEX_W_0F51_P_1) },
    { VEX_W_TABLE (VEX_W_0F51_P_1) },
  },

  /* VEX_LEN_0F51_P_3 */
  {
    { VEX_W_TABLE (VEX_W_0F51_P_3) },
    { VEX_W_TABLE (VEX_W_0F51_P_3) },
  },

  /* VEX_LEN_0F52_P_1 */
  {
    { VEX_W_TABLE (VEX_W_0F52_P_1) },
    { VEX_W_TABLE (VEX_W_0F52_P_1) },
  },

  /* VEX_LEN_0F53_P_1 */
  {
    { VEX_W_TABLE (VEX_W_0F53_P_1) },
    { VEX_W_TABLE (VEX_W_0F53_P_1) },
  },

  /* VEX_LEN_0F58_P_1 */
  {
    { VEX_W_TABLE (VEX_W_0F58_P_1) },
    { VEX_W_TABLE (VEX_W_0F58_P_1) },
  },

  /* VEX_LEN_0F58_P_3 */
  {
    { VEX_W_TABLE (VEX_W_0F58_P_3) },
    { VEX_W_TABLE (VEX_W_0F58_P_3) },
  },

  /* VEX_LEN_0F59_P_1 */
  {
    { VEX_W_TABLE (VEX_W_0F59_P_1) },
    { VEX_W_TABLE (VEX_W_0F59_P_1) },
  },

  /* VEX_LEN_0F59_P_3 */
  {
    { VEX_W_TABLE (VEX_W_0F59_P_3) },
    { VEX_W_TABLE (VEX_W_0F59_P_3) },
  },

  /* VEX_LEN_0F5A_P_1 */
  {
    { VEX_W_TABLE (VEX_W_0F5A_P_1) },
    { VEX_W_TABLE (VEX_W_0F5A_P_1) },
  },

  /* VEX_LEN_0F5A_P_3 */
  {
    { VEX_W_TABLE (VEX_W_0F5A_P_3) },
    { VEX_W_TABLE (VEX_W_0F5A_P_3) },
  },

  /* VEX_LEN_0F5C_P_1 */
  {
    { VEX_W_TABLE (VEX_W_0F5C_P_1) },
    { VEX_W_TABLE (VEX_W_0F5C_P_1) },
  },

  /* VEX_LEN_0F5C_P_3 */
  {
    { VEX_W_TABLE (VEX_W_0F5C_P_3) },
    { VEX_W_TABLE (VEX_W_0F5C_P_3) },
  },

  /* VEX_LEN_0F5D_P_1 */
  {
    { VEX_W_TABLE (VEX_W_0F5D_P_1) },
    { VEX_W_TABLE (VEX_W_0F5D_P_1) },
  },

  /* VEX_LEN_0F5D_P_3 */
  {
    { VEX_W_TABLE (VEX_W_0F5D_P_3) },
    { VEX_W_TABLE (VEX_W_0F5D_P_3) },
  },

  /* VEX_LEN_0F5E_P_1 */
  {
    { VEX_W_TABLE (VEX_W_0F5E_P_1) },
    { VEX_W_TABLE (VEX_W_0F5E_P_1) },
  },

  /* VEX_LEN_0F5E_P_3 */
  {
    { VEX_W_TABLE (VEX_W_0F5E_P_3) },
    { VEX_W_TABLE (VEX_W_0F5E_P_3) },
  },

  /* VEX_LEN_0F5F_P_1 */
  {
    { VEX_W_TABLE (VEX_W_0F5F_P_1) },
    { VEX_W_TABLE (VEX_W_0F5F_P_1) },
  },

  /* VEX_LEN_0F5F_P_3 */
  {
    { VEX_W_TABLE (VEX_W_0F5F_P_3) },
    { VEX_W_TABLE (VEX_W_0F5F_P_3) },
  },

  /* VEX_LEN_0F60_P_2 */
  {
    { VEX_W_TABLE (VEX_W_0F60_P_2) },
  },

  /* VEX_LEN_0F61_P_2 */
  {
    { VEX_W_TABLE (VEX_W_0F61_P_2) },
  },

  /* VEX_LEN_0F62_P_2 */
  {
    { VEX_W_TABLE (VEX_W_0F62_P_2) },
  },

  /* VEX_LEN_0F63_P_2 */
  {
    { VEX_W_TABLE (VEX_W_0F63_P_2) },
  },

  /* VEX_LEN_0F64_P_2 */
  {
    { VEX_W_TABLE (VEX_W_0F64_P_2) },
  },

  /* VEX_LEN_0F65_P_2 */
  {
    { VEX_W_TABLE (VEX_W_0F65_P_2) },
  },

  /* VEX_LEN_0F66_P_2 */
  {
    { VEX_W_TABLE (VEX_W_0F66_P_2) },
  },

  /* VEX_LEN_0F67_P_2 */
  {
    { VEX_W_TABLE (VEX_W_0F67_P_2) },
  },

  /* VEX_LEN_0F68_P_2 */
  {
    { VEX_W_TABLE (VEX_W_0F68_P_2) },
  },

  /* VEX_LEN_0F69_P_2 */
  {
    { VEX_W_TABLE (VEX_W_0F69_P_2) },
  },

  /* VEX_LEN_0F6A_P_2 */
  {
    { VEX_W_TABLE (VEX_W_0F6A_P_2) },
  },

  /* VEX_LEN_0F6B_P_2 */
  {
    { VEX_W_TABLE (VEX_W_0F6B_P_2) },
  },

  /* VEX_LEN_0F6C_P_2 */
  {
    { VEX_W_TABLE (VEX_W_0F6C_P_2) },
  },

  /* VEX_LEN_0F6D_P_2 */
  {
    { VEX_W_TABLE (VEX_W_0F6D_P_2) },
  },

  /* VEX_LEN_0F6E_P_2 */
  {
    { "vmovK",		{ XMScalar, Edq } },
    { "vmovK",		{ XMScalar, Edq } },
  },

  /* VEX_LEN_0F70_P_1 */
  {
    { VEX_W_TABLE (VEX_W_0F70_P_1) },
  },

  /* VEX_LEN_0F70_P_2 */
  {
    { VEX_W_TABLE (VEX_W_0F70_P_2) },
  },

  /* VEX_LEN_0F70_P_3 */
  {
    { VEX_W_TABLE (VEX_W_0F70_P_3) },
  },

  /* VEX_LEN_0F71_R_2_P_2 */
  {
    { VEX_W_TABLE (VEX_W_0F71_R_2_P_2) },
  },

  /* VEX_LEN_0F71_R_4_P_2 */
  {
    { VEX_W_TABLE (VEX_W_0F71_R_4_P_2) },
  },

  /* VEX_LEN_0F71_R_6_P_2 */
  {
    { VEX_W_TABLE (VEX_W_0F71_R_6_P_2) },
  },

  /* VEX_LEN_0F72_R_2_P_2 */
  {
    { VEX_W_TABLE (VEX_W_0F72_R_2_P_2) },
  },

  /* VEX_LEN_0F72_R_4_P_2 */
  {
    { VEX_W_TABLE (VEX_W_0F72_R_4_P_2) },
  },

  /* VEX_LEN_0F72_R_6_P_2 */
  {
    { VEX_W_TABLE (VEX_W_0F72_R_6_P_2) },
  },

  /* VEX_LEN_0F73_R_2_P_2 */
  {
    { VEX_W_TABLE (VEX_W_0F73_R_2_P_2) },
  },

  /* VEX_LEN_0F73_R_3_P_2 */
  {
    { VEX_W_TABLE (VEX_W_0F73_R_3_P_2) },
  },

  /* VEX_LEN_0F73_R_6_P_2 */
  {
    { VEX_W_TABLE (VEX_W_0F73_R_6_P_2) },
  },

  /* VEX_LEN_0F73_R_7_P_2 */
  {
    { VEX_W_TABLE (VEX_W_0F73_R_7_P_2) },
  },

  /* VEX_LEN_0F74_P_2 */
  {
    { VEX_W_TABLE (VEX_W_0F74_P_2) },
  },

  /* VEX_LEN_0F75_P_2 */
  {
    { VEX_W_TABLE (VEX_W_0F75_P_2) },
  },

  /* VEX_LEN_0F76_P_2 */
  {
    { VEX_W_TABLE (VEX_W_0F76_P_2) },
  },

  /* VEX_LEN_0F7E_P_1 */
  {
    { VEX_W_TABLE (VEX_W_0F7E_P_1) },
    { VEX_W_TABLE (VEX_W_0F7E_P_1) },
  },

  /* VEX_LEN_0F7E_P_2 */
  {
    { "vmovK",		{ Edq, XMScalar } },
    { "vmovK",		{ Edq, XMScalar } },
  },

  /* VEX_LEN_0FAE_R_2_M_0 */
  {
    { VEX_W_TABLE (VEX_W_0FAE_R_2_M_0) },
  },

  /* VEX_LEN_0FAE_R_3_M_0 */
  {
    { VEX_W_TABLE (VEX_W_0FAE_R_3_M_0) },
  },

  /* VEX_LEN_0FC2_P_1 */
  {
    { VEX_W_TABLE (VEX_W_0FC2_P_1) },
    { VEX_W_TABLE (VEX_W_0FC2_P_1) },
  },

  /* VEX_LEN_0FC2_P_3 */
  {
    { VEX_W_TABLE (VEX_W_0FC2_P_3) },
    { VEX_W_TABLE (VEX_W_0FC2_P_3) },
  },

  /* VEX_LEN_0FC4_P_2 */
  {
    { VEX_W_TABLE (VEX_W_0FC4_P_2) },
  },

  /* VEX_LEN_0FC5_P_2 */
  {
    { VEX_W_TABLE (VEX_W_0FC5_P_2) },
  },

  /* VEX_LEN_0FD1_P_2 */
  {
    { VEX_W_TABLE (VEX_W_0FD1_P_2) },
  },

  /* VEX_LEN_0FD2_P_2 */
  {
    { VEX_W_TABLE (VEX_W_0FD2_P_2) },
  },

  /* VEX_LEN_0FD3_P_2 */
  {
    { VEX_W_TABLE (VEX_W_0FD3_P_2) },
  },

  /* VEX_LEN_0FD4_P_2 */
  {
    { VEX_W_TABLE (VEX_W_0FD4_P_2) },
  },

  /* VEX_LEN_0FD5_P_2 */
  {
    { VEX_W_TABLE (VEX_W_0FD5_P_2) },
  },

  /* VEX_LEN_0FD6_P_2 */
  {
    { VEX_W_TABLE (VEX_W_0FD6_P_2) },
    { VEX_W_TABLE (VEX_W_0FD6_P_2) },
  },

  /* VEX_LEN_0FD7_P_2_M_1 */
  {
    { VEX_W_TABLE (VEX_W_0FD7_P_2_M_1) },
  },

  /* VEX_LEN_0FD8_P_2 */
  {
    { VEX_W_TABLE (VEX_W_0FD8_P_2) },
  },

  /* VEX_LEN_0FD9_P_2 */
  {
    { VEX_W_TABLE (VEX_W_0FD9_P_2) },
  },

  /* VEX_LEN_0FDA_P_2 */
  {
    { VEX_W_TABLE (VEX_W_0FDA_P_2) },
  },

  /* VEX_LEN_0FDB_P_2 */
  {
    { VEX_W_TABLE (VEX_W_0FDB_P_2) },
  },

  /* VEX_LEN_0FDC_P_2 */
  {
    { VEX_W_TABLE (VEX_W_0FDC_P_2) },
  },

  /* VEX_LEN_0FDD_P_2 */
  {
    { VEX_W_TABLE (VEX_W_0FDD_P_2) },
  },

  /* VEX_LEN_0FDE_P_2 */
  {
    { VEX_W_TABLE (VEX_W_0FDE_P_2) },
  },

  /* VEX_LEN_0FDF_P_2 */
  {
    { VEX_W_TABLE (VEX_W_0FDF_P_2) },
  },

  /* VEX_LEN_0FE0_P_2 */
  {
    { VEX_W_TABLE (VEX_W_0FE0_P_2) },
  },

  /* VEX_LEN_0FE1_P_2 */
  {
    { VEX_W_TABLE (VEX_W_0FE1_P_2) },
  },

  /* VEX_LEN_0FE2_P_2 */
  {
    { VEX_W_TABLE (VEX_W_0FE2_P_2) },
  },

  /* VEX_LEN_0FE3_P_2 */
  {
    { VEX_W_TABLE (VEX_W_0FE3_P_2) },
  },

  /* VEX_LEN_0FE4_P_2 */
  {
    { VEX_W_TABLE (VEX_W_0FE4_P_2) },
  },

  /* VEX_LEN_0FE5_P_2 */
  {
    { VEX_W_TABLE (VEX_W_0FE5_P_2) },
  },

  /* VEX_LEN_0FE8_P_2 */
  {
    { VEX_W_TABLE (VEX_W_0FE8_P_2) },
  },

  /* VEX_LEN_0FE9_P_2 */
  {
    { VEX_W_TABLE (VEX_W_0FE9_P_2) },
  },

  /* VEX_LEN_0FEA_P_2 */
  {
    { VEX_W_TABLE (VEX_W_0FEA_P_2) },
  },

  /* VEX_LEN_0FEB_P_2 */
  {
    { VEX_W_TABLE (VEX_W_0FEB_P_2) },
  },

  /* VEX_LEN_0FEC_P_2 */
  {
    { VEX_W_TABLE (VEX_W_0FEC_P_2) },
  },

  /* VEX_LEN_0FED_P_2 */
  {
    { VEX_W_TABLE (VEX_W_0FED_P_2) },
  },

  /* VEX_LEN_0FEE_P_2 */
  {
    { VEX_W_TABLE (VEX_W_0FEE_P_2) },
  },

  /* VEX_LEN_0FEF_P_2 */
  {
    { VEX_W_TABLE (VEX_W_0FEF_P_2) },
  },

  /* VEX_LEN_0FF1_P_2 */
  {
    { VEX_W_TABLE (VEX_W_0FF1_P_2) },
  },

  /* VEX_LEN_0FF2_P_2 */
  {
    { VEX_W_TABLE (VEX_W_0FF2_P_2) },
  },

  /* VEX_LEN_0FF3_P_2 */
  {
    { VEX_W_TABLE (VEX_W_0FF3_P_2) },
  },

  /* VEX_LEN_0FF4_P_2 */
  {
    { VEX_W_TABLE (VEX_W_0FF4_P_2) },
  },

  /* VEX_LEN_0FF5_P_2 */
  {
    { VEX_W_TABLE (VEX_W_0FF5_P_2) },
  },

  /* VEX_LEN_0FF6_P_2 */
  {
    { VEX_W_TABLE (VEX_W_0FF6_P_2) },
  },

  /* VEX_LEN_0FF7_P_2 */
  {
    { VEX_W_TABLE (VEX_W_0FF7_P_2) },
  },

  /* VEX_LEN_0FF8_P_2 */
  {
    { VEX_W_TABLE (VEX_W_0FF8_P_2) },
  },

  /* VEX_LEN_0FF9_P_2 */
  {
    { VEX_W_TABLE (VEX_W_0FF9_P_2) },
  },

  /* VEX_LEN_0FFA_P_2 */
  {
    { VEX_W_TABLE (VEX_W_0FFA_P_2) },
  },

  /* VEX_LEN_0FFB_P_2 */
  {
    { VEX_W_TABLE (VEX_W_0FFB_P_2) },
  },

  /* VEX_LEN_0FFC_P_2 */
  {
    { VEX_W_TABLE (VEX_W_0FFC_P_2) },
  },

  /* VEX_LEN_0FFD_P_2 */
  {
    { VEX_W_TABLE (VEX_W_0FFD_P_2) },
  },

  /* VEX_LEN_0FFE_P_2 */
  {
    { VEX_W_TABLE (VEX_W_0FFE_P_2) },
  },

  /* VEX_LEN_0F3800_P_2 */
  {
    { VEX_W_TABLE (VEX_W_0F3800_P_2) },
  },

  /* VEX_LEN_0F3801_P_2 */
  {
    { VEX_W_TABLE (VEX_W_0F3801_P_2) },
  },

  /* VEX_LEN_0F3802_P_2 */
  {
    { VEX_W_TABLE (VEX_W_0F3802_P_2) },
  },

  /* VEX_LEN_0F3803_P_2 */
  {
    { VEX_W_TABLE (VEX_W_0F3803_P_2) },
  },

  /* VEX_LEN_0F3804_P_2 */
  {
    { VEX_W_TABLE (VEX_W_0F3804_P_2) },
  },

  /* VEX_LEN_0F3805_P_2 */
  {
    { VEX_W_TABLE (VEX_W_0F3805_P_2) },
  },

  /* VEX_LEN_0F3806_P_2 */
  {
    { VEX_W_TABLE (VEX_W_0F3806_P_2) },
  },

  /* VEX_LEN_0F3807_P_2 */
  {
    { VEX_W_TABLE (VEX_W_0F3807_P_2) },
  },

  /* VEX_LEN_0F3808_P_2 */
  {
    { VEX_W_TABLE (VEX_W_0F3808_P_2) },
  },

  /* VEX_LEN_0F3809_P_2 */
  {
    { VEX_W_TABLE (VEX_W_0F3809_P_2) },
  },

  /* VEX_LEN_0F380A_P_2 */
  {
    { VEX_W_TABLE (VEX_W_0F380A_P_2) },
  },

  /* VEX_LEN_0F380B_P_2 */
  {
    { VEX_W_TABLE (VEX_W_0F380B_P_2) },
  },

  /* VEX_LEN_0F3819_P_2_M_0 */
  {
    { Bad_Opcode },
    { VEX_W_TABLE (VEX_W_0F3819_P_2_M_0) },
  },

  /* VEX_LEN_0F381A_P_2_M_0 */
  {
    { Bad_Opcode },
    { VEX_W_TABLE (VEX_W_0F381A_P_2_M_0) },
  },

  /* VEX_LEN_0F381C_P_2 */
  {
    { VEX_W_TABLE (VEX_W_0F381C_P_2) },
  },

  /* VEX_LEN_0F381D_P_2 */
  {
    { VEX_W_TABLE (VEX_W_0F381D_P_2) },
  },

  /* VEX_LEN_0F381E_P_2 */
  {
    { VEX_W_TABLE (VEX_W_0F381E_P_2) },
  },

  /* VEX_LEN_0F3820_P_2 */
  {
    { VEX_W_TABLE (VEX_W_0F3820_P_2) },
  },

  /* VEX_LEN_0F3821_P_2 */
  {
    { VEX_W_TABLE (VEX_W_0F3821_P_2) },
  },

  /* VEX_LEN_0F3822_P_2 */
  {
    { VEX_W_TABLE (VEX_W_0F3822_P_2) },
  },

  /* VEX_LEN_0F3823_P_2 */
  {
    { VEX_W_TABLE (VEX_W_0F3823_P_2) },
  },

  /* VEX_LEN_0F3824_P_2 */
  {
    { VEX_W_TABLE (VEX_W_0F3824_P_2) },
  },

  /* VEX_LEN_0F3825_P_2 */
  {
    { VEX_W_TABLE (VEX_W_0F3825_P_2) },
  },

  /* VEX_LEN_0F3828_P_2 */
  {
    { VEX_W_TABLE (VEX_W_0F3828_P_2) },
  },

  /* VEX_LEN_0F3829_P_2 */
  {
    { VEX_W_TABLE (VEX_W_0F3829_P_2) },
  },

  /* VEX_LEN_0F382A_P_2_M_0 */
  {
    { VEX_W_TABLE (VEX_W_0F382A_P_2_M_0) },
  },

  /* VEX_LEN_0F382B_P_2 */
  {
    { VEX_W_TABLE (VEX_W_0F382B_P_2) },
  },

  /* VEX_LEN_0F3830_P_2 */
  {
    { VEX_W_TABLE (VEX_W_0F3830_P_2) },
  },

  /* VEX_LEN_0F3831_P_2 */
  {
    { VEX_W_TABLE (VEX_W_0F3831_P_2) },
  },

  /* VEX_LEN_0F3832_P_2 */
  {
    { VEX_W_TABLE (VEX_W_0F3832_P_2) },
  },

  /* VEX_LEN_0F3833_P_2 */
  {
    { VEX_W_TABLE (VEX_W_0F3833_P_2) },
  },

  /* VEX_LEN_0F3834_P_2 */
  {
    { VEX_W_TABLE (VEX_W_0F3834_P_2) },
  },

  /* VEX_LEN_0F3835_P_2 */
  {
    { VEX_W_TABLE (VEX_W_0F3835_P_2) },
  },

  /* VEX_LEN_0F3837_P_2 */
  {
    { VEX_W_TABLE (VEX_W_0F3837_P_2) },
  },

  /* VEX_LEN_0F3838_P_2 */
  {
    { VEX_W_TABLE (VEX_W_0F3838_P_2) },
  },

  /* VEX_LEN_0F3839_P_2 */
  {
    { VEX_W_TABLE (VEX_W_0F3839_P_2) },
  },

  /* VEX_LEN_0F383A_P_2 */
  {
    { VEX_W_TABLE (VEX_W_0F383A_P_2) },
  },

  /* VEX_LEN_0F383B_P_2 */
  {
    { VEX_W_TABLE (VEX_W_0F383B_P_2) },
  },

  /* VEX_LEN_0F383C_P_2 */
  {
    { VEX_W_TABLE (VEX_W_0F383C_P_2) },
  },

  /* VEX_LEN_0F383D_P_2 */
  {
    { VEX_W_TABLE (VEX_W_0F383D_P_2) },
  },

  /* VEX_LEN_0F383E_P_2 */
  {
    { VEX_W_TABLE (VEX_W_0F383E_P_2) },
  },

  /* VEX_LEN_0F383F_P_2 */
  {
    { VEX_W_TABLE (VEX_W_0F383F_P_2) },
  },

  /* VEX_LEN_0F3840_P_2 */
  {
    { VEX_W_TABLE (VEX_W_0F3840_P_2) },
  },

  /* VEX_LEN_0F3841_P_2 */
  {
    { VEX_W_TABLE (VEX_W_0F3841_P_2) },
  },

  /* VEX_LEN_0F38DB_P_2 */
  {
    { VEX_W_TABLE (VEX_W_0F38DB_P_2) },
  },

  /* VEX_LEN_0F38DC_P_2 */
  {
    { VEX_W_TABLE (VEX_W_0F38DC_P_2) },
  },

  /* VEX_LEN_0F38DD_P_2 */
  {
    { VEX_W_TABLE (VEX_W_0F38DD_P_2) },
  },

  /* VEX_LEN_0F38DE_P_2 */
  {
    { VEX_W_TABLE (VEX_W_0F38DE_P_2) },
  },

  /* VEX_LEN_0F38DF_P_2 */
  {
    { VEX_W_TABLE (VEX_W_0F38DF_P_2) },
  },

  /* VEX_LEN_0F3A06_P_2 */
  {
    { Bad_Opcode },
    { VEX_W_TABLE (VEX_W_0F3A06_P_2) },
  },

  /* VEX_LEN_0F3A0A_P_2 */
  {
    { VEX_W_TABLE (VEX_W_0F3A0A_P_2) },
    { VEX_W_TABLE (VEX_W_0F3A0A_P_2) },
  },

  /* VEX_LEN_0F3A0B_P_2 */
  {
    { VEX_W_TABLE (VEX_W_0F3A0B_P_2) },
    { VEX_W_TABLE (VEX_W_0F3A0B_P_2) },
  },

  /* VEX_LEN_0F3A0E_P_2 */
  {
    { VEX_W_TABLE (VEX_W_0F3A0E_P_2) },
  },

  /* VEX_LEN_0F3A0F_P_2 */
  {
    { VEX_W_TABLE (VEX_W_0F3A0F_P_2) },
  },

  /* VEX_LEN_0F3A14_P_2 */
  {
    { VEX_W_TABLE (VEX_W_0F3A14_P_2) },
  },

  /* VEX_LEN_0F3A15_P_2 */
  {
    { VEX_W_TABLE (VEX_W_0F3A15_P_2) },
  },

  /* VEX_LEN_0F3A16_P_2  */
  {
    { "vpextrK",	{ Edq, XM, Ib } },
  },

  /* VEX_LEN_0F3A17_P_2 */
  {
    { "vextractps",	{ Edqd, XM, Ib } },
  },

  /* VEX_LEN_0F3A18_P_2 */
  {
    { Bad_Opcode },
    { VEX_W_TABLE (VEX_W_0F3A18_P_2) },
  },

  /* VEX_LEN_0F3A19_P_2 */
  {
    { Bad_Opcode },
    { VEX_W_TABLE (VEX_W_0F3A19_P_2) },
  },

  /* VEX_LEN_0F3A20_P_2 */
  {
    { VEX_W_TABLE (VEX_W_0F3A20_P_2) },
  },

  /* VEX_LEN_0F3A21_P_2 */
  {
    { VEX_W_TABLE (VEX_W_0F3A21_P_2) },
  },

  /* VEX_LEN_0F3A22_P_2 */
  {
    { "vpinsrK",	{ XM, Vex128, Edq, Ib } },
  },

  /* VEX_LEN_0F3A41_P_2 */
  {
    { VEX_W_TABLE (VEX_W_0F3A41_P_2) },
  },

  /* VEX_LEN_0F3A42_P_2 */
  {
    { VEX_W_TABLE (VEX_W_0F3A42_P_2) },
  },

  /* VEX_LEN_0F3A44_P_2 */
  {
    { VEX_W_TABLE (VEX_W_0F3A44_P_2) },
  },

  /* VEX_LEN_0F3A4C_P_2 */
  {
    { VEX_W_TABLE (VEX_W_0F3A4C_P_2) },
  },

  /* VEX_LEN_0F3A60_P_2 */
  {
    { VEX_W_TABLE (VEX_W_0F3A60_P_2) },
  },

  /* VEX_LEN_0F3A61_P_2 */
  {
    { VEX_W_TABLE (VEX_W_0F3A61_P_2) },
  },

  /* VEX_LEN_0F3A62_P_2 */
  {
    { VEX_W_TABLE (VEX_W_0F3A62_P_2) },
  },

  /* VEX_LEN_0F3A63_P_2 */
  {
    { VEX_W_TABLE (VEX_W_0F3A63_P_2) },
  },

  /* VEX_LEN_0F3A6A_P_2 */
  {
    { "vfmaddss",	{ XMVexW, Vex128, EXdVexW, EXdVexW, VexI4 } },
  },

  /* VEX_LEN_0F3A6B_P_2 */
  {
    { "vfmaddsd",	{ XMVexW, Vex128, EXqVexW, EXqVexW, VexI4 } },
  },

  /* VEX_LEN_0F3A6E_P_2 */
  {
    { "vfmsubss",	{ XMVexW, Vex128, EXdVexW, EXdVexW, VexI4 } },
  },

  /* VEX_LEN_0F3A6F_P_2 */
  {
    { "vfmsubsd",	{ XMVexW, Vex128, EXqVexW, EXqVexW, VexI4 } },
  },

  /* VEX_LEN_0F3A7A_P_2 */
  {
    { "vfnmaddss",	{ XMVexW, Vex128, EXdVexW, EXdVexW, VexI4 } },
  },

  /* VEX_LEN_0F3A7B_P_2 */
  {
    { "vfnmaddsd",	{ XMVexW, Vex128, EXqVexW, EXqVexW, VexI4 } },
  },

  /* VEX_LEN_0F3A7E_P_2 */
  {
    { "vfnmsubss",	{ XMVexW, Vex128, EXdVexW, EXdVexW, VexI4 } },
  },

  /* VEX_LEN_0F3A7F_P_2 */
  {
    { "vfnmsubsd",	{ XMVexW, Vex128, EXqVexW, EXqVexW, VexI4 } },
  },

  /* VEX_LEN_0F3ADF_P_2 */
  {
    { VEX_W_TABLE (VEX_W_0F3ADF_P_2) },
  },

  /* VEX_LEN_0FXOP_09_80 */
  {
    { "vfrczps",	{ XM, EXxmm } },
    { "vfrczps",	{ XM, EXymmq } },
  },

  /* VEX_LEN_0FXOP_09_81 */
  {
    { "vfrczpd",	{ XM, EXxmm } },
    { "vfrczpd",	{ XM, EXymmq } },
  },
};
