const struct dis386 vex_w_table[][2] = {
  {
    /* VEX_W_0F10_P_0 */
    { "vmovups",	{ XM, EXx } },
  },
  {
    /* VEX_W_0F10_P_1 */
    { "vmovss",		{ XMVexScalar, VexScalar, EXdScalar } },
  },
  {
    /* VEX_W_0F10_P_2 */
    { "vmovupd",	{ XM, EXx } },
  },
  {
    /* VEX_W_0F10_P_3 */
    { "vmovsd",		{ XMVexScalar, VexScalar, EXqScalar } },
  },
  {
    /* VEX_W_0F11_P_0 */
    { "vmovups",	{ EXxS, XM } },
  },
  {
    /* VEX_W_0F11_P_1 */
    { "vmovss",		{ EXdVexScalarS, VexScalar, XMScalar } },
  },
  {
    /* VEX_W_0F11_P_2 */
    { "vmovupd",	{ EXxS, XM } },
  },
  {
    /* VEX_W_0F11_P_3 */
    { "vmovsd",		{ EXqVexScalarS, VexScalar, XMScalar } },
  },
  {
    /* VEX_W_0F12_P_0_M_0 */
    { "vmovlps",	{ XM, Vex128, EXq } },
  },
  {
    /* VEX_W_0F12_P_0_M_1 */
    { "vmovhlps",	{ XM, Vex128, EXq } },
  },
  {
    /* VEX_W_0F12_P_1 */
    { "vmovsldup",	{ XM, EXx } },
  },
  {
    /* VEX_W_0F12_P_2 */
    { "vmovlpd",	{ XM, Vex128, EXq } },
  },
  {
    /* VEX_W_0F12_P_3 */
    { "vmovddup",	{ XM, EXymmq } },
  },
  {
    /* VEX_W_0F13_M_0 */
    { "vmovlpX",	{ EXq, XM } },
  },
  {
    /* VEX_W_0F14 */
    { "vunpcklpX",	{ XM, Vex, EXx } },
  },
  {
    /* VEX_W_0F15 */
    { "vunpckhpX",	{ XM, Vex, EXx } },
  },
  {
    /* VEX_W_0F16_P_0_M_0 */
    { "vmovhps",	{ XM, Vex128, EXq } },
  },
  {
    /* VEX_W_0F16_P_0_M_1 */
    { "vmovlhps",	{ XM, Vex128, EXq } },
  },
  {
    /* VEX_W_0F16_P_1 */
    { "vmovshdup",	{ XM, EXx } },
  },
  {
    /* VEX_W_0F16_P_2 */
    { "vmovhpd",	{ XM, Vex128, EXq } },
  },
  {
    /* VEX_W_0F17_M_0 */
    { "vmovhpX",	{ EXq, XM } },
  },
  {
    /* VEX_W_0F28 */
    { "vmovapX",	{ XM, EXx } },
  },
  {
    /* VEX_W_0F29 */
    { "vmovapX",	{ EXxS, XM } },
  },
  {
    /* VEX_W_0F2B_M_0 */
    { "vmovntpX",	{ Mx, XM } },
  },
  {
    /* VEX_W_0F2E_P_0 */
    { "vucomiss",	{ XMScalar, EXdScalar } }, 
  },
  {
    /* VEX_W_0F2E_P_2 */
    { "vucomisd",	{ XMScalar, EXqScalar } }, 
  },
  {
    /* VEX_W_0F2F_P_0 */
    { "vcomiss",	{ XMScalar, EXdScalar } },
  },
  {
    /* VEX_W_0F2F_P_2 */
    { "vcomisd",	{ XMScalar, EXqScalar } },
  },
  {
    /* VEX_W_0F50_M_0 */
    { "vmovmskpX",	{ Gdq, XS } },
  },
  {
    /* VEX_W_0F51_P_0 */
    { "vsqrtps",	{ XM, EXx } },
  },
  {
    /* VEX_W_0F51_P_1 */
    { "vsqrtss",	{ XMScalar, VexScalar, EXdScalar } },
  },
  {
    /* VEX_W_0F51_P_2  */
    { "vsqrtpd",	{ XM, EXx } },
  },
  {
    /* VEX_W_0F51_P_3 */
    { "vsqrtsd",	{ XMScalar, VexScalar, EXqScalar } },
  },
  {
    /* VEX_W_0F52_P_0 */
    { "vrsqrtps",	{ XM, EXx } },
  },
  {
    /* VEX_W_0F52_P_1 */
    { "vrsqrtss",	{ XMScalar, VexScalar, EXdScalar } },
  },
  {
    /* VEX_W_0F53_P_0  */
    { "vrcpps",		{ XM, EXx } },
  },
  {
    /* VEX_W_0F53_P_1  */
    { "vrcpss",		{ XMScalar, VexScalar, EXdScalar } },
  },
  {
    /* VEX_W_0F58_P_0  */
    { "vaddps",		{ XM, Vex, EXx } },
  },
  {
    /* VEX_W_0F58_P_1  */
    { "vaddss",		{ XMScalar, VexScalar, EXdScalar } },
  },
  {
    /* VEX_W_0F58_P_2  */
    { "vaddpd",		{ XM, Vex, EXx } },
  },
  {
    /* VEX_W_0F58_P_3  */
    { "vaddsd",		{ XMScalar, VexScalar, EXqScalar } },
  },
  {
    /* VEX_W_0F59_P_0  */
    { "vmulps",		{ XM, Vex, EXx } },
  },
  {
    /* VEX_W_0F59_P_1  */
    { "vmulss",		{ XMScalar, VexScalar, EXdScalar } },
  },
  {
    /* VEX_W_0F59_P_2  */
    { "vmulpd",		{ XM, Vex, EXx } },
  },
  {
    /* VEX_W_0F59_P_3  */
    { "vmulsd",		{ XMScalar, VexScalar, EXqScalar } },
  },
  {
    /* VEX_W_0F5A_P_0  */
    { "vcvtps2pd",	{ XM, EXxmmq } },
  },
  {
    /* VEX_W_0F5A_P_1  */
    { "vcvtss2sd",	{ XMScalar, VexScalar, EXdScalar } },
  },
  {
    /* VEX_W_0F5A_P_3  */
    { "vcvtsd2ss",	{ XMScalar, VexScalar, EXqScalar } },
  },
  {
    /* VEX_W_0F5B_P_0  */
    { "vcvtdq2ps",	{ XM, EXx } },
  },
  {
    /* VEX_W_0F5B_P_1  */
    { "vcvttps2dq",	{ XM, EXx } },
  },
  {
    /* VEX_W_0F5B_P_2  */
    { "vcvtps2dq",	{ XM, EXx } },
  },
  {
    /* VEX_W_0F5C_P_0  */
    { "vsubps",		{ XM, Vex, EXx } },
  },
  {
    /* VEX_W_0F5C_P_1  */
    { "vsubss",		{ XMScalar, VexScalar, EXdScalar } },
  },
  {
    /* VEX_W_0F5C_P_2  */
    { "vsubpd",		{ XM, Vex, EXx } },
  },
  {
    /* VEX_W_0F5C_P_3  */
    { "vsubsd",		{ XMScalar, VexScalar, EXqScalar } },
  },
  {
    /* VEX_W_0F5D_P_0  */
    { "vminps",		{ XM, Vex, EXx } },
  },
  {
    /* VEX_W_0F5D_P_1  */
    { "vminss",		{ XMScalar, VexScalar, EXdScalar } },
  },
  {
    /* VEX_W_0F5D_P_2  */
    { "vminpd",		{ XM, Vex, EXx } },
  },
  {
    /* VEX_W_0F5D_P_3  */
    { "vminsd",		{ XMScalar, VexScalar, EXqScalar } },
  },
  {
    /* VEX_W_0F5E_P_0  */
    { "vdivps",		{ XM, Vex, EXx } },
  },
  {
    /* VEX_W_0F5E_P_1  */
    { "vdivss",		{ XMScalar, VexScalar, EXdScalar } },
  },
  {
    /* VEX_W_0F5E_P_2  */
    { "vdivpd",		{ XM, Vex, EXx } },
  },
  {
    /* VEX_W_0F5E_P_3  */
    { "vdivsd",		{ XMScalar, VexScalar, EXqScalar } },
  },
  {
    /* VEX_W_0F5F_P_0  */
    { "vmaxps",		{ XM, Vex, EXx } },
  },
  {
    /* VEX_W_0F5F_P_1  */
    { "vmaxss",		{ XMScalar, VexScalar, EXdScalar } },
  },
  {
    /* VEX_W_0F5F_P_2  */
    { "vmaxpd",		{ XM, Vex, EXx } },
  },
  {
    /* VEX_W_0F5F_P_3  */
    { "vmaxsd",		{ XMScalar, VexScalar, EXqScalar } },
  },
  {
    /* VEX_W_0F60_P_2  */
    { "vpunpcklbw",	{ XM, Vex128, EXx } },
  },
  {
    /* VEX_W_0F61_P_2  */
    { "vpunpcklwd",	{ XM, Vex128, EXx } },
  },
  {
    /* VEX_W_0F62_P_2  */
    { "vpunpckldq",	{ XM, Vex128, EXx } },
  },
  {
    /* VEX_W_0F63_P_2  */
    { "vpacksswb",	{ XM, Vex128, EXx } },
  },
  {
    /* VEX_W_0F64_P_2  */
    { "vpcmpgtb",	{ XM, Vex128, EXx } },
  },
  {
    /* VEX_W_0F65_P_2  */
    { "vpcmpgtw",	{ XM, Vex128, EXx } },
  },
  {
    /* VEX_W_0F66_P_2  */
    { "vpcmpgtd",	{ XM, Vex128, EXx } },
  },
  {
    /* VEX_W_0F67_P_2  */
    { "vpackuswb",	{ XM, Vex128, EXx } },
  },
  {
    /* VEX_W_0F68_P_2  */
    { "vpunpckhbw",	{ XM, Vex128, EXx } },
  },
  {
    /* VEX_W_0F69_P_2  */
    { "vpunpckhwd",	{ XM, Vex128, EXx } },
  },
  {
    /* VEX_W_0F6A_P_2  */
    { "vpunpckhdq",	{ XM, Vex128, EXx } },
  },
  {
    /* VEX_W_0F6B_P_2  */
    { "vpackssdw",	{ XM, Vex128, EXx } },
  },
  {
    /* VEX_W_0F6C_P_2  */
    { "vpunpcklqdq",	{ XM, Vex128, EXx } },
  },
  {
    /* VEX_W_0F6D_P_2  */
    { "vpunpckhqdq",	{ XM, Vex128, EXx } },
  },
  {
    /* VEX_W_0F6F_P_1  */
    { "vmovdqu",	{ XM, EXx } },
  },
  {
    /* VEX_W_0F6F_P_2  */
    { "vmovdqa",	{ XM, EXx } },
  },
  {
    /* VEX_W_0F70_P_1 */
    { "vpshufhw",	{ XM, EXx, Ib } },
  },
  {
    /* VEX_W_0F70_P_2 */
    { "vpshufd",	{ XM, EXx, Ib } },
  },
  {
    /* VEX_W_0F70_P_3 */
    { "vpshuflw",	{ XM, EXx, Ib } },
  },
  {
    /* VEX_W_0F71_R_2_P_2  */
    { "vpsrlw",		{ Vex128, XS, Ib } },
  },
  {
    /* VEX_W_0F71_R_4_P_2  */
    { "vpsraw",		{ Vex128, XS, Ib } },
  },
  {
    /* VEX_W_0F71_R_6_P_2  */
    { "vpsllw",		{ Vex128, XS, Ib } },
  },
  {
    /* VEX_W_0F72_R_2_P_2  */
    { "vpsrld",		{ Vex128, XS, Ib } },
  },
  {
    /* VEX_W_0F72_R_4_P_2  */
    { "vpsrad",		{ Vex128, XS, Ib } },
  },
  {
    /* VEX_W_0F72_R_6_P_2  */
    { "vpslld",		{ Vex128, XS, Ib } },
  },
  {
    /* VEX_W_0F73_R_2_P_2  */
    { "vpsrlq",		{ Vex128, XS, Ib } },
  },
  {
    /* VEX_W_0F73_R_3_P_2  */
    { "vpsrldq",	{ Vex128, XS, Ib } },
  },
  {
    /* VEX_W_0F73_R_6_P_2  */
    { "vpsllq",		{ Vex128, XS, Ib } },
  },
  {
    /* VEX_W_0F73_R_7_P_2  */
    { "vpslldq",	{ Vex128, XS, Ib } },
  },
  {
    /* VEX_W_0F74_P_2 */
    { "vpcmpeqb",	{ XM, Vex128, EXx } },
  },
  {
    /* VEX_W_0F75_P_2 */
    { "vpcmpeqw",	{ XM, Vex128, EXx } },
  },
  {
    /* VEX_W_0F76_P_2 */
    { "vpcmpeqd",	{ XM, Vex128, EXx } },
  },
  {
    /* VEX_W_0F77_P_0 */
    { "",		{ VZERO } },
  },
  {
    /* VEX_W_0F7C_P_2 */
    { "vhaddpd",	{ XM, Vex, EXx } },
  },
  {
    /* VEX_W_0F7C_P_3 */
    { "vhaddps",	{ XM, Vex, EXx } },
  },
  {
    /* VEX_W_0F7D_P_2 */
    { "vhsubpd",	{ XM, Vex, EXx } },
  },
  {
    /* VEX_W_0F7D_P_3 */
    { "vhsubps",	{ XM, Vex, EXx } },
  },
  {
    /* VEX_W_0F7E_P_1 */
    { "vmovq",		{ XMScalar, EXqScalar } },
  },
  {
    /* VEX_W_0F7F_P_1 */
    { "vmovdqu",	{ EXxS, XM } },
  },
  {
    /* VEX_W_0F7F_P_2 */
    { "vmovdqa",	{ EXxS, XM } },
  },
  {
    /* VEX_W_0FAE_R_2_M_0 */
    { "vldmxcsr",	{ Md } },
  },
  {
    /* VEX_W_0FAE_R_3_M_0 */
    { "vstmxcsr",	{ Md } },
  },
  {
    /* VEX_W_0FC2_P_0 */
    { "vcmpps",		{ XM, Vex, EXx, VCMP } },
  },
  {
    /* VEX_W_0FC2_P_1 */
    { "vcmpss",		{ XMScalar, VexScalar, EXdScalar, VCMP } },
  },
  {
    /* VEX_W_0FC2_P_2 */
    { "vcmppd",		{ XM, Vex, EXx, VCMP } },
  },
  {
    /* VEX_W_0FC2_P_3 */
    { "vcmpsd",		{ XMScalar, VexScalar, EXqScalar, VCMP } },
  },
  {
    /* VEX_W_0FC4_P_2 */
    { "vpinsrw",	{ XM, Vex128, Edqw, Ib } },
  },
  {
    /* VEX_W_0FC5_P_2 */
    { "vpextrw",	{ Gdq, XS, Ib } },
  },
  {
    /* VEX_W_0FD0_P_2 */
    { "vaddsubpd",	{ XM, Vex, EXx } },
  },
  {
    /* VEX_W_0FD0_P_3 */
    { "vaddsubps",	{ XM, Vex, EXx } },
  },
  {
    /* VEX_W_0FD1_P_2 */
    { "vpsrlw",		{ XM, Vex128, EXx } },
  },
  {
    /* VEX_W_0FD2_P_2 */
    { "vpsrld",		{ XM, Vex128, EXx } },
  },
  {
    /* VEX_W_0FD3_P_2 */
    { "vpsrlq",		{ XM, Vex128, EXx } },
  },
  {
    /* VEX_W_0FD4_P_2 */
    { "vpaddq",		{ XM, Vex128, EXx } },
  },
  {
    /* VEX_W_0FD5_P_2 */
    { "vpmullw",	{ XM, Vex128, EXx } },
  },
  {
    /* VEX_W_0FD6_P_2 */
    { "vmovq",		{ EXqScalarS, XMScalar } },
  },
  {
    /* VEX_W_0FD7_P_2_M_1 */
    { "vpmovmskb",	{ Gdq, XS } },
  },
  {
    /* VEX_W_0FD8_P_2 */
    { "vpsubusb",	{ XM, Vex128, EXx } },
  },
  {
    /* VEX_W_0FD9_P_2 */
    { "vpsubusw",	{ XM, Vex128, EXx } },
  },
  {
    /* VEX_W_0FDA_P_2 */
    { "vpminub",	{ XM, Vex128, EXx } },
  },
  {
    /* VEX_W_0FDB_P_2 */
    { "vpand",		{ XM, Vex128, EXx } },
  },
  {
    /* VEX_W_0FDC_P_2 */
    { "vpaddusb",	{ XM, Vex128, EXx } },
  },
  {
    /* VEX_W_0FDD_P_2 */
    { "vpaddusw",	{ XM, Vex128, EXx } },
  },
  {
    /* VEX_W_0FDE_P_2 */
    { "vpmaxub",	{ XM, Vex128, EXx } },
  },
  {
    /* VEX_W_0FDF_P_2 */
    { "vpandn",		{ XM, Vex128, EXx } },
  },
  {
    /* VEX_W_0FE0_P_2  */
    { "vpavgb",		{ XM, Vex128, EXx } },
  },
  {
    /* VEX_W_0FE1_P_2  */
    { "vpsraw",		{ XM, Vex128, EXx } },
  },
  {
    /* VEX_W_0FE2_P_2  */
    { "vpsrad",		{ XM, Vex128, EXx } },
  },
  {
    /* VEX_W_0FE3_P_2  */
    { "vpavgw",		{ XM, Vex128, EXx } },
  },
  {
    /* VEX_W_0FE4_P_2  */
    { "vpmulhuw",	{ XM, Vex128, EXx } },
  },
  {
    /* VEX_W_0FE5_P_2  */
    { "vpmulhw",	{ XM, Vex128, EXx } },
  },
  {
    /* VEX_W_0FE6_P_1  */
    { "vcvtdq2pd",	{ XM, EXxmmq } },
  },
  {
    /* VEX_W_0FE6_P_2  */
    { "vcvttpd2dq%XY",	{ XMM, EXx } },
  },
  {
    /* VEX_W_0FE6_P_3  */
    { "vcvtpd2dq%XY",	{ XMM, EXx } },
  },
  {
    /* VEX_W_0FE7_P_2_M_0 */
    { "vmovntdq",	{ Mx, XM } },
  },
  {
    /* VEX_W_0FE8_P_2  */
    { "vpsubsb",	{ XM, Vex128, EXx } },
  },
  {
    /* VEX_W_0FE9_P_2  */
    { "vpsubsw",	{ XM, Vex128, EXx } },
  },
  {
    /* VEX_W_0FEA_P_2  */
    { "vpminsw",	{ XM, Vex128, EXx } },
  },
  {
    /* VEX_W_0FEB_P_2  */
    { "vpor",		{ XM, Vex128, EXx } },
  },
  {
    /* VEX_W_0FEC_P_2  */
    { "vpaddsb",	{ XM, Vex128, EXx } },
  },
  {
    /* VEX_W_0FED_P_2  */
    { "vpaddsw",	{ XM, Vex128, EXx } },
  },
  {
    /* VEX_W_0FEE_P_2  */
    { "vpmaxsw",	{ XM, Vex128, EXx } },
  },
  {
    /* VEX_W_0FEF_P_2  */
    { "vpxor",		{ XM, Vex128, EXx } },
  },
  {
    /* VEX_W_0FF0_P_3_M_0 */
    { "vlddqu",		{ XM, M } },
  },
  {
    /* VEX_W_0FF1_P_2 */
    { "vpsllw",		{ XM, Vex128, EXx } },
  },
  {
    /* VEX_W_0FF2_P_2 */
    { "vpslld",		{ XM, Vex128, EXx } },
  },
  {
    /* VEX_W_0FF3_P_2 */
    { "vpsllq",		{ XM, Vex128, EXx } },
  },
  {
    /* VEX_W_0FF4_P_2 */
    { "vpmuludq",	{ XM, Vex128, EXx } },
  },
  {
    /* VEX_W_0FF5_P_2 */
    { "vpmaddwd",	{ XM, Vex128, EXx } },
  },
  {
    /* VEX_W_0FF6_P_2 */
    { "vpsadbw",	{ XM, Vex128, EXx } },
  },
  {
    /* VEX_W_0FF7_P_2 */
    { "vmaskmovdqu",	{ XM, XS } },
  },
  {
    /* VEX_W_0FF8_P_2 */
    { "vpsubb",		{ XM, Vex128, EXx } },
  },
  {
    /* VEX_W_0FF9_P_2 */
    { "vpsubw",		{ XM, Vex128, EXx } },
  },
  {
    /* VEX_W_0FFA_P_2 */
    { "vpsubd",		{ XM, Vex128, EXx } },
  },
  {
    /* VEX_W_0FFB_P_2 */
    { "vpsubq",		{ XM, Vex128, EXx } },
  },
  {
    /* VEX_W_0FFC_P_2 */
    { "vpaddb",		{ XM, Vex128, EXx } },
  },
  {
    /* VEX_W_0FFD_P_2 */
    { "vpaddw",		{ XM, Vex128, EXx } },
  },
  {
    /* VEX_W_0FFE_P_2 */
    { "vpaddd",		{ XM, Vex128, EXx } },
  },
  {
    /* VEX_W_0F3800_P_2  */
    { "vpshufb",	{ XM, Vex128, EXx } },
  },
  {
    /* VEX_W_0F3801_P_2  */
    { "vphaddw",	{ XM, Vex128, EXx } },
  },
  {
    /* VEX_W_0F3802_P_2  */
    { "vphaddd",	{ XM, Vex128, EXx } },
  },
  {
    /* VEX_W_0F3803_P_2  */
    { "vphaddsw",	{ XM, Vex128, EXx } },
  },
  {
    /* VEX_W_0F3804_P_2  */
    { "vpmaddubsw",	{ XM, Vex128, EXx } },
  },
  {
    /* VEX_W_0F3805_P_2  */
    { "vphsubw",	{ XM, Vex128, EXx } },
  },
  {
    /* VEX_W_0F3806_P_2  */
    { "vphsubd",	{ XM, Vex128, EXx } },
  },
  {
    /* VEX_W_0F3807_P_2  */
    { "vphsubsw",	{ XM, Vex128, EXx } },
  },
  {
    /* VEX_W_0F3808_P_2  */
    { "vpsignb",	{ XM, Vex128, EXx } },
  },
  {
    /* VEX_W_0F3809_P_2  */
    { "vpsignw",	{ XM, Vex128, EXx } },
  },
  {
    /* VEX_W_0F380A_P_2  */
    { "vpsignd",	{ XM, Vex128, EXx } },
  },
  {
    /* VEX_W_0F380B_P_2  */
    { "vpmulhrsw",	{ XM, Vex128, EXx } },
  },
  {
    /* VEX_W_0F380C_P_2  */
    { "vpermilps",	{ XM, Vex, EXx } },
  },
  {
    /* VEX_W_0F380D_P_2  */
    { "vpermilpd",	{ XM, Vex, EXx } },
  },
  {
    /* VEX_W_0F380E_P_2  */
    { "vtestps",	{ XM, EXx } },
  },
  {
    /* VEX_W_0F380F_P_2  */
    { "vtestpd",	{ XM, EXx } },
  },
  {
    /* VEX_W_0F3817_P_2 */
    { "vptest",		{ XM, EXx } },
  },
  {
    /* VEX_W_0F3818_P_2_M_0 */
    { "vbroadcastss",	{ XM, Md } },
  },
  {
    /* VEX_W_0F3819_P_2_M_0 */
    { "vbroadcastsd",	{ XM, Mq } },
  },
  {
    /* VEX_W_0F381A_P_2_M_0 */
    { "vbroadcastf128",	{ XM, Mxmm } },
  },
  {
    /* VEX_W_0F381C_P_2 */
    { "vpabsb",		{ XM, EXx } },
  },
  {
    /* VEX_W_0F381D_P_2 */
    { "vpabsw",		{ XM, EXx } },
  },
  {
    /* VEX_W_0F381E_P_2 */
    { "vpabsd",		{ XM, EXx } },
  },
  {
    /* VEX_W_0F3820_P_2 */
    { "vpmovsxbw",	{ XM, EXq } },
  },
  {
    /* VEX_W_0F3821_P_2 */
    { "vpmovsxbd",	{ XM, EXd } },
  },
  {
    /* VEX_W_0F3822_P_2 */
    { "vpmovsxbq",	{ XM, EXw } },
  },
  {
    /* VEX_W_0F3823_P_2 */
    { "vpmovsxwd",	{ XM, EXq } },
  },
  {
    /* VEX_W_0F3824_P_2 */
    { "vpmovsxwq",	{ XM, EXd } },
  },
  {
    /* VEX_W_0F3825_P_2 */
    { "vpmovsxdq",	{ XM, EXq } },
  },
  {
    /* VEX_W_0F3828_P_2 */
    { "vpmuldq",	{ XM, Vex128, EXx } },
  },
  {
    /* VEX_W_0F3829_P_2 */
    { "vpcmpeqq",	{ XM, Vex128, EXx } },
  },
  {
    /* VEX_W_0F382A_P_2_M_0 */
    { "vmovntdqa",	{ XM, Mx } },
  },
  {
    /* VEX_W_0F382B_P_2 */
    { "vpackusdw",	{ XM, Vex128, EXx } },
  },
  {
    /* VEX_W_0F382C_P_2_M_0 */
    { "vmaskmovps",	{ XM, Vex, Mx } },
  },
  {
    /* VEX_W_0F382D_P_2_M_0 */
    { "vmaskmovpd",	{ XM, Vex, Mx } },
  },
  {
    /* VEX_W_0F382E_P_2_M_0 */
    { "vmaskmovps",	{ Mx, Vex, XM } },
  },
  {
    /* VEX_W_0F382F_P_2_M_0 */
    { "vmaskmovpd",	{ Mx, Vex, XM } },
  },
  {
    /* VEX_W_0F3830_P_2 */
    { "vpmovzxbw",	{ XM, EXq } },
  },
  {
    /* VEX_W_0F3831_P_2 */
    { "vpmovzxbd",	{ XM, EXd } },
  },
  {
    /* VEX_W_0F3832_P_2 */
    { "vpmovzxbq",	{ XM, EXw } },
  },
  {
    /* VEX_W_0F3833_P_2 */
    { "vpmovzxwd",	{ XM, EXq } },
  },
  {
    /* VEX_W_0F3834_P_2 */
    { "vpmovzxwq",	{ XM, EXd } },
  },
  {
    /* VEX_W_0F3835_P_2 */
    { "vpmovzxdq",	{ XM, EXq } },
  },
  {
    /* VEX_W_0F3837_P_2 */
    { "vpcmpgtq",	{ XM, Vex128, EXx } },
  },
  {
    /* VEX_W_0F3838_P_2 */
    { "vpminsb",	{ XM, Vex128, EXx } },
  },
  {
    /* VEX_W_0F3839_P_2 */
    { "vpminsd",	{ XM, Vex128, EXx } },
  },
  {
    /* VEX_W_0F383A_P_2 */
    { "vpminuw",	{ XM, Vex128, EXx } },
  },
  {
    /* VEX_W_0F383B_P_2 */
    { "vpminud",	{ XM, Vex128, EXx } },
  },
  {
    /* VEX_W_0F383C_P_2 */
    { "vpmaxsb",	{ XM, Vex128, EXx } },
  },
  {
    /* VEX_W_0F383D_P_2 */
    { "vpmaxsd",	{ XM, Vex128, EXx } },
  },
  {
    /* VEX_W_0F383E_P_2 */
    { "vpmaxuw",	{ XM, Vex128, EXx } },
  },
  {
    /* VEX_W_0F383F_P_2 */
    { "vpmaxud",	{ XM, Vex128, EXx } },
  },
  {
    /* VEX_W_0F3840_P_2 */
    { "vpmulld",	{ XM, Vex128, EXx } },
  },
  {
    /* VEX_W_0F3841_P_2 */
    { "vphminposuw",	{ XM, EXx } },
  },
  {
    /* VEX_W_0F38DB_P_2 */
    { "vaesimc",	{ XM, EXx } },
  },
  {
    /* VEX_W_0F38DC_P_2 */
    { "vaesenc",	{ XM, Vex128, EXx } },
  },
  {
    /* VEX_W_0F38DD_P_2 */
    { "vaesenclast",	{ XM, Vex128, EXx } },
  },
  {
    /* VEX_W_0F38DE_P_2 */
    { "vaesdec",	{ XM, Vex128, EXx } },
  },
  {
    /* VEX_W_0F38DF_P_2 */
    { "vaesdeclast",	{ XM, Vex128, EXx } },
  },
  {
    /* VEX_W_0F3A04_P_2 */
    { "vpermilps",	{ XM, EXx, Ib } },
  },
  {
    /* VEX_W_0F3A05_P_2 */
    { "vpermilpd",	{ XM, EXx, Ib } },
  },
  {
    /* VEX_W_0F3A06_P_2 */
    { "vperm2f128",	{ XM, Vex256, EXx, Ib } },
  },
  {
    /* VEX_W_0F3A08_P_2 */
    { "vroundps",	{ XM, EXx, Ib } },
  },
  {
    /* VEX_W_0F3A09_P_2 */
    { "vroundpd",	{ XM, EXx, Ib } },
  },
  {
    /* VEX_W_0F3A0A_P_2 */
    { "vroundss",	{ XMScalar, VexScalar, EXdScalar, Ib } },
  },
  {
    /* VEX_W_0F3A0B_P_2 */
    { "vroundsd",	{ XMScalar, VexScalar, EXqScalar, Ib } },
  },
  {
    /* VEX_W_0F3A0C_P_2 */
    { "vblendps",	{ XM, Vex, EXx, Ib } },
  },
  {
    /* VEX_W_0F3A0D_P_2 */
    { "vblendpd",	{ XM, Vex, EXx, Ib } },
  },
  {
    /* VEX_W_0F3A0E_P_2 */
    { "vpblendw",	{ XM, Vex128, EXx, Ib } },
  },
  {
    /* VEX_W_0F3A0F_P_2 */
    { "vpalignr",	{ XM, Vex128, EXx, Ib } },
  },
  {
    /* VEX_W_0F3A14_P_2 */
    { "vpextrb",	{ Edqb, XM, Ib } },
  },
  {
    /* VEX_W_0F3A15_P_2 */
    { "vpextrw",	{ Edqw, XM, Ib } },
  },
  {
    /* VEX_W_0F3A18_P_2 */
    { "vinsertf128",	{ XM, Vex256, EXxmm, Ib } },
  },
  {
    /* VEX_W_0F3A19_P_2 */
    { "vextractf128",	{ EXxmm, XM, Ib } },
  },
  {
    /* VEX_W_0F3A20_P_2 */
    { "vpinsrb",	{ XM, Vex128, Edqb, Ib } },
  },
  {
    /* VEX_W_0F3A21_P_2 */
    { "vinsertps",	{ XM, Vex128, EXd, Ib } },
  },
  {
    /* VEX_W_0F3A40_P_2 */
    { "vdpps",		{ XM, Vex, EXx, Ib } },
  },
  {
    /* VEX_W_0F3A41_P_2 */
    { "vdppd",		{ XM, Vex128, EXx, Ib } },
  },
  {
    /* VEX_W_0F3A42_P_2 */
    { "vmpsadbw",	{ XM, Vex128, EXx, Ib } },
  },
  {
    /* VEX_W_0F3A44_P_2 */
    { "vpclmulqdq",	{ XM, Vex128, EXx, PCLMUL } },
  },
  {
    /* VEX_W_0F3A48_P_2 */
    { "vpermil2ps",	{ XMVexW, Vex, EXVexImmW, EXVexImmW, EXVexImmW } },
    { "vpermil2ps",	{ XMVexW, Vex, EXVexImmW, EXVexImmW, EXVexImmW } },
  },
  {
    /* VEX_W_0F3A49_P_2 */
    { "vpermil2pd",	{ XMVexW, Vex, EXVexImmW, EXVexImmW, EXVexImmW } },
    { "vpermil2pd",	{ XMVexW, Vex, EXVexImmW, EXVexImmW, EXVexImmW } },
  },
  {
    /* VEX_W_0F3A4A_P_2 */
    { "vblendvps",	{ XM, Vex, EXx, XMVexI4 } },
  },
  {
    /* VEX_W_0F3A4B_P_2 */
    { "vblendvpd",	{ XM, Vex, EXx, XMVexI4 } },
  },
  {
    /* VEX_W_0F3A4C_P_2 */
    { "vpblendvb",	{ XM, Vex128, EXx, XMVexI4 } },
  },
  {
    /* VEX_W_0F3A60_P_2 */
    { "vpcmpestrm",	{ XM, EXx, Ib } },
  },
  {
    /* VEX_W_0F3A61_P_2 */
    { "vpcmpestri",	{ XM, EXx, Ib } },
  },
  {
    /* VEX_W_0F3A62_P_2 */
    { "vpcmpistrm",	{ XM, EXx, Ib } },
  },
  {
    /* VEX_W_0F3A63_P_2 */
    { "vpcmpistri",	{ XM, EXx, Ib } },
  },
  {
    /* VEX_W_0F3ADF_P_2 */
    { "vaeskeygenassist", { XM, EXx, Ib } },
  },
};
