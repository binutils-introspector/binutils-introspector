 const struct dis386 float_reg[][8] = {
  /* d8 */
  {
    { "fadd",	{ ST, STi } },
    { "fmul",	{ ST, STi } },
    { "fcom",	{ STi } },
    { "fcomp",	{ STi } },
    { "fsub",	{ ST, STi } },
    { "fsubr",	{ ST, STi } },
    { "fdiv",	{ ST, STi } },
    { "fdivr",	{ ST, STi } },
  },
  /* d9 */
  {
    { "fld",	{ STi } },
    { "fxch",	{ STi } },
    { FGRPd9_2 },
    { Bad_Opcode },
    { FGRPd9_4 },
    { FGRPd9_5 },
    { FGRPd9_6 },
    { FGRPd9_7 },
  },
  /* da */
  {
    { "fcmovb",	{ ST, STi } },
    { "fcmove",	{ ST, STi } },
    { "fcmovbe",{ ST, STi } },
    { "fcmovu",	{ ST, STi } },
    { Bad_Opcode },
    { FGRPda_5 },
    { Bad_Opcode },
    { Bad_Opcode },
  },
  /* db */
  {
    { "fcmovnb",{ ST, STi } },
    { "fcmovne",{ ST, STi } },
    { "fcmovnbe",{ ST, STi } },
    { "fcmovnu",{ ST, STi } },
    { FGRPdb_4 },
    { "fucomi",	{ ST, STi } },
    { "fcomi",	{ ST, STi } },
    { Bad_Opcode },
  },
  /* dc */
  {
    { "fadd",	{ STi, ST } },
    { "fmul",	{ STi, ST } },
    { Bad_Opcode },
    { Bad_Opcode },
    { "fsub!M",	{ STi, ST } },
    { "fsubM",	{ STi, ST } },
    { "fdiv!M",	{ STi, ST } },
    { "fdivM",	{ STi, ST } },
  },
  /* dd */
  {
    { "ffree",	{ STi } },
    { Bad_Opcode },
    { "fst",	{ STi } },
    { "fstp",	{ STi } },
    { "fucom",	{ STi } },
    { "fucomp",	{ STi } },
    { Bad_Opcode },
    { Bad_Opcode },
  },
  /* de */
  {
    { "faddp",	{ STi, ST } },
    { "fmulp",	{ STi, ST } },
    { Bad_Opcode },
    { FGRPde_3 },
    { "fsub!Mp", { STi, ST } },
    { "fsubMp",	{ STi, ST } },
    { "fdiv!Mp", { STi, ST } },
    { "fdivMp",	{ STi, ST } },
  },
  /* df */
  {
    { "ffreep",	{ STi } },
    { Bad_Opcode },
    { Bad_Opcode },
    { Bad_Opcode },
    { FGRPdf_4 },
    { "fucomip", { ST, STi } },
    { "fcomip", { ST, STi } },
    { Bad_Opcode },
  },
};
