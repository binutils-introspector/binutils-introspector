/* Print i386 instructions for GDB, the GNU debugger.
   Copyright 1988, 1989, 1991, 1993, 1994, 1995, 1996, 1997, 1998, 1999,
   2001, 2002, 2003, 2004, 2005, 2006, 2007, 2008, 2009, 2010
   Free Software Foundation, Inc.

   This file is part of the GNU opcodes library.

   This library is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3, or (at your option)
   any later version.

   It is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 51 Franklin Street - Fifth Floor, Boston,
   MA 02110-1301, USA.  */


/* 80386 instruction printer by Pace Willisson (pace@prep.ai.mit.edu)
   July 1988
    modified by John Hassey (hassey@dg-rtp.dg.com)
    x86-64 support added by Jan Hubicka (jh@suse.cz)
    VIA PadLock support by Michal Ludvig (mludvig@suse.cz).  */

/* The main tables describing the instructions is essentially a copy
   of the "Opcode Map" chapter (Appendix A) of the Intel 80386
   Programmers Manual.  Usually, there is a capital letter, followed
   by a small letter.  The capital letter tell the addressing mode,
   and the small letter tells about the operand size.  Refer to
   the Intel manual for details.  */


#include "i386-dis-std-inc.h"
using namespace std;

namespace binutils_objdump_opcodes_i386
{


/* Make sure that bytes from INFO->PRIVATE_DATA->BUFFER (inclusive)
   to ADDR (exclusive) are valid.  Returns 1 for success, longjmps
   on error.  */


 int
fetch_data (struct disassemble_info *info, bfd_byte *addr)
{
  int status;
  struct dis_private *priv = (struct dis_private *) info->private_data;
  bfd_vma start = priv->insn_start + (priv->max_fetched - priv->the_buffer);

  if (addr <= priv->the_buffer + MAX_MNEM_SIZE)
    status = (*info->read_memory_func) (start,
					priv->max_fetched,
					addr - priv->max_fetched,
					info);
  else
    status = -1;
  if (status != 0)
    {
      /* If we did manage to read at least one byte, then
	 print_insn_i386 will do something sensible.  Otherwise, print
	 an error.  We do that here because this is where we know
	 STATUS.  */
      if (priv->max_fetched == priv->the_buffer)
	(*info->memory_error_func) (status, start, info);
      longjmp (priv->bailout, 1);
    }
  else
    priv->max_fetched = addr;
  return 1;
}


/* If we are accessing mod/rm/reg without need_modrm set, then the
   values are stale.  Hitting this abort likely indicates that you
   need to update onebyte_has_modrm or twobyte_has_modrm.  */

  void DOMODRM_CHECK(char need_modrm)
  {
    if (!need_modrm) 
      {
	cerr << "MODRM_CHECK" << endl;
	abort ();
      }
  }

#define MODRM_CHECK  DOMODRM_CHECK(need_modrm);



 int
   StaticData::ckprefix (void)
{
  int newrex, i, length;
  rex = 0;
  rex_ignored = 0;
  prefixes = 0;
  used_prefixes = 0;
  rex_used = 0;
  last_lock_prefix = -1;
  last_repz_prefix = -1;
  last_repnz_prefix = -1;
  last_data_prefix = -1;
  last_addr_prefix = -1;
  last_rex_prefix = -1;
  last_seg_prefix = -1;
  for (i = 0; i < (int) ARRAY_SIZE (all_prefixes); i++)
    all_prefixes[i] = 0;
  i = 0;
  length = 0;
  /* The maximum instruction length is 15bytes.  */
  while (length < MAX_CODE_LENGTH - 1)
    {
      FETCH_DATA (the_info, codep + 1);
      newrex = 0;
      switch (*codep)
	{
	/* REX prefixes family.  */
	case 0x40:
	case 0x41:
	case 0x42:
	case 0x43:
	case 0x44:
	case 0x45:
	case 0x46:
	case 0x47:
	case 0x48:
	case 0x49:
	case 0x4a:
	case 0x4b:
	case 0x4c:
	case 0x4d:
	case 0x4e:
	case 0x4f:
	  if (address_mode == mode_64bit)
	    newrex = *codep;
	  else
	    return 1;
	  last_rex_prefix = i;
	  break;
	case 0xf3:
	  prefixes |= PREFIX_REPZ;
	  last_repz_prefix = i;
	  break;
	case 0xf2:
	  prefixes |= PREFIX_REPNZ;
	  last_repnz_prefix = i;
	  break;
	case 0xf0:
	  prefixes |= PREFIX_LOCK;
	  last_lock_prefix = i;
	  break;
	case 0x2e:
	  prefixes |= PREFIX_CS;
	  last_seg_prefix = i;
	  break;
	case 0x36:
	  prefixes |= PREFIX_SS;
	  last_seg_prefix = i;
	  break;
	case 0x3e:
	  prefixes |= PREFIX_DS;
	  last_seg_prefix = i;
	  break;
	case 0x26:
	  prefixes |= PREFIX_ES;
	  last_seg_prefix = i;
	  break;
	case 0x64:
	  prefixes |= PREFIX_FS;
	  last_seg_prefix = i;
	  break;
	case 0x65:
	  prefixes |= PREFIX_GS;
	  last_seg_prefix = i;
	  break;
	case 0x66:
	  prefixes |= PREFIX_DATA;
	  last_data_prefix = i;
	  break;
	case 0x67:
	  prefixes |= PREFIX_ADDR;
	  last_addr_prefix = i;
	  break;
	case FWAIT_OPCODE:
	  /* fwait is really an instruction.  If there are prefixes
	     before the fwait, they belong to the fwait, *not* to the
	     following instruction.  */
	  if (prefixes || rex)
	    {
	      prefixes |= PREFIX_FWAIT;
	      codep++;
	      return 1;
	    }
	  prefixes = PREFIX_FWAIT;
	  break;
	default:
	  return 1;
	}
      /* Rex is ignored when followed by another prefix.  */
      if (rex)
	{
	  rex_used = rex;
	  return 1;
	}
      if (*codep != FWAIT_OPCODE)
	all_prefixes[i++] = *codep;
      rex = newrex;
      codep++;
      length++;
    }
  return 0;
}

 int
seg_prefix (int pref)
{
  switch (pref)
    {
    case 0x2e:
      return PREFIX_CS;
    case 0x36:
      return PREFIX_SS;
    case 0x3e:
      return PREFIX_DS;
    case 0x26:
      return PREFIX_ES;
    case 0x64:
      return PREFIX_FS;
    case 0x65:
      return PREFIX_GS;
    default:
      return 0;
    }
}

/* Return the name of the prefix byte PREF, or NULL if PREF is not a
   prefix byte.  */

int DOFETCH_DATA(struct disassemble_info *info, bfd_byte *addr) 
{
  return ((addr) <= ((struct dis_private *) (info->private_data))->max_fetched 
	  ? 1 : fetch_data ((info), (addr)));
}

 const char *  StaticData::prefix_name (int pref, int sizeflag)
 {
   switch (pref)
     {
    /* REX prefixes family.  */
    case 0x40:
    case 0x41:
    case 0x42:
    case 0x43:
    case 0x44:
    case 0x45:
    case 0x46:
    case 0x47:
    case 0x48:
    case 0x49:
    case 0x4a:
    case 0x4b:
    case 0x4c:
    case 0x4d:
    case 0x4e:
    case 0x4f:
      return rexes [pref - 0x40];
    case 0xf3:
      return "repz";
    case 0xf2:
      return "repnz";
    case 0xf0:
      return "lock";
    case 0x2e:
      return "cs";
    case 0x36:
      return "ss";
    case 0x3e:
      return "ds";
    case 0x26:
      return "es";
    case 0x64:
      return "fs";
    case 0x65:
      return "gs";
    case 0x66:
      return (sizeflag & DFLAG) ? "data16" : "data32";
    case 0x67:
      if (address_mode == mode_64bit)
	return (sizeflag & AFLAG) ? "addr32" : "addr64";
      else
	return (sizeflag & AFLAG) ? "addr16" : "addr32";
    case FWAIT_OPCODE:
      return "fwait";
    case ADDR16_PREFIX:
      return "addr16";
    case ADDR32_PREFIX:
      return "addr32";
    case DATA16_PREFIX:
      return "data16";
    case DATA32_PREFIX:
      return "data32";
    case REP_PREFIX:
      return "rep";
    default:
      return NULL;
    }
}


/* Here for backwards compatibility.  When gdb stops using
   print_insn_i386_att and print_insn_i386_intel these functions can
   disappear, and print_insn_i386 be merged into print_insn.  */
int
  StaticData::print_insn_i386_att (bfd_vma pc, disassemble_info *info)
{
  intel_syntax = 0;

  return print_insn (pc, info);
}

int
  StaticData::print_insn_i386_intel (bfd_vma pc, disassemble_info *info)
{
  intel_syntax = 1;

  return print_insn (pc, info);
}

int
StaticData::print_insn_i386 (bfd_vma pc, disassemble_info *info)
{
  intel_syntax = -1;

  return print_insn (pc, info);
}

void
StaticData::print_i386_disassembler_options (FILE *stream)
{
  fprintf (stream, _("\n\
The following i386/x86-64 specific disassembler options are supported for use\n\
with the -M switch (multiple options should be separated by commas):\n"));

  fprintf (stream, _("  x86-64      Disassemble in 64bit mode\n"));
  fprintf (stream, _("  i386        Disassemble in 32bit mode\n"));
  fprintf (stream, _("  i8086       Disassemble in 16bit mode\n"));
  fprintf (stream, _("  att         Display instruction in AT&T syntax\n"));
  fprintf (stream, _("  intel       Display instruction in Intel syntax\n"));
  fprintf (stream, _("  att-mnemonic\n"
		     "              Display instruction in AT&T mnemonic\n"));
  fprintf (stream, _("  intel-mnemonic\n"
		     "              Display instruction in Intel mnemonic\n"));
  fprintf (stream, _("  addr64      Assume 64bit address size\n"));
  fprintf (stream, _("  addr32      Assume 32bit address size\n"));
  fprintf (stream, _("  addr16      Assume 16bit address size\n"));
  fprintf (stream, _("  data32      Assume 32bit data size\n"));
  fprintf (stream, _("  data16      Assume 16bit data size\n"));
  fprintf (stream, _("  suffix      Always display instruction suffix in AT&T syntax\n"));
}

/* Bad opcode.  */
//static const struct dis386 bad_opcode = { "(bad)", { XX } };

/* Get a pointer to struct dis386 with a valid name.  */

const struct dis386 *
StaticData::get_valid_dis386 (const struct dis386 *dp, disassemble_info *info)
{
  int vindex, vex_table_index;

  if (dp->name != NULL)
    return dp;

  switch (dp->op[0].bytemode)
    {
    case USE_REG_TABLE:
      dp = &reg_table[dp->op[1].bytemode][modrm.reg];
      break;

    case USE_MOD_TABLE:
      vindex = modrm.mod == 0x3 ? 1 : 0;
      dp = &mod_table[dp->op[1].bytemode][vindex];
      break;

    case USE_RM_TABLE:
      dp = &rm_table[dp->op[1].bytemode][modrm.rm];
      break;

    case USE_PREFIX_TABLE:
      if (need_vex)
	{
	  /* The prefix in VEX is implicit.  */
	  switch (vex.prefix)
	    {
	    case 0:
	      vindex = 0;
	      break;
	    case REPE_PREFIX_OPCODE:
	      vindex = 1;
	      break;
	    case DATA_PREFIX_OPCODE:
	      vindex = 2;
	      break;
	    case REPNE_PREFIX_OPCODE:
	      vindex = 3;
	      break;
	    default:
	      cerr << "vex.prefix is invalid : " << vex.prefix << endl;
	      abort ();
	      break;
	    }
	}
      else 
	{
	  vindex = 0;
	  used_prefixes |= (prefixes & PREFIX_REPZ);
	  if (prefixes & PREFIX_REPZ)
	    {
	      vindex = 1;
	      all_prefixes[last_repz_prefix] = 0;
	    }
	  else
	    {
	      /* We should check PREFIX_REPNZ and PREFIX_REPZ before
		 PREFIX_DATA.  */
	      used_prefixes |= (prefixes & PREFIX_REPNZ);
	      if (prefixes & PREFIX_REPNZ)
		{
		  vindex = 3;
		  all_prefixes[last_repnz_prefix] = 0;
		}
	      else
		{
		  used_prefixes |= (prefixes & PREFIX_DATA);
		  if (prefixes & PREFIX_DATA)
		    {
		      vindex = 2;
		      all_prefixes[last_data_prefix] = 0;
		    }
		}
	    }
	}
      dp = &prefix_table[dp->op[1].bytemode][vindex];
      break;

    case USE_X86_64_TABLE:
      vindex = address_mode == mode_64bit ? 1 : 0;
      dp = &x86_64_table[dp->op[1].bytemode][vindex];
      break;

    case USE_3BYTE_TABLE:
      FETCH_DATA (info, codep + 2);
      vindex = *codep++;
      dp = &three_byte_table[dp->op[1].bytemode][vindex];
      modrm.mod = (*codep >> 6) & 3;
      modrm.reg = (*codep >> 3) & 7;
      modrm.rm = *codep & 7;
      break;

    case USE_VEX_LEN_TABLE:
      if (!need_vex)
	{
	  cerr << "!need_vex" << endl;
	  abort ();
	}

      switch (vex.length)
	{
	case 128:
	  vindex = 0;
	  break;
	case 256:
	  vindex = 1;
	  break;
	default:
	  cerr << "vex.length wrong :" << vex.length << endl;
	  abort ();
	  break;
	}

      dp = &vex_len_table[dp->op[1].bytemode][vindex];
      break;

    case USE_XOP_8F_TABLE:
      FETCH_DATA (info, codep + 3);
      /* All bits in the REX prefix are ignored.  */
      rex_ignored = rex;
      rex = ~(*codep >> 5) & 0x7;

      /* VEX_TABLE_INDEX is the mmmmm part of the XOP byte 1 "RCB.mmmmm".  */
      switch ((*codep & 0x1f))
	{
	default:
	  dp = &bad_opcode;
	  return dp;
	case 0x8:
	  vex_table_index = XOP_08;
	  break;
	case 0x9:
	  vex_table_index = XOP_09;
	  break;
	case 0xa:
	  vex_table_index = XOP_0A;
	  break;
	}
      codep++;
      vex.w = *codep & 0x80;
      if (vex.w && address_mode == mode_64bit)
	rex |= REX_W;

      vex.register_specifier = (~(*codep >> 3)) & 0xf;
      if (address_mode != mode_64bit
	  && vex.register_specifier > 0x7)
	{
	  dp = &bad_opcode;
	  return dp;
	}

      vex.length = (*codep & 0x4) ? 256 : 128;
      switch ((*codep & 0x3))
	{
	case 0:
	  vex.prefix = 0;
	  break;
	case 1:
	  vex.prefix = DATA_PREFIX_OPCODE;
	  break;
	case 2:
	  vex.prefix = REPE_PREFIX_OPCODE;
	  break;
	case 3:
	  vex.prefix = REPNE_PREFIX_OPCODE;
	  break;
	}
      need_vex = 1;
      need_vex_reg = 1;
      codep++;
      vindex = *codep++;
      dp = &xop_table[vex_table_index][vindex];

      FETCH_DATA (info, codep + 1);
      modrm.mod = (*codep >> 6) & 3;
      modrm.reg = (*codep >> 3) & 7;
      modrm.rm = *codep & 7;
      break;

    case USE_VEX_C4_TABLE:
      FETCH_DATA (info, codep + 3);
      /* All bits in the REX prefix are ignored.  */
      rex_ignored = rex;
      rex = ~(*codep >> 5) & 0x7;
      switch ((*codep & 0x1f))
	{
	default:
	  dp = &bad_opcode;
	  return dp;
	case 0x1:
	  vex_table_index = VEX_0F;
	  break;
	case 0x2:
	  vex_table_index = VEX_0F38;
	  break;
	case 0x3:
	  vex_table_index = VEX_0F3A;
	  break;
	}
      codep++;
      vex.w = *codep & 0x80;
      if (vex.w && address_mode == mode_64bit)
	rex |= REX_W;

      vex.register_specifier = (~(*codep >> 3)) & 0xf;
      if (address_mode != mode_64bit
	  && vex.register_specifier > 0x7)
	{
	  dp = &bad_opcode;
	  return dp;
	}

      vex.length = (*codep & 0x4) ? 256 : 128;
      switch ((*codep & 0x3))
	{
	case 0:
	  vex.prefix = 0;
	  break;
	case 1:
	  vex.prefix = DATA_PREFIX_OPCODE;
	  break;
	case 2:
	  vex.prefix = REPE_PREFIX_OPCODE;
	  break;
	case 3:
	  vex.prefix = REPNE_PREFIX_OPCODE;
	  break;
	}
      need_vex = 1;
      need_vex_reg = 1;
      codep++;
      vindex = *codep++;
      dp = &vex_table[vex_table_index][vindex];
      /* There is no MODRM byte for VEX [82|77].  */
      if (vindex != 0x77 && vindex != 0x82)
	{
	  FETCH_DATA (info, codep + 1);
	  modrm.mod = (*codep >> 6) & 3;
	  modrm.reg = (*codep >> 3) & 7;
	  modrm.rm = *codep & 7;
	}
      break;

    case USE_VEX_C5_TABLE:
      FETCH_DATA (info, codep + 2);
      /* All bits in the REX prefix are ignored.  */
      rex_ignored = rex;
      rex = (*codep & 0x80) ? 0 : REX_R;

      vex.register_specifier = (~(*codep >> 3)) & 0xf;
      if (address_mode != mode_64bit
	  && vex.register_specifier > 0x7)
	{
	  dp = &bad_opcode;
	  return dp;
	}

      vex.w = 0;

      vex.length = (*codep & 0x4) ? 256 : 128;
      switch ((*codep & 0x3))
	{
	case 0:
	  vex.prefix = 0;
	  break;
	case 1:
	  vex.prefix = DATA_PREFIX_OPCODE;
	  break;
	case 2:
	  vex.prefix = REPE_PREFIX_OPCODE;
	  break;
	case 3:
	  vex.prefix = REPNE_PREFIX_OPCODE;
	  break;
	}
      need_vex = 1;
      need_vex_reg = 1;
      codep++;
      vindex = *codep++;
      dp = &vex_table[dp->op[1].bytemode][vindex];
      /* There is no MODRM byte for VEX [82|77].  */
      if (vindex != 0x77 && vindex != 0x82)
	{
	  FETCH_DATA (info, codep + 1);
	  modrm.mod = (*codep >> 6) & 3;
	  modrm.reg = (*codep >> 3) & 7;
	  modrm.rm = *codep & 7;
	}
      break;

    case USE_VEX_W_TABLE:
      if (!need_vex)
	{
	  cerr << "! need_vex"<< endl;
	  abort ();
	}

      dp = &vex_w_table[dp->op[1].bytemode][vex.w ? 1 : 0];
      break;

    case 0:
      cerr << "Bad opcode :" << dp->op[0].bytemode << endl;
      dp = &bad_opcode;
      break;

    default:
      cerr << "Bad bytemode :" << dp->op[0].bytemode << endl;
      abort ();
    }

  if (dp->name != NULL)
    return dp;
  else
    return get_valid_dis386 (dp, info);
}

void
StaticData::get_sib (disassemble_info *info)
{
  /* If modrm.mod == 3, operand must be register.  */
  if (need_modrm
      && address_mode != mode_16bit
      && modrm.mod != 3
      && modrm.rm == 4)
    {
      FETCH_DATA (info, codep + 2);
      sib.index = (codep [1] >> 3) & 7;
      sib.scale = (codep [1] >> 6) & 3;
      sib.base = codep [1] & 7;
    }
}




void
StaticData::swap_operand (void)
{
  mnemonicendp[0] = '.';
  mnemonicendp[1] = 's';
  mnemonicendp += 2;
}

void
StaticData::OP_Skip_MODRM (int bytemode ATTRIBUTE_UNUSED,
	       int sizeflag ATTRIBUTE_UNUSED)
{
  /* Skip mod/rm byte.  */
  MODRM_CHECK;
  codep++;
}

void
StaticData::dofloat (int sizeflag)
{
  const struct dis386 *dp;
  unsigned char floatop;

  floatop = codep[-1];

  if (modrm.mod != 3)
    {
      int fp_indx = (floatop - 0xd8) * 8 + modrm.reg;

      putop (float_mem[fp_indx], sizeflag);
      obufp = op_out[0];
      op_ad = 2;
      OP_E (float_mem_mode[fp_indx], sizeflag);
      return;
    }
  /* Skip mod/rm byte.  */
  MODRM_CHECK;
  codep++;

  dp = &float_reg[floatop - 0xd8][modrm.reg];
  if (dp->name == NULL)
    {
      putop (fgrps[dp->op[0].bytemode][modrm.rm], sizeflag);

      /* Instruction fnstsw is only one with strange arg.  */
      if (floatop == 0xdf && codep[-1] == 0xe0)
	strcpy (op_out[0], names16[0]);
    }
  else
    {
      putop (dp->name, sizeflag);

      obufp = op_out[0];
      op_ad = 2;
      if (dp->op[0].rtn)
	(*dp->op[0].rtn) (dp->op[0].bytemode, sizeflag);

      obufp = op_out[1];
      op_ad = 1;
      if (dp->op[1].rtn)
	(*dp->op[1].rtn) (dp->op[1].bytemode, sizeflag);
    }
}

 void
StaticData::OP_ST (int bytemode ATTRIBUTE_UNUSED, int sizeflag ATTRIBUTE_UNUSED)
{
  oappend ("%st" + intel_syntax);
}

void
StaticData::OP_STi (int bytemode ATTRIBUTE_UNUSED, int sizeflag ATTRIBUTE_UNUSED)
{
  sprintf (scratchbuf, "%%st(%d)", modrm.rm);
  oappend (scratchbuf + intel_syntax);
}


void
StaticData::oappend (const char *s)
{
  obufp = stpcpy (obufp, s);
}

void
StaticData::append_seg (void)
{
  if (prefixes & PREFIX_CS)
    {
      used_prefixes |= PREFIX_CS;
      oappend ("%cs:" + intel_syntax);
    }
  if (prefixes & PREFIX_DS)
    {
      used_prefixes |= PREFIX_DS;
      oappend ("%ds:" + intel_syntax);
    }
  if (prefixes & PREFIX_SS)
    {
      used_prefixes |= PREFIX_SS;
      oappend ("%ss:" + intel_syntax);
    }
  if (prefixes & PREFIX_ES)
    {
      used_prefixes |= PREFIX_ES;
      oappend ("%es:" + intel_syntax);
    }
  if (prefixes & PREFIX_FS)
    {
      used_prefixes |= PREFIX_FS;
      oappend ("%fs:" + intel_syntax);
    }
  if (prefixes & PREFIX_GS)
    {
      used_prefixes |= PREFIX_GS;
      oappend ("%gs:" + intel_syntax);
    }
}

void
StaticData::OP_indirE (int bytemode, int sizeflag)
{
  if (!intel_syntax)
    oappend ("*");
  OP_E (bytemode, sizeflag);
}

void
StaticData::print_operand_value (char *buf, int hex, bfd_vma disp)
{
  if (address_mode == mode_64bit)
    {
      if (hex)
	{
	  char tmp[30];
	  int i;
	  buf[0] = '0';
	  buf[1] = 'x';
	  sprintf_vma (tmp, disp);
	  for (i = 0; tmp[i] == '0' && tmp[i + 1]; i++);
	  strcpy (buf + 2, tmp + i);
	}
      else
	{
	  bfd_signed_vma v = disp;
	  char tmp[30];
	  int i;
	  if (v < 0)
	    {
	      *(buf++) = '-';
	      v = -disp;
	      /* Check for possible overflow on 0x8000000000000000.  */
	      if (v < 0)
		{
		  strcpy (buf, "9223372036854775808");
		  return;
		}
	    }
	  if (!v)
	    {
	      strcpy (buf, "0");
	      return;
	    }

	  i = 0;
	  tmp[29] = 0;
	  while (v)
	    {
	      tmp[28 - i] = (v % 10) + '0';
	      v /= 10;
	      i++;
	    }
	  strcpy (buf, tmp + 29 - i);
	}
    }
  else
    {
      if (hex)
	sprintf (buf, "0x%x", (unsigned int) disp);
      else
	sprintf (buf, "%d", (int) disp);
    }
}

/* Put DISP in BUF as signed hex number.  */

void
StaticData::print_displacement (char *buf, bfd_vma disp)
{
  bfd_signed_vma val = disp;
  char tmp[30];
  int i, j = 0;

  if (val < 0)
    {
      buf[j++] = '-';
      val = -disp;

      /* Check for possible overflow.  */
      if (val < 0)
	{
	  switch (address_mode)
	    {
	    case mode_64bit:
	      strcpy (buf + j, "0x8000000000000000");
	      break;
	    case mode_32bit:
	      strcpy (buf + j, "0x80000000");
	      break;
	    case mode_16bit:
	      strcpy (buf + j, "0x8000");
	      break;
	    }
	  return;
	}
    }

  buf[j++] = '0';
  buf[j++] = 'x';

  sprintf_vma (tmp, (bfd_vma) val);
  for (i = 0; tmp[i] == '0'; i++)
    continue;
  if (tmp[i] == '\0')
    i--;
  strcpy (buf + j, tmp + i);
}

void
StaticData::intel_operand_size (int bytemode, int sizeflag)
{
  switch (bytemode)
    {
    case b_mode:
    case b_swap_mode:
    case dqb_mode:
      oappend ("BYTE PTR ");
      break;
    case w_mode:
    case dqw_mode:
      oappend ("WORD PTR ");
      break;
    case stack_v_mode:
      if (address_mode == mode_64bit && (sizeflag & DFLAG))
	{
	  oappend ("QWORD PTR ");
	  break;
	}
      /* FALLTHRU */
    case v_mode:
    case v_swap_mode:
    case dq_mode:
      USED_REX (REX_W);
      if (rex & REX_W)
	oappend ("QWORD PTR ");
      else
	{
	  if ((sizeflag & DFLAG) || bytemode == dq_mode)
	    oappend ("DWORD PTR ");
	  else
	    oappend ("WORD PTR ");
	  used_prefixes |= (prefixes & PREFIX_DATA);
	}
      break;
    case z_mode:
      if ((rex & REX_W) || (sizeflag & DFLAG))
	*obufp++ = 'D';
      oappend ("WORD PTR ");
      if (!(rex & REX_W))
	used_prefixes |= (prefixes & PREFIX_DATA);
      break;
    case a_mode:
      if (sizeflag & DFLAG)
	oappend ("QWORD PTR ");
      else
	oappend ("DWORD PTR ");
      used_prefixes |= (prefixes & PREFIX_DATA);
      break;
    case d_mode:
    case d_scalar_mode:
    case d_scalar_swap_mode:
    case d_swap_mode:
    case dqd_mode:
      oappend ("DWORD PTR ");
      break;
    case q_mode:
    case q_scalar_mode:
    case q_scalar_swap_mode:
    case q_swap_mode:
      oappend ("QWORD PTR ");
      break;
    case m_mode:
      if (address_mode == mode_64bit)
	oappend ("QWORD PTR ");
      else
	oappend ("DWORD PTR ");
      break;
    case f_mode:
      if (sizeflag & DFLAG)
	oappend ("FWORD PTR ");
      else
	oappend ("DWORD PTR ");
      used_prefixes |= (prefixes & PREFIX_DATA);
      break;
    case t_mode:
      oappend ("TBYTE PTR ");
      break;
    case x_mode:
    case x_swap_mode:
      if (need_vex)
	{
	  switch (vex.length)
	    {
	    case 128:
	      oappend ("XMMWORD PTR ");
	      break;
	    case 256:
	      oappend ("YMMWORD PTR ");
	      break;
	    default:
	  cerr << "vex.length wrong :" << vex.length << endl;
	      abort ();
	    }
	}
      else
	oappend ("XMMWORD PTR ");
      break;
    case xmm_mode:
      oappend ("XMMWORD PTR ");
      break;
    case xmmq_mode:
      if (!need_vex)
	{
	  cerr << "!need_vex" << endl;
	  abort ();
	}

      switch (vex.length)
	{
	case 128:
	  oappend ("QWORD PTR ");
	  break;
	case 256:
	  oappend ("XMMWORD PTR ");
	  break;
	default:
	  cerr << "vex.length wrong :" << vex.length << endl;
	  abort ();
	}
      break;
    case ymmq_mode:
      if (!need_vex)
	{
	  cerr << "!need_vex" << endl;
	  abort ();
	}

      switch (vex.length)
	{
	case 128:
	  oappend ("QWORD PTR ");
	  break;
	case 256:
	  oappend ("YMMWORD PTR ");
	  break;
	default:
	  cerr << "vex.length wrong :" << vex.length << endl;
	  abort ();
	}
      break;
    case o_mode:
      oappend ("OWORD PTR ");
      break;
    case vex_w_dq_mode:
    case vex_scalar_w_dq_mode:
      if (!need_vex)
	{
	  cerr << "!need_vex" << endl;
	  abort ();
	}

      if (vex.w)
	oappend ("QWORD PTR ");
      else
	oappend ("DWORD PTR ");
      break;
    default:
      break;
    }
}

void
StaticData::OP_E_register (int bytemode, int sizeflag)
{
  int reg = modrm.rm;
  const char **names;

  USED_REX (REX_B);
  if ((rex & REX_B))
    reg += 8;

  if ((sizeflag & SUFFIX_ALWAYS)
      && (bytemode == b_swap_mode || bytemode == v_swap_mode))
    swap_operand ();

  switch (bytemode)
    {
    case b_mode:
    case b_swap_mode:
      USED_REX (0);
      if (rex)
	names = names8rex;
      else
	names = names8;
      break;
    case w_mode:
      names = names16;
      break;
    case d_mode:
      names = names32;
      break;
    case q_mode:
      names = names64;
      break;
    case m_mode:
      names = address_mode == mode_64bit ? names64 : names32;
      break;
    case stack_v_mode:
      if (address_mode == mode_64bit && (sizeflag & DFLAG))
	{
	  names = names64;
	  break;
	}
      bytemode = v_mode;
      /* FALLTHRU */
    case v_mode:
    case v_swap_mode:
    case dq_mode:
    case dqb_mode:
    case dqd_mode:
    case dqw_mode:
      USED_REX (REX_W);
      if (rex & REX_W)
	names = names64;
      else
	{
	  if ((sizeflag & DFLAG) 
	      || (bytemode != v_mode
		  && bytemode != v_swap_mode))
	    names = names32;
	  else
	    names = names16;
	  used_prefixes |= (prefixes & PREFIX_DATA);
	}
      break;
    case 0:
      return;
    default:
      oappend (INTERNAL_DISASSEMBLER_ERROR);
      return;
    }
  oappend (names[reg]);
}

void
StaticData::OP_E_memory (int bytemode, int sizeflag)
{
  bfd_vma disp = 0;
  int add = (rex & REX_B) ? 8 : 0;
  int riprel = 0;

  USED_REX (REX_B);
  if (intel_syntax)
    intel_operand_size (bytemode, sizeflag);
  append_seg ();

  if ((sizeflag & AFLAG) || address_mode == mode_64bit)
    {
      /* 32/64 bit address mode */
      int havedisp;
      int havesib;
      int havebase;
      int haveindex;
      int needindex;
      int base, rbase;
      int vindex = 0;
      int scale = 0;

      havesib = 0;
      havebase = 1;
      haveindex = 0;
      base = modrm.rm;

      if (base == 4)
	{
	  havesib = 1;
	  vindex = sib.index;
	  scale = sib.scale;
	  base = sib.base;
	  USED_REX (REX_X);
	  if (rex & REX_X)
	    vindex += 8;
	  haveindex = vindex != 4;
	  codep++;
	}
      rbase = base + add;

      switch (modrm.mod)
	{
	case 0:
	  if (base == 5)
	    {
	      havebase = 0;
	      if (address_mode == mode_64bit && !havesib)
		riprel = 1;
	      disp = get32s ();
	    }
	  break;
	case 1:
	  FETCH_DATA (the_info, codep + 1);
	  disp = *codep++;
	  if ((disp & 0x80) != 0)
	    disp -= 0x100;
	  break;
	case 2:
	  disp = get32s ();
	  break;
	}

      /* In 32bit mode, we need index register to tell [offset] from
	 [eiz*1 + offset].  */
      needindex = (havesib
		   && !havebase
		   && !haveindex
		   && address_mode == mode_32bit);
      havedisp = (havebase
		  || needindex
		  || (havesib && (haveindex || scale != 0)));

      if (!intel_syntax)
	if (modrm.mod != 0 || base == 5)
	  {
	    if (havedisp || riprel)
	      print_displacement (scratchbuf, disp);
	    else
	      print_operand_value (scratchbuf, 1, disp);
	    oappend (scratchbuf);
	    if (riprel)
	      {
		set_op (disp, 1);
		oappend (sizeflag & AFLAG ? "(%rip)" : "(%eip)");
	      }
	  }

      if (havebase || haveindex || riprel)
	used_prefixes |= PREFIX_ADDR;

      if (havedisp || (intel_syntax && riprel))
	{
	  *obufp++ = open_char;
	  if (intel_syntax && riprel)
	    {
	      set_op (disp, 1);
	      oappend (sizeflag & AFLAG ? "rip" : "eip");
	    }
	  *obufp = '\0';
	  if (havebase)
	    oappend (address_mode == mode_64bit && (sizeflag & AFLAG)
		     ? names64[rbase] : names32[rbase]);
	  if (havesib)
	    {
	      /* ESP/RSP won't allow index.  If base isn't ESP/RSP,
		 print index to tell base + index from base.  */
	      if (scale != 0
		  || needindex
		  || haveindex
		  || (havebase && base != ESP_REG_NUM))
		{
		  if (!intel_syntax || havebase)
		    {
		      *obufp++ = separator_char;
		      *obufp = '\0';
		    }
		  if (haveindex)
		    oappend (address_mode == mode_64bit 
			     && (sizeflag & AFLAG)
			     ? names64[vindex] : names32[vindex]);
		  else
		    oappend (address_mode == mode_64bit 
			     && (sizeflag & AFLAG)
			     ? index64 : index32);

		  *obufp++ = scale_char;
		  *obufp = '\0';
		  sprintf (scratchbuf, "%d", 1 << scale);
		  oappend (scratchbuf);
		}
	    }
	  if (intel_syntax
	      && (disp || modrm.mod != 0 || base == 5))
	    {
	      if (!havedisp || (bfd_signed_vma) disp >= 0)
		{
		  *obufp++ = '+';
		  *obufp = '\0';
		}
	      else if (modrm.mod != 1 && disp != -disp)
		{
		  *obufp++ = '-';
		  *obufp = '\0';
		  disp = - (bfd_signed_vma) disp;
		}

	      if (havedisp)
		print_displacement (scratchbuf, disp);
	      else
		print_operand_value (scratchbuf, 1, disp);
	      oappend (scratchbuf);
	    }

	  *obufp++ = close_char;
	  *obufp = '\0';
	}
      else if (intel_syntax)
	{
	  if (modrm.mod != 0 || base == 5)
	    {
	      if (prefixes & (PREFIX_CS | PREFIX_SS | PREFIX_DS
			      | PREFIX_ES | PREFIX_FS | PREFIX_GS))
		;
	      else
		{
		  oappend (names_seg[ds_reg - es_reg]);
		  oappend (":");
		}
	      print_operand_value (scratchbuf, 1, disp);
	      oappend (scratchbuf);
	    }
	}
    }
  else
    {
      /* 16 bit address mode */
      used_prefixes |= prefixes & PREFIX_ADDR;
      switch (modrm.mod)
	{
	case 0:
	  if (modrm.rm == 6)
	    {
	      disp = get16 ();
	      if ((disp & 0x8000) != 0)
		disp -= 0x10000;
	    }
	  break;
	case 1:
	  FETCH_DATA (the_info, codep + 1);
	  disp = *codep++;
	  if ((disp & 0x80) != 0)
	    disp -= 0x100;
	  break;
	case 2:
	  disp = get16 ();
	  if ((disp & 0x8000) != 0)
	    disp -= 0x10000;
	  break;
	}

      if (!intel_syntax)
	if (modrm.mod != 0 || modrm.rm == 6)
	  {
	    print_displacement (scratchbuf, disp);
	    oappend (scratchbuf);
	  }

      if (modrm.mod != 0 || modrm.rm != 6)
	{
	  *obufp++ = open_char;
	  *obufp = '\0';
	  oappend (index16[modrm.rm]);
	  if (intel_syntax
	      && (disp || modrm.mod != 0 || modrm.rm == 6))
	    {
	      if ((bfd_signed_vma) disp >= 0)
		{
		  *obufp++ = '+';
		  *obufp = '\0';
		}
	      else if (modrm.mod != 1)
		{
		  *obufp++ = '-';
		  *obufp = '\0';
		  disp = - (bfd_signed_vma) disp;
		}

	      print_displacement (scratchbuf, disp);
	      oappend (scratchbuf);
	    }

	  *obufp++ = close_char;
	  *obufp = '\0';
	}
      else if (intel_syntax)
	{
	  if (prefixes & (PREFIX_CS | PREFIX_SS | PREFIX_DS
			  | PREFIX_ES | PREFIX_FS | PREFIX_GS))
	    ;
	  else
	    {
	      oappend (names_seg[ds_reg - es_reg]);
	      oappend (":");
	    }
	  print_operand_value (scratchbuf, 1, disp & 0xffff);
	  oappend (scratchbuf);
	}
    }
}

void
StaticData::OP_E (int bytemode, int sizeflag)
{
  /* Skip mod/rm byte.  */
  MODRM_CHECK;
  codep++;

  if (modrm.mod == 3)
    OP_E_register (bytemode, sizeflag);
  else
    OP_E_memory (bytemode, sizeflag);
}

void
StaticData::OP_G (int bytemode, int sizeflag)
{
  int add = 0;
  USED_REX (REX_R);
  if (rex & REX_R)
    add += 8;
  switch (bytemode)
    {
    case b_mode:
      USED_REX (0);
      if (rex)
	oappend (names8rex[modrm.reg + add]);
      else
	oappend (names8[modrm.reg + add]);
      break;
    case w_mode:
      oappend (names16[modrm.reg + add]);
      break;
    case d_mode:
      oappend (names32[modrm.reg + add]);
      break;
    case q_mode:
      oappend (names64[modrm.reg + add]);
      break;
    case v_mode:
    case dq_mode:
    case dqb_mode:
    case dqd_mode:
    case dqw_mode:
      USED_REX (REX_W);
      if (rex & REX_W)
	oappend (names64[modrm.reg + add]);
      else
	{
	  if ((sizeflag & DFLAG) || bytemode != v_mode)
	    oappend (names32[modrm.reg + add]);
	  else
	    oappend (names16[modrm.reg + add]);
	  used_prefixes |= (prefixes & PREFIX_DATA);
	}
      break;
    case m_mode:
      if (address_mode == mode_64bit)
	oappend (names64[modrm.reg + add]);
      else
	oappend (names32[modrm.reg + add]);
      break;
    default:
      oappend (INTERNAL_DISASSEMBLER_ERROR);
      break;
    }
}

bfd_vma
StaticData::get64 (void)
{
  bfd_vma x;
#ifdef BFD64
  unsigned int a;
  unsigned int b;

  FETCH_DATA (the_info, codep + 8);
  a = *codep++ & 0xff;
  a |= (*codep++ & 0xff) << 8;
  a |= (*codep++ & 0xff) << 16;
  a |= (*codep++ & 0xff) << 24;
  b = *codep++ & 0xff;
  b |= (*codep++ & 0xff) << 8;
  b |= (*codep++ & 0xff) << 16;
  b |= (*codep++ & 0xff) << 24;
  x = a + ((bfd_vma) b << 32);
#else
  cerr << "BFD64" << endl;
  abort ();
  x = 0;
#endif
  return x;
}

bfd_signed_vma
StaticData::get32 (void)
{
  bfd_signed_vma x = 0;

  FETCH_DATA (the_info, codep + 4);
  x = *codep++ & (bfd_signed_vma) 0xff;
  x |= (*codep++ & (bfd_signed_vma) 0xff) << 8;
  x |= (*codep++ & (bfd_signed_vma) 0xff) << 16;
  x |= (*codep++ & (bfd_signed_vma) 0xff) << 24;
  return x;
}

bfd_signed_vma
StaticData::get32s (void)
{
  bfd_signed_vma x = 0;

  FETCH_DATA (the_info, codep + 4);
  x = *codep++ & (bfd_signed_vma) 0xff;
  x |= (*codep++ & (bfd_signed_vma) 0xff) << 8;
  x |= (*codep++ & (bfd_signed_vma) 0xff) << 16;
  x |= (*codep++ & (bfd_signed_vma) 0xff) << 24;

  x = (x ^ ((bfd_signed_vma) 1 << 31)) - ((bfd_signed_vma) 1 << 31);

  return x;
}

int
StaticData::get16 (void)
{
  int x = 0;

  FETCH_DATA (the_info, codep + 2);
  x = *codep++ & 0xff;
  x |= (*codep++ & 0xff) << 8;
  return x;
}

 void
StaticData::set_op (bfd_vma op, int riprel)
{
  op_index[op_ad] = op_ad;
  if (address_mode == mode_64bit)
    {
      op_address[op_ad] = op;
      op_riprel[op_ad] = riprel;
    }
  else
    {
      /* Mask to get a 32-bit address.  */
      op_address[op_ad] = op & 0xffffffff;
      op_riprel[op_ad] = riprel & 0xffffffff;
    }
}

 void
StaticData::OP_REG (int code, int sizeflag)
{
  const char *s;
  int add;
  USED_REX (REX_B);
  if (rex & REX_B)
    add = 8;
  else
    add = 0;

  switch (code)
    {
    case ax_reg: case cx_reg: case dx_reg: case bx_reg:
    case sp_reg: case bp_reg: case si_reg: case di_reg:
      s = names16[code - ax_reg + add];
      break;
    case es_reg: case ss_reg: case cs_reg:
    case ds_reg: case fs_reg: case gs_reg:
      s = names_seg[code - es_reg + add];
      break;
    case al_reg: case ah_reg: case cl_reg: case ch_reg:
    case dl_reg: case dh_reg: case bl_reg: case bh_reg:
      USED_REX (0);
      if (rex)
	s = names8rex[code - al_reg + add];
      else
	s = names8[code - al_reg];
      break;
    case rAX_reg: case rCX_reg: case rDX_reg: case rBX_reg:
    case rSP_reg: case rBP_reg: case rSI_reg: case rDI_reg:
      if (address_mode == mode_64bit && (sizeflag & DFLAG))
	{
	  s = names64[code - rAX_reg + add];
	  break;
	}
      code += eAX_reg - rAX_reg;
      /* Fall through.  */
    case eAX_reg: case eCX_reg: case eDX_reg: case eBX_reg:
    case eSP_reg: case eBP_reg: case eSI_reg: case eDI_reg:
      USED_REX (REX_W);
      if (rex & REX_W)
	s = names64[code - eAX_reg + add];
      else
	{
	  if (sizeflag & DFLAG)
	    s = names32[code - eAX_reg + add];
	  else
	    s = names16[code - eAX_reg + add];
	  used_prefixes |= (prefixes & PREFIX_DATA);
	}
      break;
    default:
      s = INTERNAL_DISASSEMBLER_ERROR;
      break;
    }
  oappend (s);
}

 void
StaticData::OP_IMREG (int code, int sizeflag)
{
  const char *s;

  switch (code)
    {
    case indir_dx_reg:
      if (intel_syntax)
	s = "dx";
      else
	s = "(%dx)";
      break;
    case ax_reg: case cx_reg: case dx_reg: case bx_reg:
    case sp_reg: case bp_reg: case si_reg: case di_reg:
      s = names16[code - ax_reg];
      break;
    case es_reg: case ss_reg: case cs_reg:
    case ds_reg: case fs_reg: case gs_reg:
      s = names_seg[code - es_reg];
      break;
    case al_reg: case ah_reg: case cl_reg: case ch_reg:
    case dl_reg: case dh_reg: case bl_reg: case bh_reg:
      USED_REX (0);
      if (rex)
	s = names8rex[code - al_reg];
      else
	s = names8[code - al_reg];
      break;
    case eAX_reg: case eCX_reg: case eDX_reg: case eBX_reg:
    case eSP_reg: case eBP_reg: case eSI_reg: case eDI_reg:
      USED_REX (REX_W);
      if (rex & REX_W)
	s = names64[code - eAX_reg];
      else
	{
	  if (sizeflag & DFLAG)
	    s = names32[code - eAX_reg];
	  else
	    s = names16[code - eAX_reg];
	  used_prefixes |= (prefixes & PREFIX_DATA);
	}
      break;
    case z_mode_ax_reg:
      if ((rex & REX_W) || (sizeflag & DFLAG))
	s = *names32;
      else
	s = *names16;
      if (!(rex & REX_W))
	used_prefixes |= (prefixes & PREFIX_DATA);
      break;
    default:
      s = INTERNAL_DISASSEMBLER_ERROR;
      break;
    }
  oappend (s);
}

 void
StaticData::OP_I (int bytemode, int sizeflag)
{
  bfd_signed_vma op;
  bfd_signed_vma mask = -1;

  switch (bytemode)
    {
    case b_mode:
      FETCH_DATA (the_info, codep + 1);
      op = *codep++;
      mask = 0xff;
      break;
    case q_mode:
      if (address_mode == mode_64bit)
	{
	  op = get32s ();
	  break;
	}
      /* Fall through.  */
    case v_mode:
      USED_REX (REX_W);
      if (rex & REX_W)
	op = get32s ();
      else
	{
	  if (sizeflag & DFLAG)
	    {
	      op = get32 ();
	      mask = 0xffffffff;
	    }
	  else
	    {
	      op = get16 ();
	      mask = 0xfffff;
	    }
	  used_prefixes |= (prefixes & PREFIX_DATA);
	}
      break;
    case w_mode:
      mask = 0xfffff;
      op = get16 ();
      break;
    case const_1_mode:
      if (intel_syntax)
        oappend ("1");
      return;
    default:
      oappend (INTERNAL_DISASSEMBLER_ERROR);
      return;
    }

  op &= mask;
  scratchbuf[0] = '$';
  print_operand_value (scratchbuf + 1, 1, op);
  oappend (scratchbuf + intel_syntax);
  scratchbuf[0] = '\0';
}

 void
StaticData::OP_I64 (int bytemode, int sizeflag)
{
  bfd_signed_vma op;
  bfd_signed_vma mask = -1;

  if (address_mode != mode_64bit)
    {
      OP_I (bytemode, sizeflag);
      return;
    }

  switch (bytemode)
    {
    case b_mode:
      FETCH_DATA (the_info, codep + 1);
      op = *codep++;
      mask = 0xff;
      break;
    case v_mode:
      USED_REX (REX_W);
      if (rex & REX_W)
	op = get64 ();
      else
	{
	  if (sizeflag & DFLAG)
	    {
	      op = get32 ();
	      mask = 0xffffffff;
	    }
	  else
	    {
	      op = get16 ();
	      mask = 0xfffff;
	    }
	  used_prefixes |= (prefixes & PREFIX_DATA);
	}
      break;
    case w_mode:
      mask = 0xfffff;
      op = get16 ();
      break;
    default:
      oappend (INTERNAL_DISASSEMBLER_ERROR);
      return;
    }

  op &= mask;
  scratchbuf[0] = '$';
  print_operand_value (scratchbuf + 1, 1, op);
  oappend (scratchbuf + intel_syntax);
  scratchbuf[0] = '\0';
}

 void
StaticData::OP_sI (int bytemode, int sizeflag)
{
  bfd_signed_vma op;

  switch (bytemode)
    {
    case b_mode:
      FETCH_DATA (the_info, codep + 1);
      op = *codep++;
      if ((op & 0x80) != 0)
	op -= 0x100;
      break;
    case v_mode:
      if (sizeflag & DFLAG)
	op = get32s ();
      else
	op = get16 ();
      break;
    default:
      oappend (INTERNAL_DISASSEMBLER_ERROR);
      return;
    }

  scratchbuf[0] = '$';
  print_operand_value (scratchbuf + 1, 1, op);
  oappend (scratchbuf + intel_syntax);
}

 void
StaticData::OP_J (int bytemode, int sizeflag)
{
  bfd_vma disp;
  bfd_vma mask = -1;
  bfd_vma segment = 0;

  switch (bytemode)
    {
    case b_mode:
      FETCH_DATA (the_info, codep + 1);
      disp = *codep++;
      if ((disp & 0x80) != 0)
	disp -= 0x100;
      break;
    case v_mode:
      USED_REX (REX_W);
      if ((sizeflag & DFLAG) || (rex & REX_W))
	disp = get32s ();
      else
	{
	  disp = get16 ();
	  if ((disp & 0x8000) != 0)
	    disp -= 0x10000;
	  /* In 16bit mode, address is wrapped around at 64k within
	     the same segment.  Otherwise, a data16 prefix on a jump
	     instruction means that the pc is masked to 16 bits after
	     the displacement is added!  */
	  mask = 0xffff;
	  if ((prefixes & PREFIX_DATA) == 0)
	    segment = ((start_pc + codep - start_codep)
		       & ~((bfd_vma) 0xffff));
	}
      if (!(rex & REX_W))
	used_prefixes |= (prefixes & PREFIX_DATA);
      break;
    default:
      oappend (INTERNAL_DISASSEMBLER_ERROR);
      return;
    }
  disp = ((start_pc + codep - start_codep + disp) & mask) | segment;
  set_op (disp, 0);
  print_operand_value (scratchbuf, 1, disp);
  oappend (scratchbuf);
}

 void
StaticData::OP_SEG (int bytemode, int sizeflag)
{
  if (bytemode == w_mode)
    oappend (names_seg[modrm.reg]);
  else
    OP_E (modrm.mod == 3 ? bytemode : w_mode, sizeflag);
}

 void
StaticData::OP_DIR (int dummy ATTRIBUTE_UNUSED, int sizeflag)
{
  int seg, offset;

  if (sizeflag & DFLAG)
    {
      offset = get32 ();
      seg = get16 ();
    }
  else
    {
      offset = get16 ();
      seg = get16 ();
    }
  used_prefixes |= (prefixes & PREFIX_DATA);
  if (intel_syntax)
    sprintf (scratchbuf, "0x%x:0x%x", seg, offset);
  else
    sprintf (scratchbuf, "$0x%x,$0x%x", seg, offset);
  oappend (scratchbuf);
}

  void
StaticData::OP_OFF (int bytemode, int sizeflag)
{
  bfd_vma off;

  if (intel_syntax && (sizeflag & SUFFIX_ALWAYS))
    intel_operand_size (bytemode, sizeflag);
  append_seg ();

  if ((sizeflag & AFLAG) || address_mode == mode_64bit)
    off = get32 ();
  else
    off = get16 ();

  if (intel_syntax)
    {
      if (!(prefixes & (PREFIX_CS | PREFIX_SS | PREFIX_DS
			| PREFIX_ES | PREFIX_FS | PREFIX_GS)))
	{
	  oappend (names_seg[ds_reg - es_reg]);
	  oappend (":");
	}
    }
  print_operand_value (scratchbuf, 1, off);
  oappend (scratchbuf);
}

  void
StaticData::OP_OFF64 (int bytemode, int sizeflag)
{
  bfd_vma off;

  if (address_mode != mode_64bit
      || (prefixes & PREFIX_ADDR))
    {
      OP_OFF (bytemode, sizeflag);
      return;
    }

  if (intel_syntax && (sizeflag & SUFFIX_ALWAYS))
    intel_operand_size (bytemode, sizeflag);
  append_seg ();

  off = get64 ();

  if (intel_syntax)
    {
      if (!(prefixes & (PREFIX_CS | PREFIX_SS | PREFIX_DS
			| PREFIX_ES | PREFIX_FS | PREFIX_GS)))
	{
	  oappend (names_seg[ds_reg - es_reg]);
	  oappend (":");
	}
    }
  print_operand_value (scratchbuf, 1, off);
  oappend (scratchbuf);
}

  void
StaticData::ptr_reg (int code, int sizeflag)
{
  const char *s;

  *obufp++ = open_char;
  used_prefixes |= (prefixes & PREFIX_ADDR);
  if (address_mode == mode_64bit)
    {
      if (!(sizeflag & AFLAG))
	s = names32[code - eAX_reg];
      else
	s = names64[code - eAX_reg];
    }
  else if (sizeflag & AFLAG)
    s = names32[code - eAX_reg];
  else
    s = names16[code - eAX_reg];
  oappend (s);
  *obufp++ = close_char;
  *obufp = 0;
}

  void
StaticData::OP_ESreg (int code, int sizeflag)
{
  if (intel_syntax)
    {
      switch (codep[-1])
	{
	case 0x6d:	/* insw/insl */
	  intel_operand_size (z_mode, sizeflag);
	  break;
	case 0xa5:	/* movsw/movsl/movsq */
	case 0xa7:	/* cmpsw/cmpsl/cmpsq */
	case 0xab:	/* stosw/stosl */
	case 0xaf:	/* scasw/scasl */
	  intel_operand_size (v_mode, sizeflag);
	  break;
	default:
	  intel_operand_size (b_mode, sizeflag);
	}
    }
  oappend ("%es:" + intel_syntax);
  ptr_reg (code, sizeflag);
}

  void
StaticData::OP_DSreg (int code, int sizeflag)
{
  if (intel_syntax)
    {
      switch (codep[-1])
	{
	case 0x6f:	/* outsw/outsl */
	  intel_operand_size (z_mode, sizeflag);
	  break;
	case 0xa5:	/* movsw/movsl/movsq */
	case 0xa7:	/* cmpsw/cmpsl/cmpsq */
	case 0xad:	/* lodsw/lodsl/lodsq */
	  intel_operand_size (v_mode, sizeflag);
	  break;
	default:
	  intel_operand_size (b_mode, sizeflag);
	}
    }
  if ((prefixes
       & (PREFIX_CS
	  | PREFIX_DS
	  | PREFIX_SS
	  | PREFIX_ES
	  | PREFIX_FS
	  | PREFIX_GS)) == 0)
    prefixes |= PREFIX_DS;
  append_seg ();
  ptr_reg (code, sizeflag);
}

  void
StaticData::OP_C (int dummy ATTRIBUTE_UNUSED, int sizeflag ATTRIBUTE_UNUSED)
{
  int add;
  if (rex & REX_R)
    {
      USED_REX (REX_R);
      add = 8;
    }
  else if (address_mode != mode_64bit && (prefixes & PREFIX_LOCK))
    {
      all_prefixes[last_lock_prefix] = 0;
      used_prefixes |= PREFIX_LOCK;
      add = 8;
    }
  else
    add = 0;
  sprintf (scratchbuf, "%%cr%d", modrm.reg + add);
  oappend (scratchbuf + intel_syntax);
}

  void
StaticData::OP_D (int dummy ATTRIBUTE_UNUSED, int sizeflag ATTRIBUTE_UNUSED)
{
  int add;
  USED_REX (REX_R);
  if (rex & REX_R)
    add = 8;
  else
    add = 0;
  if (intel_syntax)
    sprintf (scratchbuf, "db%d", modrm.reg + add);
  else
    sprintf (scratchbuf, "%%db%d", modrm.reg + add);
  oappend (scratchbuf);
}

  void
StaticData::OP_T (int dummy ATTRIBUTE_UNUSED, int sizeflag ATTRIBUTE_UNUSED)
{
  sprintf (scratchbuf, "%%tr%d", modrm.reg);
  oappend (scratchbuf + intel_syntax);
}

  void
StaticData::OP_R (int bytemode, int sizeflag)
{
  if (modrm.mod == 3)
    OP_E (bytemode, sizeflag);
  else
    BadOp ();
}

  void
StaticData::OP_MMX (int bytemode ATTRIBUTE_UNUSED, int sizeflag ATTRIBUTE_UNUSED)
{
  int reg = modrm.reg;
  const char **names;

  used_prefixes |= (prefixes & PREFIX_DATA);
  if (prefixes & PREFIX_DATA)
    {
      names = names_xmm;
      USED_REX (REX_R);
      if (rex & REX_R)
	reg += 8;
    }
  else
    names = names_mm;
  oappend (names[reg]);
}

  void
StaticData::OP_XMM (int bytemode, int sizeflag ATTRIBUTE_UNUSED)
{
  int reg = modrm.reg;
  const char **names;

  USED_REX (REX_R);
  if (rex & REX_R)
    reg += 8;
  if (need_vex
      && bytemode != xmm_mode
      && bytemode != scalar_mode)
    {
      switch (vex.length)
	{
	case 128:
	  names = names_xmm;
	  break;
	case 256:
	  names = names_ymm;
	  break;
	default:
	  cerr << "vex.length wrong :" << vex.length << endl;
	  abort ();
	}
    }
  else
    names = names_xmm;
  oappend (names[reg]);
}

  void
StaticData::OP_EM (int bytemode, int sizeflag)
{
  int reg;
  const char **names;

  if (modrm.mod != 3)
    {
      if (intel_syntax
	  && (bytemode == v_mode || bytemode == v_swap_mode))
	{
	  bytemode = (prefixes & PREFIX_DATA) ? x_mode : q_mode;
	  used_prefixes |= (prefixes & PREFIX_DATA);
 	}
      OP_E (bytemode, sizeflag);
      return;
    }

  if ((sizeflag & SUFFIX_ALWAYS) && bytemode == v_swap_mode)
    swap_operand ();

  /* Skip mod/rm byte.  */
  MODRM_CHECK;
  codep++;
  used_prefixes |= (prefixes & PREFIX_DATA);
  reg = modrm.rm;
  if (prefixes & PREFIX_DATA)
    {
      names = names_xmm;
      USED_REX (REX_B);
      if (rex & REX_B)
	reg += 8;
    }
  else
    names = names_mm;
  oappend (names[reg]);
}

/* cvt* are the only instructions in sse2 which have
   both SSE and MMX operands and also have 0x66 prefix
   in their opcode. 0x66 was originally used to differentiate
   between SSE and MMX instruction(operands). So we have to handle the
   cvt* separately using OP_EMC and OP_MXC */
  void
StaticData::OP_EMC (int bytemode, int sizeflag)
{
  if (modrm.mod != 3)
    {
      if (intel_syntax && bytemode == v_mode)
	{
	  bytemode = (prefixes & PREFIX_DATA) ? x_mode : q_mode;
	  used_prefixes |= (prefixes & PREFIX_DATA);
 	}
      OP_E (bytemode, sizeflag);
      return;
    }

  /* Skip mod/rm byte.  */
  MODRM_CHECK;
  codep++;
  used_prefixes |= (prefixes & PREFIX_DATA);
  oappend (names_mm[modrm.rm]);
}

  void
StaticData::OP_MXC (int bytemode ATTRIBUTE_UNUSED, int sizeflag ATTRIBUTE_UNUSED)
{
  used_prefixes |= (prefixes & PREFIX_DATA);
  oappend (names_mm[modrm.reg]);
}

  void
StaticData::OP_EX (int bytemode, int sizeflag)
{
  int reg;
  const char **names;

  /* Skip mod/rm byte.  */
  MODRM_CHECK;
  codep++;

  if (modrm.mod != 3)
    {
      OP_E_memory (bytemode, sizeflag);
      return;
    }

  reg = modrm.rm;
  USED_REX (REX_B);
  if (rex & REX_B)
    reg += 8;

  if ((sizeflag & SUFFIX_ALWAYS)
      && (bytemode == x_swap_mode
	  || bytemode == d_swap_mode
	  || bytemode == d_scalar_swap_mode 
	  || bytemode == q_swap_mode
	  || bytemode == q_scalar_swap_mode))
    swap_operand ();

  if (need_vex
      && bytemode != xmm_mode
      && bytemode != xmmq_mode
      && bytemode != d_scalar_mode
      && bytemode != d_scalar_swap_mode 
      && bytemode != q_scalar_mode
      && bytemode != q_scalar_swap_mode
      && bytemode != vex_scalar_w_dq_mode)
    {
      switch (vex.length)
	{
	case 128:
	  names = names_xmm;
	  break;
	case 256:
	  names = names_ymm;
	  break;
	default:
	  cerr << "vex.length wrong :" << vex.length << endl;
	  abort ();
	}
    }
  else
    names = names_xmm;
  oappend (names[reg]);
}

  void
StaticData::OP_MS (int bytemode, int sizeflag)
{
  if (modrm.mod == 3)
    OP_EM (bytemode, sizeflag);
  else
    BadOp ();
}

  void
StaticData::OP_XS (int bytemode, int sizeflag)
{
  if (modrm.mod == 3)
    OP_EX (bytemode, sizeflag);
  else
    BadOp ();
}

  void
StaticData::OP_M (int bytemode, int sizeflag)
{
  if (modrm.mod == 3)
    /* bad bound,lea,lds,les,lfs,lgs,lss,cmpxchg8b,vmptrst modrm */
    BadOp ();
  else
    OP_E (bytemode, sizeflag);
}

  void
StaticData::OP_0f07 (int bytemode, int sizeflag)
{
  if (modrm.mod != 3 || modrm.rm != 0)
    BadOp ();
  else
    OP_E (bytemode, sizeflag);
}

/* NOP is an alias of "xchg %ax,%ax" in 16bit mode, "xchg %eax,%eax" in
   32bit mode and "xchg %rax,%rax" in 64bit mode.  */

  void
StaticData::NOP_Fixup1 (int bytemode, int sizeflag)
{
  if ((prefixes & PREFIX_DATA) != 0
      || (rex != 0
	  && rex != 0x48
	  && address_mode == mode_64bit))
    OP_REG (bytemode, sizeflag);
  else
    strcpy (obuf, "nop");
}

  void
StaticData::NOP_Fixup2 (int bytemode, int sizeflag)
{
  if ((prefixes & PREFIX_DATA) != 0
      || (rex != 0
	  && rex != 0x48
	  && address_mode == mode_64bit))
    OP_IMREG (bytemode, sizeflag);
}


  void
StaticData::OP_3DNowSuffix (int bytemode ATTRIBUTE_UNUSED, int sizeflag ATTRIBUTE_UNUSED)
{
  const char *mnemonic;

  FETCH_DATA (the_info, codep + 1);
  /* AMD 3DNow! instructions are specified by an opcode suffix in the
     place where an 8-bit immediate would normally go.  ie. the last
     byte of the instruction.  */
  obufp = mnemonicendp;
  mnemonic = Suffix3DNow[*codep++ & 0xff];
  if (mnemonic)
    oappend (mnemonic);
  else
    {
      /* Since a variable sized modrm/sib chunk is between the start
	 of the opcode (0x0f0f) and the opcode suffix, we need to do
	 all the modrm processing first, and don't know until now that
	 we have a bad opcode.  This necessitates some cleaning up.  */
      op_out[0][0] = '\0';
      op_out[1][0] = '\0';
      BadOp ();
    }
  mnemonicendp = obufp;
}


  void
StaticData::CMP_Fixup (int bytemode ATTRIBUTE_UNUSED, int sizeflag ATTRIBUTE_UNUSED)
{
  unsigned int cmp_type;

  FETCH_DATA (the_info, codep + 1);
  cmp_type = *codep++ & 0xff;
  if (cmp_type < ARRAY_SIZE (simd_cmp_op))
    {
      char suffix [3];
      char *p = mnemonicendp - 2;
      suffix[0] = p[0];
      suffix[1] = p[1];
      suffix[2] = '\0';
      sprintf (p, "%s%s", simd_cmp_op[cmp_type].name, suffix);
      mnemonicendp += simd_cmp_op[cmp_type].len;
    }
  else
    {
      /* We have a reserved extension byte.  Output it directly.  */
      scratchbuf[0] = '$';
      print_operand_value (scratchbuf + 1, 1, cmp_type);
      oappend (scratchbuf + intel_syntax);
      scratchbuf[0] = '\0';
    }
}

  void
StaticData::OP_Mwait (int bytemode ATTRIBUTE_UNUSED,
	  int sizeflag ATTRIBUTE_UNUSED)
{
  /* mwait %eax,%ecx  */
  if (!intel_syntax)
    {
      const char **names = (address_mode == mode_64bit
			    ? names64 : names32);
      strcpy (op_out[0], names[0]);
      strcpy (op_out[1], names[1]);
      two_source_ops = 1;
    }
  /* Skip mod/rm byte.  */
  MODRM_CHECK;
  codep++;
}

  void
StaticData::OP_Monitor (int bytemode ATTRIBUTE_UNUSED,
	    int sizeflag ATTRIBUTE_UNUSED)
{
  /* monitor %eax,%ecx,%edx"  */
  if (!intel_syntax)
    {
      const char **op1_names;
      const char **names = (address_mode == mode_64bit
			    ? names64 : names32);

      if (!(prefixes & PREFIX_ADDR))
	op1_names = (address_mode == mode_16bit
		     ? names16 : names);
      else
	{
	  /* Remove "addr16/addr32".  */
	  all_prefixes[last_addr_prefix] = 0;
	  op1_names = (address_mode != mode_32bit
		       ? names32 : names16);
	  used_prefixes |= PREFIX_ADDR;
	}
      strcpy (op_out[0], op1_names[0]);
      strcpy (op_out[1], names[1]);
      strcpy (op_out[2], names[2]);
      two_source_ops = 1;
    }
  /* Skip mod/rm byte.  */
  MODRM_CHECK;
  codep++;
}

  void
StaticData::BadOp (void)
{
  /* Throw away prefixes and 1st. opcode byte.  */
  codep = insn_codep + 1;
  oappend ("(bad)");
}

  void
StaticData::REP_Fixup (int bytemode, int sizeflag)
{
  /* The 0xf3 prefix should be displayed as "rep" for ins, outs, movs,
     lods and stos.  */
  if (prefixes & PREFIX_REPZ)
    all_prefixes[last_repz_prefix] = REP_PREFIX;

  switch (bytemode)
    {
    case al_reg:
    case eAX_reg:
    case indir_dx_reg:
      OP_IMREG (bytemode, sizeflag);
      break;
    case eDI_reg:
      OP_ESreg (bytemode, sizeflag);
      break;
    case eSI_reg:
      OP_DSreg (bytemode, sizeflag);
      break;
    default:
      cerr << "bytemode wrong :" << bytemode << endl;
      abort ();
      break;
    }
}

  void
StaticData::CMPXCHG8B_Fixup (int bytemode, int sizeflag)
{
  USED_REX (REX_W);
  if (rex & REX_W)
    {
      /* Change cmpxchg8b to cmpxchg16b.  */
      char *p = mnemonicendp - 2;
      mnemonicendp = stpcpy (p, "16b");
      bytemode = o_mode;
    }
  OP_M (bytemode, sizeflag);
}

  void
StaticData::XMM_Fixup (int reg, int sizeflag ATTRIBUTE_UNUSED)
{
  const char **names;

  if (need_vex)
    {
      switch (vex.length)
	{
	case 128:
	  names = names_xmm;
	  break;
	case 256:
	  names = names_ymm;
	  break;
	default:
	  cerr << "vex.length wrong :" << vex.length << endl;
	  abort ();
	}
    }
  else
    names = names_xmm;
  oappend (names[reg]);
}

  void
StaticData::CRC32_Fixup (int bytemode, int sizeflag)
{
  /* Add proper suffix to "crc32".  */
  char *p = mnemonicendp;

  switch (bytemode)
    {
    case b_mode:
      if (intel_syntax)
	goto skip;

      *p++ = 'b';
      break;
    case v_mode:
      if (intel_syntax)
	goto skip;

      USED_REX (REX_W);
      if (rex & REX_W)
	*p++ = 'q';
      else 
	{
	  if (sizeflag & DFLAG)
	    *p++ = 'l';
	  else
	    *p++ = 'w';
	  used_prefixes |= (prefixes & PREFIX_DATA);
	}
      break;
    default:
      oappend (INTERNAL_DISASSEMBLER_ERROR);
      break;
    }
  mnemonicendp = p;
  *p = '\0';

skip:
  if (modrm.mod == 3)
    {
      int add;

      /* Skip mod/rm byte.  */
      MODRM_CHECK;
      codep++;

      USED_REX (REX_B);
      add = (rex & REX_B) ? 8 : 0;
      if (bytemode == b_mode)
	{
	  USED_REX (0);
	  if (rex)
	    oappend (names8rex[modrm.rm + add]);
	  else
	    oappend (names8[modrm.rm + add]);
	}
      else
	{
	  USED_REX (REX_W);
	  if (rex & REX_W)
	    oappend (names64[modrm.rm + add]);
	  else if ((prefixes & PREFIX_DATA))
	    oappend (names16[modrm.rm + add]);
	  else
	    oappend (names32[modrm.rm + add]);
	}
    }
  else
    OP_E (bytemode, sizeflag);
}

  void
StaticData::FXSAVE_Fixup (int bytemode, int sizeflag)
{
  /* Add proper suffix to "fxsave" and "fxrstor".  */
  USED_REX (REX_W);
  if (rex & REX_W)
    {
      char *p = mnemonicendp;
      *p++ = '6';
      *p++ = '4';
      *p = '\0';
      mnemonicendp = p;
    }
  OP_M (bytemode, sizeflag);
}

/* Display the destination register operand for instructions with
   VEX. */

  void
StaticData::OP_VEX (int bytemode, int sizeflag ATTRIBUTE_UNUSED)
{
  int reg;
  const char **names;

  if (!need_vex)
    {
      cerr << "! need_vex" << endl;
      abort ();
    }

  if (!need_vex_reg)
    return;

  reg = vex.register_specifier;
  if (bytemode == vex_scalar_mode)
    {
      oappend (names_xmm[reg]);
      return;
    }

  switch (vex.length)
    {
    case 128:
      switch (bytemode)
	{
	case vex_mode:
	case vex128_mode:
	  break;
	default:
	  cerr << "vex.length wrong :" << vex.length << endl;
	  abort ();
	  return;
	}

      names = names_xmm;
      break;
    case 256:
      switch (bytemode)
	{
	case vex_mode:
	case vex256_mode:
	  break;
	default:
	  cerr << "bytemode wrong :" << bytemode << endl;
	  abort ();
	  return;
	}

      names = names_ymm;
      break;
    default:
      cerr << "vex.length wrong :" << vex.length << endl;
      abort ();
      break;
    }
  oappend (names[reg]);
}

/* Get the VEX immediate byte without moving codep.  */

unsigned char
StaticData::get_vex_imm8 (int sizeflag, int opnum)
{
  int bytes_before_imm = 0;

  if (modrm.mod != 3)
    {
      /* There are SIB/displacement bytes.  */
      if ((sizeflag & AFLAG) || address_mode == mode_64bit)
        {
	  /* 32/64 bit address mode */
          int base = modrm.rm;

	  /* Check SIB byte.  */
          if (base == 4)
            {
              FETCH_DATA (the_info, codep + 1);
              base = *codep & 7;
              /* When decoding the third source, don't increase
                 bytes_before_imm as this has already been incremented
                 by one in OP_E_memory while decoding the second
                 source operand.  */
              if (opnum == 0)
                bytes_before_imm++;
            }

          /* Don't increase bytes_before_imm when decoding the third source,
             it has already been incremented by OP_E_memory while decoding
             the second source operand.  */
          if (opnum == 0)
            {
              switch (modrm.mod)
                {
                  case 0:
                    /* When modrm.rm == 5 or modrm.rm == 4 and base in
                       SIB == 5, there is a 4 byte displacement.  */
                    if (base != 5)
                      /* No displacement. */
                      break;
                  case 2:
                    /* 4 byte displacement.  */
                    bytes_before_imm += 4;
                    break;
                  case 1:
                    /* 1 byte displacement.  */
                    bytes_before_imm++;
                    break;
                }
            }
        }
      else
	{
	  /* 16 bit address mode */
          /* Don't increase bytes_before_imm when decoding the third source,
             it has already been incremented by OP_E_memory while decoding
             the second source operand.  */
          if (opnum == 0)
            {
	      switch (modrm.mod)
		{
		case 0:
		  /* When modrm.rm == 6, there is a 2 byte displacement.  */
		  if (modrm.rm != 6)
		    /* No displacement. */
		    break;
		case 2:
		  /* 2 byte displacement.  */
		  bytes_before_imm += 2;
		  break;
		case 1:
		  /* 1 byte displacement: when decoding the third source,
		     don't increase bytes_before_imm as this has already
		     been incremented by one in OP_E_memory while decoding
		     the second source operand.  */
		  if (opnum == 0)
		    bytes_before_imm++;

		  break;
		}
	    }
	}
    }

  FETCH_DATA (the_info, codep + bytes_before_imm + 1);
  return codep [bytes_before_imm];
}

  void
StaticData::OP_EX_VexReg (int bytemode, int sizeflag, int reg)
{
  const char **names;

  if (reg == -1 && modrm.mod != 3)
    {
      OP_E_memory (bytemode, sizeflag);
      return;
    }
  else
    {
      if (reg == -1)
	{
	  reg = modrm.rm;
	  USED_REX (REX_B);
	  if (rex & REX_B)
	    reg += 8;
	}
      else if (reg > 7 && address_mode != mode_64bit)
	BadOp ();
    }

  switch (vex.length)
    {
    case 128:
      names = names_xmm;
      break;
    case 256:
      names = names_ymm;
      break;
    default:
      cerr << "vex.length wrong :" << vex.length << endl;
      abort ();
    }
  oappend (names[reg]);
}

  void
StaticData::OP_EX_VexImmW (int bytemode, int sizeflag)
{
  int reg = -1;
  static unsigned char vex_imm8;

  if (vex_w_done == 0)
    {
      vex_w_done = 1;

      /* Skip mod/rm byte.  */
      MODRM_CHECK;
      codep++;

      vex_imm8 = get_vex_imm8 (sizeflag, 0);

      if (vex.w)
	  reg = vex_imm8 >> 4;

      OP_EX_VexReg (bytemode, sizeflag, reg);
    }
  else if (vex_w_done == 1)
    {
      vex_w_done = 2;

      if (!vex.w)
	  reg = vex_imm8 >> 4;

      OP_EX_VexReg (bytemode, sizeflag, reg);
    }
  else
    {
      /* Output the imm8 directly.  */
      scratchbuf[0] = '$';
      print_operand_value (scratchbuf + 1, 1, vex_imm8 & 0xf);
      oappend (scratchbuf + intel_syntax);
      scratchbuf[0] = '\0';
      codep++;
    }
}

 void
StaticData::OP_Vex_2src (int bytemode, int sizeflag)
{
  if (modrm.mod == 3)
    {
      int reg = modrm.rm;
      USED_REX (REX_B);
      if (rex & REX_B)
	reg += 8;
      oappend (names_xmm[reg]);
    }
  else
    {
      if (intel_syntax
	  && (bytemode == v_mode || bytemode == v_swap_mode))
	{
	  bytemode = (prefixes & PREFIX_DATA) ? x_mode : q_mode;
	  used_prefixes |= (prefixes & PREFIX_DATA);
	}
      OP_E (bytemode, sizeflag);
    }
}

  void
StaticData::OP_Vex_2src_1 (int bytemode, int sizeflag)
{
  if (modrm.mod == 3)
    {
      /* Skip mod/rm byte.   */
      MODRM_CHECK;
      codep++;
    }

  if (vex.w)
    oappend (names_xmm[vex.register_specifier]);
  else
    OP_Vex_2src (bytemode, sizeflag);
}

  void
StaticData::OP_Vex_2src_2 (int bytemode, int sizeflag)
{
  if (vex.w)
    OP_Vex_2src (bytemode, sizeflag);
  else
    oappend (names_xmm[vex.register_specifier]);
}

  void
StaticData::OP_EX_VexW (int bytemode, int sizeflag)
{
  int reg = -1;

  if (!vex_w_done)
    {
      vex_w_done = 1;

      /* Skip mod/rm byte.  */
      MODRM_CHECK;
      codep++;

      if (vex.w)
	reg = get_vex_imm8 (sizeflag, 0) >> 4;
    }
  else
    {
      if (!vex.w)
	reg = get_vex_imm8 (sizeflag, 1) >> 4;
    }

  OP_EX_VexReg (bytemode, sizeflag, reg);
}

  void
StaticData::VEXI4_Fixup (int bytemode ATTRIBUTE_UNUSED,
	     int sizeflag ATTRIBUTE_UNUSED)
{
  /* Skip the immediate byte and check for invalid bits.  */
  FETCH_DATA (the_info, codep + 1);
  if (*codep++ & 0xf)
    BadOp ();
}

  void
StaticData::OP_REG_VexI4 (int bytemode, int sizeflag ATTRIBUTE_UNUSED)
{
  int reg;
  const char **names;

  FETCH_DATA (the_info, codep + 1);
  reg = *codep++;

  if (bytemode != x_mode)
    {      
      cerr << "bytemode wrong :" << bytemode << endl;
      abort ();
    }

  if (reg & 0xf)
      BadOp ();

  reg >>= 4;
  if (reg > 7 && address_mode != mode_64bit)
    BadOp ();

  switch (vex.length)
    {
    case 128:
      names = names_xmm;
      break;
    case 256:
      names = names_ymm;
      break;
    default:
      cerr << "vex.length wrong :" << vex.length << endl;
      abort ();
    }
  oappend (names[reg]);
}

  void
StaticData::OP_XMM_VexW (int bytemode, int sizeflag)
{
  /* Turn off the REX.W bit since it is used for swapping operands
     now.  */
  rex &= ~REX_W;
  OP_XMM (bytemode, sizeflag);
}

  void
StaticData::OP_EX_Vex (int bytemode, int sizeflag)
{
  if (modrm.mod != 3)
    {
      if (vex.register_specifier != 0)
	BadOp ();
      need_vex_reg = 0;
    }
  OP_EX (bytemode, sizeflag);
}

  void
StaticData::OP_XMM_Vex (int bytemode, int sizeflag)
{
  if (modrm.mod != 3)
    {
      if (vex.register_specifier != 0)
	BadOp ();
      need_vex_reg = 0;
    }
  OP_XMM (bytemode, sizeflag);
}

  void
StaticData::VZERO_Fixup (int bytemode ATTRIBUTE_UNUSED, int sizeflag ATTRIBUTE_UNUSED)
{
  switch (vex.length)
    {
    case 128:
      mnemonicendp = stpcpy (obuf, "vzeroupper");
      break;
    case 256:
      mnemonicendp = stpcpy (obuf, "vzeroall");
      break;
    default:
      cerr << "vex.length wrong :" << vex.length << endl;
      abort ();
    }
}


  void
StaticData::VCMP_Fixup (int bytemode ATTRIBUTE_UNUSED, int sizeflag ATTRIBUTE_UNUSED)
{
  unsigned int cmp_type;

  FETCH_DATA (the_info, codep + 1);
  cmp_type = *codep++ & 0xff;
  if (cmp_type < ARRAY_SIZE_vex_cmp_op())
    {
      char suffix [3];
      char *p = mnemonicendp - 2;
      suffix[0] = p[0];
      suffix[1] = p[1];
      suffix[2] = '\0';
      sprintf (p, "%s%s", vex_cmp_op[cmp_type].name, suffix);
      mnemonicendp += vex_cmp_op[cmp_type].len;
    }
  else
    {
      /* We have a reserved extension byte.  Output it directly.  */
      scratchbuf[0] = '$';
      print_operand_value (scratchbuf + 1, 1, cmp_type);
      oappend (scratchbuf + intel_syntax);
      scratchbuf[0] = '\0';
    }
}


void
StaticData::PCLMUL_Fixup (int bytemode ATTRIBUTE_UNUSED,
	      int sizeflag ATTRIBUTE_UNUSED)
{
  unsigned int pclmul_type;

  FETCH_DATA (the_info, codep + 1);
  pclmul_type = *codep++ & 0xff;
  switch (pclmul_type)
    {
    case 0x10:
      pclmul_type = 2;
      break;
    case 0x11:
      pclmul_type = 3;
      break;
    default:
      break;
    } 
  if (pclmul_type < ARRAY_SIZE (pclmul_op))
    {
      char suffix [4];
      char *p = mnemonicendp - 3;
      suffix[0] = p[0];
      suffix[1] = p[1];
      suffix[2] = p[2];
      suffix[3] = '\0';
      sprintf (p, "%s%s", pclmul_op[pclmul_type].name, suffix);
      mnemonicendp += pclmul_op[pclmul_type].len;
    }
  else
    {
      /* We have a reserved extension byte.  Output it directly.  */
      scratchbuf[0] = '$';
      print_operand_value (scratchbuf + 1, 1, pclmul_type);
      oappend (scratchbuf + intel_syntax);
      scratchbuf[0] = '\0';
    }
}

 void
StaticData::MOVBE_Fixup (int bytemode, int sizeflag)
{
  /* Add proper suffix to "movbe".  */
  char *p = mnemonicendp;

  switch (bytemode)
    {
    case v_mode:
      if (intel_syntax)
	goto skip;

      USED_REX (REX_W);
      if (sizeflag & SUFFIX_ALWAYS)
	{
	  if (rex & REX_W)
	    *p++ = 'q';
	  else
	    {
	      if (sizeflag & DFLAG)
		*p++ = 'l';
	      else
		*p++ = 'w';
	      used_prefixes |= (prefixes & PREFIX_DATA);
	    }
	}
      break;
    default:
      oappend (INTERNAL_DISASSEMBLER_ERROR);
      break;
    }
  mnemonicendp = p;
  *p = '\0';

skip:
  OP_M (bytemode, sizeflag);
}

 void
StaticData::OP_LWPCB_E (int bytemode ATTRIBUTE_UNUSED, int sizeflag ATTRIBUTE_UNUSED)
{
  int reg;
  const char **names;

  /* Skip mod/rm byte.  */
  MODRM_CHECK;
  codep++;

  if (vex.w)
    names = names64;
  else
    names = names32;

  reg = modrm.rm;
  USED_REX (REX_B);
  if (rex & REX_B)
    reg += 8;

  oappend (names[reg]);
}
 
 void
StaticData::OP_LWP_E (int bytemode ATTRIBUTE_UNUSED, int sizeflag ATTRIBUTE_UNUSED)
{
  const char **names;

  if (vex.w)
    names = names64;
  else
    names = names32;

  oappend (names[vex.register_specifier]);
}

#define makef(X)   \
  void X (int a, int b) \
  { \
  return glob.X(a,b); \
  }

// the static data
  StaticData glob;

  makef (CMP_Fixup)
  makef (CMPXCHG8B_Fixup)
  makef (CRC32_Fixup)
  makef(FXSAVE_Fixup)
  makef(MOVBE_Fixup)
  makef(NOP_Fixup1)
  makef(NOP_Fixup2)
  makef(OP_0f07)
  makef(OP_3DNowSuffix)
  makef(OP_C)
  makef(OP_D)
  makef(OP_DIR)
  makef(  OP_DSreg)
  makef(  OP_E)
  makef(  OP_EMC)
  makef(  OP_EM)
  makef(  OP_ESreg)
  makef(  OP_EX)
  makef(  OP_EX_VexImmW)
  makef(  OP_EX_Vex)
  makef(  OP_EX_VexW)
  makef(  OP_G)
  makef(  OP_I64)
  makef(  OP_I)
  makef(  OP_IMREG)
  makef(  OP_indirE)
  makef(  OP_J)
  makef(  OP_LWPCB_E)
  makef(  OP_LWP_E)
  makef(  OP_M)
  makef(  OP_MMX)
  makef(  OP_Monitor)
  makef(  OP_MS)
  makef(  OP_Mwait)
  makef(  OP_MXC)
  makef(  OP_OFF64)
  makef(  OP_REG)
  makef(  OP_REG_VexI4)
  makef(  OP_R)
  makef(  OP_SEG)
  makef(  OP_sI)
  makef(  OP_Skip_MODRM)
  makef(  OP_STi)
  makef(  OP_ST)
  makef(  OP_T)
  makef(  OP_Vex_2src_1)
  makef(  OP_Vex_2src_2)
  makef(  OP_VEX)
  makef(  OP_XMM)
  makef(  OP_XMM_Vex)
  makef(  OP_XMM_VexW)
  makef(  OP_XS)
  makef(  PCLMUL_Fixup)
  makef(  REP_Fixup)
  makef(  VCMP_Fixup)
  makef(  VEXI4_Fixup)
  makef(  VZERO_Fixup)
  makef(  XMM_Fixup)
  //void (*)(int, int), int)
  //  T_op::T_op(void (*)(int, int), int){}

// moved code to i386-dis-op.cpp

  // allocate the space.
  char * StaticData::INTERNAL_DISASSEMBLER_ERROR;

  #include "i386-dis-table_fgrps.h"


/*StaticData::att_index16
StaticData::att_index32
StaticData::att_index64
StaticData::att_names16
StaticData::att_names32
StaticData::att_names64
StaticData::att_names8
StaticData::att_names8rex
StaticData::att_names_mm
StaticData::att_names_seg
StaticData::att_names_xmm
StaticData::att_names_ymm
StaticData::float_mem
StaticData::intel_index16
StaticData::intel_index32
StaticData::intel_index64
StaticData::intel_names16
StaticData::intel_names32
StaticData::intel_names64
StaticData::intel_names8
StaticData::intel_names8rex
StaticData::intel_names_mm
StaticData::intel_names_seg
StaticData::intel_names_xmm
StaticData::intel_names_ymm
StaticData::names_mm
StaticData::names_xmm
StaticData::names_ymm
StaticData::vex_cmp_op
StaticData::rexes
*/
#include "i386-dis-table_floatmem.h"
#include "i386-dis-table_names.h"
#include "i386-dis-table_vex_cmp_ops.h"
#include "i386-dis-table_rex.h"

unsigned int StaticData::ARRAY_SIZE_vex_cmp_op()
{
    cerr << "TODO ARRAY_SIZE_vex_cmp_op" << endl;
  // TODO: calculate the length
  return 1;//ARRAY_SIZE(vex_cmp_op);
}

}; // end of namespace


void print_i386_disassembler_options (FILE *stream)
{
  binutils_objdump_opcodes_i386::glob.print_i386_disassembler_options (stream);
}


int print_insn_i386 (bfd_vma pc, disassemble_info *info)
{
  binutils_objdump_opcodes_i386::glob.print_insn_i386 ( pc, info);
}
