const struct dis386 vex_table[][256] = {
  /* VEX_0F */
  {
    /* 00 */
    { Bad_Opcode },
    { Bad_Opcode },
    { Bad_Opcode },
    { Bad_Opcode },
    { Bad_Opcode },
    { Bad_Opcode },
    { Bad_Opcode },
    { Bad_Opcode },
    /* 08 */
    { Bad_Opcode },
    { Bad_Opcode },
    { Bad_Opcode },
    { Bad_Opcode },
    { Bad_Opcode },
    { Bad_Opcode },
    { Bad_Opcode },
    { Bad_Opcode },
    /* 10 */
    { PREFIX_TABLE (PREFIX_VEX_0F10) },
    { PREFIX_TABLE (PREFIX_VEX_0F11) },
    { PREFIX_TABLE (PREFIX_VEX_0F12) },
    { MOD_TABLE (MOD_VEX_0F13) },
    { VEX_W_TABLE (VEX_W_0F14) },
    { VEX_W_TABLE (VEX_W_0F15) },
    { PREFIX_TABLE (PREFIX_VEX_0F16) },
    { MOD_TABLE (MOD_VEX_0F17) },
    /* 18 */
    { Bad_Opcode },
    { Bad_Opcode },
    { Bad_Opcode },
    { Bad_Opcode },
    { Bad_Opcode },
    { Bad_Opcode },
    { Bad_Opcode },
    { Bad_Opcode },
    /* 20 */
    { Bad_Opcode },
    { Bad_Opcode },
    { Bad_Opcode },
    { Bad_Opcode },
    { Bad_Opcode },
    { Bad_Opcode },
    { Bad_Opcode },
    { Bad_Opcode },
    /* 28 */
    { VEX_W_TABLE (VEX_W_0F28) },
    { VEX_W_TABLE (VEX_W_0F29) },
    { PREFIX_TABLE (PREFIX_VEX_0F2A) },
    { MOD_TABLE (MOD_VEX_0F2B) },
    { PREFIX_TABLE (PREFIX_VEX_0F2C) },
    { PREFIX_TABLE (PREFIX_VEX_0F2D) },
    { PREFIX_TABLE (PREFIX_VEX_0F2E) },
    { PREFIX_TABLE (PREFIX_VEX_0F2F) },
    /* 30 */
    { Bad_Opcode },
    { Bad_Opcode },
    { Bad_Opcode },
    { Bad_Opcode },
    { Bad_Opcode },
    { Bad_Opcode },
    { Bad_Opcode },
    { Bad_Opcode },
    /* 38 */
    { Bad_Opcode },
    { Bad_Opcode },
    { Bad_Opcode },
    { Bad_Opcode },
    { Bad_Opcode },
    { Bad_Opcode },
    { Bad_Opcode },
    { Bad_Opcode },
    /* 40 */
    { Bad_Opcode },
    { Bad_Opcode },
    { Bad_Opcode },
    { Bad_Opcode },
    { Bad_Opcode },
    { Bad_Opcode },
    { Bad_Opcode },
    { Bad_Opcode },
    /* 48 */
    { Bad_Opcode },
    { Bad_Opcode },
    { Bad_Opcode },
    { Bad_Opcode },
    { Bad_Opcode },
    { Bad_Opcode },
    { Bad_Opcode },
    { Bad_Opcode },
    /* 50 */
    { MOD_TABLE (MOD_VEX_0F50) },
    { PREFIX_TABLE (PREFIX_VEX_0F51) },
    { PREFIX_TABLE (PREFIX_VEX_0F52) },
    { PREFIX_TABLE (PREFIX_VEX_0F53) },
    { "vandpX",		{ XM, Vex, EXx } },
    { "vandnpX",	{ XM, Vex, EXx } },
    { "vorpX",		{ XM, Vex, EXx } },
    { "vxorpX",		{ XM, Vex, EXx } },
    /* 58 */
    { PREFIX_TABLE (PREFIX_VEX_0F58) },
    { PREFIX_TABLE (PREFIX_VEX_0F59) },
    { PREFIX_TABLE (PREFIX_VEX_0F5A) },
    { PREFIX_TABLE (PREFIX_VEX_0F5B) },
    { PREFIX_TABLE (PREFIX_VEX_0F5C) },
    { PREFIX_TABLE (PREFIX_VEX_0F5D) },
    { PREFIX_TABLE (PREFIX_VEX_0F5E) },
    { PREFIX_TABLE (PREFIX_VEX_0F5F) },
    /* 60 */
    { PREFIX_TABLE (PREFIX_VEX_0F60) },
    { PREFIX_TABLE (PREFIX_VEX_0F61) },
    { PREFIX_TABLE (PREFIX_VEX_0F62) },
    { PREFIX_TABLE (PREFIX_VEX_0F63) },
    { PREFIX_TABLE (PREFIX_VEX_0F64) },
    { PREFIX_TABLE (PREFIX_VEX_0F65) },
    { PREFIX_TABLE (PREFIX_VEX_0F66) },
    { PREFIX_TABLE (PREFIX_VEX_0F67) },
    /* 68 */
    { PREFIX_TABLE (PREFIX_VEX_0F68) },
    { PREFIX_TABLE (PREFIX_VEX_0F69) },
    { PREFIX_TABLE (PREFIX_VEX_0F6A) },
    { PREFIX_TABLE (PREFIX_VEX_0F6B) },
    { PREFIX_TABLE (PREFIX_VEX_0F6C) },
    { PREFIX_TABLE (PREFIX_VEX_0F6D) },
    { PREFIX_TABLE (PREFIX_VEX_0F6E) },
    { PREFIX_TABLE (PREFIX_VEX_0F6F) },
    /* 70 */
    { PREFIX_TABLE (PREFIX_VEX_0F70) },
    { REG_TABLE (REG_VEX_0F71) },
    { REG_TABLE (REG_VEX_0F72) },
    { REG_TABLE (REG_VEX_0F73) },
    { PREFIX_TABLE (PREFIX_VEX_0F74) },
    { PREFIX_TABLE (PREFIX_VEX_0F75) },
    { PREFIX_TABLE (PREFIX_VEX_0F76) },
    { PREFIX_TABLE (PREFIX_VEX_0F77) },
    /* 78 */
    { Bad_Opcode },
    { Bad_Opcode },
    { Bad_Opcode },
    { Bad_Opcode },
    { PREFIX_TABLE (PREFIX_VEX_0F7C) },
    { PREFIX_TABLE (PREFIX_VEX_0F7D) },
    { PREFIX_TABLE (PREFIX_VEX_0F7E) },
    { PREFIX_TABLE (PREFIX_VEX_0F7F) },
    /* 80 */
    { Bad_Opcode },
    { Bad_Opcode },
    { Bad_Opcode },
    { Bad_Opcode },
    { Bad_Opcode },
    { Bad_Opcode },
    { Bad_Opcode },
    { Bad_Opcode },
    /* 88 */
    { Bad_Opcode },
    { Bad_Opcode },
    { Bad_Opcode },
    { Bad_Opcode },
    { Bad_Opcode },
    { Bad_Opcode },
    { Bad_Opcode },
    { Bad_Opcode },
    /* 90 */
    { Bad_Opcode },
    { Bad_Opcode },
    { Bad_Opcode },
    { Bad_Opcode },
    { Bad_Opcode },
    { Bad_Opcode },
    { Bad_Opcode },
    { Bad_Opcode },
    /* 98 */
    { Bad_Opcode },
    { Bad_Opcode },
    { Bad_Opcode },
    { Bad_Opcode },
    { Bad_Opcode },
    { Bad_Opcode },
    { Bad_Opcode },
    { Bad_Opcode },
    /* a0 */
    { Bad_Opcode },
    { Bad_Opcode },
    { Bad_Opcode },
    { Bad_Opcode },
    { Bad_Opcode },
    { Bad_Opcode },
    { Bad_Opcode },
    { Bad_Opcode },
    /* a8 */
    { Bad_Opcode },
    { Bad_Opcode },
    { Bad_Opcode },
    { Bad_Opcode },
    { Bad_Opcode },
    { Bad_Opcode },
    { REG_TABLE (REG_VEX_0FAE) },
    { Bad_Opcode },
    /* b0 */
    { Bad_Opcode },
    { Bad_Opcode },
    { Bad_Opcode },
    { Bad_Opcode },
    { Bad_Opcode },
    { Bad_Opcode },
    { Bad_Opcode },
    { Bad_Opcode },
    /* b8 */
    { Bad_Opcode },
    { Bad_Opcode },
    { Bad_Opcode },
    { Bad_Opcode },
    { Bad_Opcode },
    { Bad_Opcode },
    { Bad_Opcode },
    { Bad_Opcode },
    /* c0 */
    { Bad_Opcode },
    { Bad_Opcode },
    { PREFIX_TABLE (PREFIX_VEX_0FC2) },
    { Bad_Opcode },
    { PREFIX_TABLE (PREFIX_VEX_0FC4) },
    { PREFIX_TABLE (PREFIX_VEX_0FC5) },
    { "vshufpX",	{ XM, Vex, EXx, Ib } },
    { Bad_Opcode },
    /* c8 */
    { Bad_Opcode },
    { Bad_Opcode },
    { Bad_Opcode },
    { Bad_Opcode },
    { Bad_Opcode },
    { Bad_Opcode },
    { Bad_Opcode },
    { Bad_Opcode },
    /* d0 */
    { PREFIX_TABLE (PREFIX_VEX_0FD0) },
    { PREFIX_TABLE (PREFIX_VEX_0FD1) },
    { PREFIX_TABLE (PREFIX_VEX_0FD2) },
    { PREFIX_TABLE (PREFIX_VEX_0FD3) },
    { PREFIX_TABLE (PREFIX_VEX_0FD4) },
    { PREFIX_TABLE (PREFIX_VEX_0FD5) },
    { PREFIX_TABLE (PREFIX_VEX_0FD6) },
    { PREFIX_TABLE (PREFIX_VEX_0FD7) },
    /* d8 */
    { PREFIX_TABLE (PREFIX_VEX_0FD8) },
    { PREFIX_TABLE (PREFIX_VEX_0FD9) },
    { PREFIX_TABLE (PREFIX_VEX_0FDA) },
    { PREFIX_TABLE (PREFIX_VEX_0FDB) },
    { PREFIX_TABLE (PREFIX_VEX_0FDC) },
    { PREFIX_TABLE (PREFIX_VEX_0FDD) },
    { PREFIX_TABLE (PREFIX_VEX_0FDE) },
    { PREFIX_TABLE (PREFIX_VEX_0FDF) },
    /* e0 */
    { PREFIX_TABLE (PREFIX_VEX_0FE0) },
    { PREFIX_TABLE (PREFIX_VEX_0FE1) },
    { PREFIX_TABLE (PREFIX_VEX_0FE2) },
    { PREFIX_TABLE (PREFIX_VEX_0FE3) },
    { PREFIX_TABLE (PREFIX_VEX_0FE4) },
    { PREFIX_TABLE (PREFIX_VEX_0FE5) },
    { PREFIX_TABLE (PREFIX_VEX_0FE6) },
    { PREFIX_TABLE (PREFIX_VEX_0FE7) },
    /* e8 */
    { PREFIX_TABLE (PREFIX_VEX_0FE8) },
    { PREFIX_TABLE (PREFIX_VEX_0FE9) },
    { PREFIX_TABLE (PREFIX_VEX_0FEA) },
    { PREFIX_TABLE (PREFIX_VEX_0FEB) },
    { PREFIX_TABLE (PREFIX_VEX_0FEC) },
    { PREFIX_TABLE (PREFIX_VEX_0FED) },
    { PREFIX_TABLE (PREFIX_VEX_0FEE) },
    { PREFIX_TABLE (PREFIX_VEX_0FEF) },
    /* f0 */
    { PREFIX_TABLE (PREFIX_VEX_0FF0) },
    { PREFIX_TABLE (PREFIX_VEX_0FF1) },
    { PREFIX_TABLE (PREFIX_VEX_0FF2) },
    { PREFIX_TABLE (PREFIX_VEX_0FF3) },
    { PREFIX_TABLE (PREFIX_VEX_0FF4) },
    { PREFIX_TABLE (PREFIX_VEX_0FF5) },
    { PREFIX_TABLE (PREFIX_VEX_0FF6) },
    { PREFIX_TABLE (PREFIX_VEX_0FF7) },
    /* f8 */
    { PREFIX_TABLE (PREFIX_VEX_0FF8) },
    { PREFIX_TABLE (PREFIX_VEX_0FF9) },
    { PREFIX_TABLE (PREFIX_VEX_0FFA) },
    { PREFIX_TABLE (PREFIX_VEX_0FFB) },
    { PREFIX_TABLE (PREFIX_VEX_0FFC) },
    { PREFIX_TABLE (PREFIX_VEX_0FFD) },
    { PREFIX_TABLE (PREFIX_VEX_0FFE) },
    { Bad_Opcode },
  },
  /* VEX_0F38 */
  {
    /* 00 */
    { PREFIX_TABLE (PREFIX_VEX_0F3800) },
    { PREFIX_TABLE (PREFIX_VEX_0F3801) },
    { PREFIX_TABLE (PREFIX_VEX_0F3802) },
    { PREFIX_TABLE (PREFIX_VEX_0F3803) },
    { PREFIX_TABLE (PREFIX_VEX_0F3804) },
    { PREFIX_TABLE (PREFIX_VEX_0F3805) },
    { PREFIX_TABLE (PREFIX_VEX_0F3806) },
    { PREFIX_TABLE (PREFIX_VEX_0F3807) },
    /* 08 */
    { PREFIX_TABLE (PREFIX_VEX_0F3808) },
    { PREFIX_TABLE (PREFIX_VEX_0F3809) },
    { PREFIX_TABLE (PREFIX_VEX_0F380A) },
    { PREFIX_TABLE (PREFIX_VEX_0F380B) },
    { PREFIX_TABLE (PREFIX_VEX_0F380C) },
    { PREFIX_TABLE (PREFIX_VEX_0F380D) },
    { PREFIX_TABLE (PREFIX_VEX_0F380E) },
    { PREFIX_TABLE (PREFIX_VEX_0F380F) },
    /* 10 */
    { Bad_Opcode },
    { Bad_Opcode },
    { Bad_Opcode },
    { PREFIX_TABLE (PREFIX_VEX_0F3813) },
    { Bad_Opcode },
    { Bad_Opcode },
    { Bad_Opcode },
    { PREFIX_TABLE (PREFIX_VEX_0F3817) },
    /* 18 */
    { PREFIX_TABLE (PREFIX_VEX_0F3818) },
    { PREFIX_TABLE (PREFIX_VEX_0F3819) },
    { PREFIX_TABLE (PREFIX_VEX_0F381A) },
    { Bad_Opcode },
    { PREFIX_TABLE (PREFIX_VEX_0F381C) },
    { PREFIX_TABLE (PREFIX_VEX_0F381D) },
    { PREFIX_TABLE (PREFIX_VEX_0F381E) },
    { Bad_Opcode },
    /* 20 */
    { PREFIX_TABLE (PREFIX_VEX_0F3820) },
    { PREFIX_TABLE (PREFIX_VEX_0F3821) },
    { PREFIX_TABLE (PREFIX_VEX_0F3822) },
    { PREFIX_TABLE (PREFIX_VEX_0F3823) },
    { PREFIX_TABLE (PREFIX_VEX_0F3824) },
    { PREFIX_TABLE (PREFIX_VEX_0F3825) },
    { Bad_Opcode },
    { Bad_Opcode },
    /* 28 */
    { PREFIX_TABLE (PREFIX_VEX_0F3828) },
    { PREFIX_TABLE (PREFIX_VEX_0F3829) },
    { PREFIX_TABLE (PREFIX_VEX_0F382A) },
    { PREFIX_TABLE (PREFIX_VEX_0F382B) },
    { PREFIX_TABLE (PREFIX_VEX_0F382C) },
    { PREFIX_TABLE (PREFIX_VEX_0F382D) },
    { PREFIX_TABLE (PREFIX_VEX_0F382E) },
    { PREFIX_TABLE (PREFIX_VEX_0F382F) },
    /* 30 */
    { PREFIX_TABLE (PREFIX_VEX_0F3830) },
    { PREFIX_TABLE (PREFIX_VEX_0F3831) },
    { PREFIX_TABLE (PREFIX_VEX_0F3832) },
    { PREFIX_TABLE (PREFIX_VEX_0F3833) },
    { PREFIX_TABLE (PREFIX_VEX_0F3834) },
    { PREFIX_TABLE (PREFIX_VEX_0F3835) },
    { Bad_Opcode },
    { PREFIX_TABLE (PREFIX_VEX_0F3837) },
    /* 38 */
    { PREFIX_TABLE (PREFIX_VEX_0F3838) },
    { PREFIX_TABLE (PREFIX_VEX_0F3839) },
    { PREFIX_TABLE (PREFIX_VEX_0F383A) },
    { PREFIX_TABLE (PREFIX_VEX_0F383B) },
    { PREFIX_TABLE (PREFIX_VEX_0F383C) },
    { PREFIX_TABLE (PREFIX_VEX_0F383D) },
    { PREFIX_TABLE (PREFIX_VEX_0F383E) },
    { PREFIX_TABLE (PREFIX_VEX_0F383F) },
    /* 40 */
    { PREFIX_TABLE (PREFIX_VEX_0F3840) },
    { PREFIX_TABLE (PREFIX_VEX_0F3841) },
    { Bad_Opcode },
    { Bad_Opcode },
    { Bad_Opcode },
    { Bad_Opcode },
    { Bad_Opcode },
    { Bad_Opcode },
    /* 48 */
    { Bad_Opcode },
    { Bad_Opcode },
    { Bad_Opcode },
    { Bad_Opcode },
    { Bad_Opcode },
    { Bad_Opcode },
    { Bad_Opcode },
    { Bad_Opcode },
    /* 50 */
    { Bad_Opcode },
    { Bad_Opcode },
    { Bad_Opcode },
    { Bad_Opcode },
    { Bad_Opcode },
    { Bad_Opcode },
    { Bad_Opcode },
    { Bad_Opcode },
    /* 58 */
    { Bad_Opcode },
    { Bad_Opcode },
    { Bad_Opcode },
    { Bad_Opcode },
    { Bad_Opcode },
    { Bad_Opcode },
    { Bad_Opcode },
    { Bad_Opcode },
    /* 60 */
    { Bad_Opcode },
    { Bad_Opcode },
    { Bad_Opcode },
    { Bad_Opcode },
    { Bad_Opcode },
    { Bad_Opcode },
    { Bad_Opcode },
    { Bad_Opcode },
    /* 68 */
    { Bad_Opcode },
    { Bad_Opcode },
    { Bad_Opcode },
    { Bad_Opcode },
    { Bad_Opcode },
    { Bad_Opcode },
    { Bad_Opcode },
    { Bad_Opcode },
    /* 70 */
    { Bad_Opcode },
    { Bad_Opcode },
    { Bad_Opcode },
    { Bad_Opcode },
    { Bad_Opcode },
    { Bad_Opcode },
    { Bad_Opcode },
    { Bad_Opcode },
    /* 78 */
    { Bad_Opcode },
    { Bad_Opcode },
    { Bad_Opcode },
    { Bad_Opcode },
    { Bad_Opcode },
    { Bad_Opcode },
    { Bad_Opcode },
    { Bad_Opcode },
    /* 80 */
    { Bad_Opcode },
    { Bad_Opcode },
    { Bad_Opcode },
    { Bad_Opcode },
    { Bad_Opcode },
    { Bad_Opcode },
    { Bad_Opcode },
    { Bad_Opcode },
    /* 88 */
    { Bad_Opcode },
    { Bad_Opcode },
    { Bad_Opcode },
    { Bad_Opcode },
    { Bad_Opcode },
    { Bad_Opcode },
    { Bad_Opcode },
    { Bad_Opcode },
    /* 90 */
    { Bad_Opcode },
    { Bad_Opcode },
    { Bad_Opcode },
    { Bad_Opcode },
    { Bad_Opcode },
    { Bad_Opcode },
    { PREFIX_TABLE (PREFIX_VEX_0F3896) },
    { PREFIX_TABLE (PREFIX_VEX_0F3897) },
    /* 98 */
    { PREFIX_TABLE (PREFIX_VEX_0F3898) },
    { PREFIX_TABLE (PREFIX_VEX_0F3899) },
    { PREFIX_TABLE (PREFIX_VEX_0F389A) },
    { PREFIX_TABLE (PREFIX_VEX_0F389B) },
    { PREFIX_TABLE (PREFIX_VEX_0F389C) },
    { PREFIX_TABLE (PREFIX_VEX_0F389D) },
    { PREFIX_TABLE (PREFIX_VEX_0F389E) },
    { PREFIX_TABLE (PREFIX_VEX_0F389F) },
    /* a0 */
    { Bad_Opcode },
    { Bad_Opcode },
    { Bad_Opcode },
    { Bad_Opcode },
    { Bad_Opcode },
    { Bad_Opcode },
    { PREFIX_TABLE (PREFIX_VEX_0F38A6) },
    { PREFIX_TABLE (PREFIX_VEX_0F38A7) },
    /* a8 */
    { PREFIX_TABLE (PREFIX_VEX_0F38A8) },
    { PREFIX_TABLE (PREFIX_VEX_0F38A9) },
    { PREFIX_TABLE (PREFIX_VEX_0F38AA) },
    { PREFIX_TABLE (PREFIX_VEX_0F38AB) },
    { PREFIX_TABLE (PREFIX_VEX_0F38AC) },
    { PREFIX_TABLE (PREFIX_VEX_0F38AD) },
    { PREFIX_TABLE (PREFIX_VEX_0F38AE) },
    { PREFIX_TABLE (PREFIX_VEX_0F38AF) },
    /* b0 */
    { Bad_Opcode },
    { Bad_Opcode },
    { Bad_Opcode },
    { Bad_Opcode },
    { Bad_Opcode },
    { Bad_Opcode },
    { PREFIX_TABLE (PREFIX_VEX_0F38B6) },
    { PREFIX_TABLE (PREFIX_VEX_0F38B7) },
    /* b8 */
    { PREFIX_TABLE (PREFIX_VEX_0F38B8) },
    { PREFIX_TABLE (PREFIX_VEX_0F38B9) },
    { PREFIX_TABLE (PREFIX_VEX_0F38BA) },
    { PREFIX_TABLE (PREFIX_VEX_0F38BB) },
    { PREFIX_TABLE (PREFIX_VEX_0F38BC) },
    { PREFIX_TABLE (PREFIX_VEX_0F38BD) },
    { PREFIX_TABLE (PREFIX_VEX_0F38BE) },
    { PREFIX_TABLE (PREFIX_VEX_0F38BF) },
    /* c0 */
    { Bad_Opcode },
    { Bad_Opcode },
    { Bad_Opcode },
    { Bad_Opcode },
    { Bad_Opcode },
    { Bad_Opcode },
    { Bad_Opcode },
    { Bad_Opcode },
    /* c8 */
    { Bad_Opcode },
    { Bad_Opcode },
    { Bad_Opcode },
    { Bad_Opcode },
    { Bad_Opcode },
    { Bad_Opcode },
    { Bad_Opcode },
    { Bad_Opcode },
    /* d0 */
    { Bad_Opcode },
    { Bad_Opcode },
    { Bad_Opcode },
    { Bad_Opcode },
    { Bad_Opcode },
    { Bad_Opcode },
    { Bad_Opcode },
    { Bad_Opcode },
    /* d8 */
    { Bad_Opcode },
    { Bad_Opcode },
    { Bad_Opcode },
    { PREFIX_TABLE (PREFIX_VEX_0F38DB) },
    { PREFIX_TABLE (PREFIX_VEX_0F38DC) },
    { PREFIX_TABLE (PREFIX_VEX_0F38DD) },
    { PREFIX_TABLE (PREFIX_VEX_0F38DE) },
    { PREFIX_TABLE (PREFIX_VEX_0F38DF) },
    /* e0 */
    { Bad_Opcode },
    { Bad_Opcode },
    { Bad_Opcode },
    { Bad_Opcode },
    { Bad_Opcode },
    { Bad_Opcode },
    { Bad_Opcode },
    { Bad_Opcode },
    /* e8 */
    { Bad_Opcode },
    { Bad_Opcode },
    { Bad_Opcode },
    { Bad_Opcode },
    { Bad_Opcode },
    { Bad_Opcode },
    { Bad_Opcode },
    { Bad_Opcode },
    /* f0 */
    { Bad_Opcode },
    { Bad_Opcode },
    { Bad_Opcode },
    { Bad_Opcode },
    { Bad_Opcode },
    { Bad_Opcode },
    { Bad_Opcode },
    { Bad_Opcode },
    /* f8 */
    { Bad_Opcode },
    { Bad_Opcode },
    { Bad_Opcode },
    { Bad_Opcode },
    { Bad_Opcode },
    { Bad_Opcode },
    { Bad_Opcode },
    { Bad_Opcode },
  },
  /* VEX_0F3A */
  {
    /* 00 */
    { Bad_Opcode },
    { Bad_Opcode },
    { Bad_Opcode },
    { Bad_Opcode },
    { PREFIX_TABLE (PREFIX_VEX_0F3A04) },
    { PREFIX_TABLE (PREFIX_VEX_0F3A05) },
    { PREFIX_TABLE (PREFIX_VEX_0F3A06) },
    { Bad_Opcode },
    /* 08 */
    { PREFIX_TABLE (PREFIX_VEX_0F3A08) },
    { PREFIX_TABLE (PREFIX_VEX_0F3A09) },
    { PREFIX_TABLE (PREFIX_VEX_0F3A0A) },
    { PREFIX_TABLE (PREFIX_VEX_0F3A0B) },
    { PREFIX_TABLE (PREFIX_VEX_0F3A0C) },
    { PREFIX_TABLE (PREFIX_VEX_0F3A0D) },
    { PREFIX_TABLE (PREFIX_VEX_0F3A0E) },
    { PREFIX_TABLE (PREFIX_VEX_0F3A0F) },
    /* 10 */
    { Bad_Opcode },
    { Bad_Opcode },
    { Bad_Opcode },
    { Bad_Opcode },
    { PREFIX_TABLE (PREFIX_VEX_0F3A14) },
    { PREFIX_TABLE (PREFIX_VEX_0F3A15) },
    { PREFIX_TABLE (PREFIX_VEX_0F3A16) },
    { PREFIX_TABLE (PREFIX_VEX_0F3A17) },
    /* 18 */
    { PREFIX_TABLE (PREFIX_VEX_0F3A18) },
    { PREFIX_TABLE (PREFIX_VEX_0F3A19) },
    { Bad_Opcode },
    { Bad_Opcode },
    { Bad_Opcode },
    { PREFIX_TABLE (PREFIX_VEX_0F3A1D) },
    { Bad_Opcode },
    { Bad_Opcode },
    /* 20 */
    { PREFIX_TABLE (PREFIX_VEX_0F3A20) },
    { PREFIX_TABLE (PREFIX_VEX_0F3A21) },
    { PREFIX_TABLE (PREFIX_VEX_0F3A22) },
    { Bad_Opcode },
    { Bad_Opcode },
    { Bad_Opcode },
    { Bad_Opcode },
    { Bad_Opcode },
    /* 28 */
    { Bad_Opcode },
    { Bad_Opcode },
    { Bad_Opcode },
    { Bad_Opcode },
    { Bad_Opcode },
    { Bad_Opcode },
    { Bad_Opcode },
    { Bad_Opcode },
    /* 30 */
    { Bad_Opcode },
    { Bad_Opcode },
    { Bad_Opcode },
    { Bad_Opcode },
    { Bad_Opcode },
    { Bad_Opcode },
    { Bad_Opcode },
    { Bad_Opcode },
    /* 38 */
    { Bad_Opcode },
    { Bad_Opcode },
    { Bad_Opcode },
    { Bad_Opcode },
    { Bad_Opcode },
    { Bad_Opcode },
    { Bad_Opcode },
    { Bad_Opcode },
    /* 40 */
    { PREFIX_TABLE (PREFIX_VEX_0F3A40) },
    { PREFIX_TABLE (PREFIX_VEX_0F3A41) },
    { PREFIX_TABLE (PREFIX_VEX_0F3A42) },
    { Bad_Opcode },
    { PREFIX_TABLE (PREFIX_VEX_0F3A44) },
    { Bad_Opcode },
    { Bad_Opcode },
    { Bad_Opcode },
    /* 48 */
    { PREFIX_TABLE (PREFIX_VEX_0F3A48) },
    { PREFIX_TABLE (PREFIX_VEX_0F3A49) },
    { PREFIX_TABLE (PREFIX_VEX_0F3A4A) },
    { PREFIX_TABLE (PREFIX_VEX_0F3A4B) },
    { PREFIX_TABLE (PREFIX_VEX_0F3A4C) },
    { Bad_Opcode },
    { Bad_Opcode },
    { Bad_Opcode },
    /* 50 */
    { Bad_Opcode },
    { Bad_Opcode },
    { Bad_Opcode },
    { Bad_Opcode },
    { Bad_Opcode },
    { Bad_Opcode },
    { Bad_Opcode },
    { Bad_Opcode },
    /* 58 */
    { Bad_Opcode },
    { Bad_Opcode },
    { Bad_Opcode },
    { Bad_Opcode },
    { PREFIX_TABLE (PREFIX_VEX_0F3A5C) },
    { PREFIX_TABLE (PREFIX_VEX_0F3A5D) },
    { PREFIX_TABLE (PREFIX_VEX_0F3A5E) },
    { PREFIX_TABLE (PREFIX_VEX_0F3A5F) },
    /* 60 */
    { PREFIX_TABLE (PREFIX_VEX_0F3A60) },
    { PREFIX_TABLE (PREFIX_VEX_0F3A61) },
    { PREFIX_TABLE (PREFIX_VEX_0F3A62) },
    { PREFIX_TABLE (PREFIX_VEX_0F3A63) },
    { Bad_Opcode },
    { Bad_Opcode },
    { Bad_Opcode },
    { Bad_Opcode },
    /* 68 */
    { PREFIX_TABLE (PREFIX_VEX_0F3A68) },
    { PREFIX_TABLE (PREFIX_VEX_0F3A69) },
    { PREFIX_TABLE (PREFIX_VEX_0F3A6A) },
    { PREFIX_TABLE (PREFIX_VEX_0F3A6B) },
    { PREFIX_TABLE (PREFIX_VEX_0F3A6C) },
    { PREFIX_TABLE (PREFIX_VEX_0F3A6D) },
    { PREFIX_TABLE (PREFIX_VEX_0F3A6E) },
    { PREFIX_TABLE (PREFIX_VEX_0F3A6F) },
    /* 70 */
    { Bad_Opcode },
    { Bad_Opcode },
    { Bad_Opcode },
    { Bad_Opcode },
    { Bad_Opcode },
    { Bad_Opcode },
    { Bad_Opcode },
    { Bad_Opcode },
    /* 78 */
    { PREFIX_TABLE (PREFIX_VEX_0F3A78) },
    { PREFIX_TABLE (PREFIX_VEX_0F3A79) },
    { PREFIX_TABLE (PREFIX_VEX_0F3A7A) },
    { PREFIX_TABLE (PREFIX_VEX_0F3A7B) },
    { PREFIX_TABLE (PREFIX_VEX_0F3A7C) },
    { PREFIX_TABLE (PREFIX_VEX_0F3A7D) },
    { PREFIX_TABLE (PREFIX_VEX_0F3A7E) },
    { PREFIX_TABLE (PREFIX_VEX_0F3A7F) },
    /* 80 */
    { Bad_Opcode },
    { Bad_Opcode },
    { Bad_Opcode },
    { Bad_Opcode },
    { Bad_Opcode },
    { Bad_Opcode },
    { Bad_Opcode },
    { Bad_Opcode },
    /* 88 */
    { Bad_Opcode },
    { Bad_Opcode },
    { Bad_Opcode },
    { Bad_Opcode },
    { Bad_Opcode },
    { Bad_Opcode },
    { Bad_Opcode },
    { Bad_Opcode },
    /* 90 */
    { Bad_Opcode },
    { Bad_Opcode },
    { Bad_Opcode },
    { Bad_Opcode },
    { Bad_Opcode },
    { Bad_Opcode },
    { Bad_Opcode },
    { Bad_Opcode },
    /* 98 */
    { Bad_Opcode },
    { Bad_Opcode },
    { Bad_Opcode },
    { Bad_Opcode },
    { Bad_Opcode },
    { Bad_Opcode },
    { Bad_Opcode },
    { Bad_Opcode },
    /* a0 */
    { Bad_Opcode },
    { Bad_Opcode },
    { Bad_Opcode },
    { Bad_Opcode },
    { Bad_Opcode },
    { Bad_Opcode },
    { Bad_Opcode },
    { Bad_Opcode },
    /* a8 */
    { Bad_Opcode },
    { Bad_Opcode },
    { Bad_Opcode },
    { Bad_Opcode },
    { Bad_Opcode },
    { Bad_Opcode },
    { Bad_Opcode },
    { Bad_Opcode },
    /* b0 */
    { Bad_Opcode },
    { Bad_Opcode },
    { Bad_Opcode },
    { Bad_Opcode },
    { Bad_Opcode },
    { Bad_Opcode },
    { Bad_Opcode },
    { Bad_Opcode },
    /* b8 */
    { Bad_Opcode },
    { Bad_Opcode },
    { Bad_Opcode },
    { Bad_Opcode },
    { Bad_Opcode },
    { Bad_Opcode },
    { Bad_Opcode },
    { Bad_Opcode },
    /* c0 */
    { Bad_Opcode },
    { Bad_Opcode },
    { Bad_Opcode },
    { Bad_Opcode },
    { Bad_Opcode },
    { Bad_Opcode },
    { Bad_Opcode },
    { Bad_Opcode },
    /* c8 */
    { Bad_Opcode },
    { Bad_Opcode },
    { Bad_Opcode },
    { Bad_Opcode },
    { Bad_Opcode },
    { Bad_Opcode },
    { Bad_Opcode },
    { Bad_Opcode },
    /* d0 */
    { Bad_Opcode },
    { Bad_Opcode },
    { Bad_Opcode },
    { Bad_Opcode },
    { Bad_Opcode },
    { Bad_Opcode },
    { Bad_Opcode },
    { Bad_Opcode },
    /* d8 */
    { Bad_Opcode },
    { Bad_Opcode },
    { Bad_Opcode },
    { Bad_Opcode },
    { Bad_Opcode },
    { Bad_Opcode },
    { Bad_Opcode },
    { PREFIX_TABLE (PREFIX_VEX_0F3ADF) },
    /* e0 */
    { Bad_Opcode },
    { Bad_Opcode },
    { Bad_Opcode },
    { Bad_Opcode },
    { Bad_Opcode },
    { Bad_Opcode },
    { Bad_Opcode },
    { Bad_Opcode },
    /* e8 */
    { Bad_Opcode },
    { Bad_Opcode },
    { Bad_Opcode },
    { Bad_Opcode },
    { Bad_Opcode },
    { Bad_Opcode },
    { Bad_Opcode },
    { Bad_Opcode },
    /* f0 */
    { Bad_Opcode },
    { Bad_Opcode },
    { Bad_Opcode },
    { Bad_Opcode },
    { Bad_Opcode },
    { Bad_Opcode },
    { Bad_Opcode },
    { Bad_Opcode },
    /* f8 */
    { Bad_Opcode },
    { Bad_Opcode },
    { Bad_Opcode },
    { Bad_Opcode },
    { Bad_Opcode },
    { Bad_Opcode },
    { Bad_Opcode },
    { Bad_Opcode },
  },
};
