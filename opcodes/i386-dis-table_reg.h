const struct dis386 reg_table[][8] = {
  /* REG_80 */
  {
    { "addA",	{ Eb, Ib } },
    { "orA",	{ Eb, Ib } },
    { "adcA",	{ Eb, Ib } },
    { "sbbA",	{ Eb, Ib } },
    { "andA",	{ Eb, Ib } },
    { "subA",	{ Eb, Ib } },
    { "xorA",	{ Eb, Ib } },
    { "cmpA",	{ Eb, Ib } },
  },
  /* REG_81 */
  {
    { "addQ",	{ Ev, Iv } },
    { "orQ",	{ Ev, Iv } },
    { "adcQ",	{ Ev, Iv } },
    { "sbbQ",	{ Ev, Iv } },
    { "andQ",	{ Ev, Iv } },
    { "subQ",	{ Ev, Iv } },
    { "xorQ",	{ Ev, Iv } },
    { "cmpQ",	{ Ev, Iv } },
  },
  /* REG_82 */
  {
    { "addQ",	{ Ev, sIb } },
    { "orQ",	{ Ev, sIb } },
    { "adcQ",	{ Ev, sIb } },
    { "sbbQ",	{ Ev, sIb } },
    { "andQ",	{ Ev, sIb } },
    { "subQ",	{ Ev, sIb } },
    { "xorQ",	{ Ev, sIb } },
    { "cmpQ",	{ Ev, sIb } },
  },
  /* REG_8F */
  {
    { "popU",	{ stackEv } },
    { XOP_8F_TABLE (XOP_09) },
    { Bad_Opcode },
    { Bad_Opcode },
    { Bad_Opcode },
    { XOP_8F_TABLE (XOP_09) },
  },
  /* REG_C0 */
  {
    { "rolA",	{ Eb, Ib } },
    { "rorA",	{ Eb, Ib } },
    { "rclA",	{ Eb, Ib } },
    { "rcrA",	{ Eb, Ib } },
    { "shlA",	{ Eb, Ib } },
    { "shrA",	{ Eb, Ib } },
    { Bad_Opcode },
    { "sarA",	{ Eb, Ib } },
  },
  /* REG_C1 */
  {
    { "rolQ",	{ Ev, Ib } },
    { "rorQ",	{ Ev, Ib } },
    { "rclQ",	{ Ev, Ib } },
    { "rcrQ",	{ Ev, Ib } },
    { "shlQ",	{ Ev, Ib } },
    { "shrQ",	{ Ev, Ib } },
    { Bad_Opcode },
    { "sarQ",	{ Ev, Ib } },
  },
  /* REG_C6 */
  {
    { "movA",	{ Eb, Ib } },
  },
  /* REG_C7 */
  {
    { "movQ",	{ Ev, Iv } },
  },
  /* REG_D0 */
  {
    { "rolA",	{ Eb, I1 } },
    { "rorA",	{ Eb, I1 } },
    { "rclA",	{ Eb, I1 } },
    { "rcrA",	{ Eb, I1 } },
    { "shlA",	{ Eb, I1 } },
    { "shrA",	{ Eb, I1 } },
    { Bad_Opcode },
    { "sarA",	{ Eb, I1 } },
  },
  /* REG_D1 */
  {
    { "rolQ",	{ Ev, I1 } },
    { "rorQ",	{ Ev, I1 } },
    { "rclQ",	{ Ev, I1 } },
    { "rcrQ",	{ Ev, I1 } },
    { "shlQ",	{ Ev, I1 } },
    { "shrQ",	{ Ev, I1 } },
    { Bad_Opcode },
    { "sarQ",	{ Ev, I1 } },
  },
  /* REG_D2 */
  {
    { "rolA",	{ Eb, CL } },
    { "rorA",	{ Eb, CL } },
    { "rclA",	{ Eb, CL } },
    { "rcrA",	{ Eb, CL } },
    { "shlA",	{ Eb, CL } },
    { "shrA",	{ Eb, CL } },
    { Bad_Opcode },
    { "sarA",	{ Eb, CL } },
  },
  /* REG_D3 */
  {
    { "rolQ",	{ Ev, CL } },
    { "rorQ",	{ Ev, CL } },
    { "rclQ",	{ Ev, CL } },
    { "rcrQ",	{ Ev, CL } },
    { "shlQ",	{ Ev, CL } },
    { "shrQ",	{ Ev, CL } },
    { Bad_Opcode },
    { "sarQ",	{ Ev, CL } },
  },
  /* REG_F6 */
  {
    { "testA",	{ Eb, Ib } },
    { Bad_Opcode },
    { "notA",	{ Eb } },
    { "negA",	{ Eb } },
    { "mulA",	{ Eb } },	/* Don't print the implicit %al register,  */
    { "imulA",	{ Eb } },	/* to distinguish these opcodes from other */
    { "divA",	{ Eb } },	/* mul/imul opcodes.  Do the same for div  */
    { "idivA",	{ Eb } },	/* and idiv for consistency.		   */
  },
  /* REG_F7 */
  {
    { "testQ",	{ Ev, Iv } },
    { Bad_Opcode },
    { "notQ",	{ Ev } },
    { "negQ",	{ Ev } },
    { "mulQ",	{ Ev } },	/* Don't print the implicit register.  */
    { "imulQ",	{ Ev } },
    { "divQ",	{ Ev } },
    { "idivQ",	{ Ev } },
  },
  /* REG_FE */
  {
    { "incA",	{ Eb } },
    { "decA",	{ Eb } },
  },
  /* REG_FF */
  {
    { "incQ",	{ Ev } },
    { "decQ",	{ Ev } },
    { "call{T|}", { indirEv } },
    { "Jcall{T|}", { indirEp } },
    { "jmp{T|}", { indirEv } },
    { "Jjmp{T|}", { indirEp } },
    { "pushU",	{ stackEv } },
    { Bad_Opcode },
  },
  /* REG_0F00 */
  {
    { "sldtD",	{ Sv } },
    { "strD",	{ Sv } },
    { "lldt",	{ Ew } },
    { "ltr",	{ Ew } },
    { "verr",	{ Ew } },
    { "verw",	{ Ew } },
    { Bad_Opcode },
    { Bad_Opcode },
  },
  /* REG_0F01 */
  {
    { MOD_TABLE (MOD_0F01_REG_0) },
    { MOD_TABLE (MOD_0F01_REG_1) },
    { MOD_TABLE (MOD_0F01_REG_2) },
    { MOD_TABLE (MOD_0F01_REG_3) },
    { "smswD",	{ Sv } },
    { Bad_Opcode },
    { "lmsw",	{ Ew } },
    { MOD_TABLE (MOD_0F01_REG_7) },
  },
  /* REG_0F0D */
  {
    { "prefetch",	{ Mb } },
    { "prefetchw",	{ Mb } },
  },
  /* REG_0F18 */
  {
    { MOD_TABLE (MOD_0F18_REG_0) },
    { MOD_TABLE (MOD_0F18_REG_1) },
    { MOD_TABLE (MOD_0F18_REG_2) },
    { MOD_TABLE (MOD_0F18_REG_3) },
  },
  /* REG_0F71 */
  {
    { Bad_Opcode },
    { Bad_Opcode },
    { MOD_TABLE (MOD_0F71_REG_2) },
    { Bad_Opcode },
    { MOD_TABLE (MOD_0F71_REG_4) },
    { Bad_Opcode },
    { MOD_TABLE (MOD_0F71_REG_6) },
  },
  /* REG_0F72 */
  {
    { Bad_Opcode },
    { Bad_Opcode },
    { MOD_TABLE (MOD_0F72_REG_2) },
    { Bad_Opcode },
    { MOD_TABLE (MOD_0F72_REG_4) },
    { Bad_Opcode },
    { MOD_TABLE (MOD_0F72_REG_6) },
  },
  /* REG_0F73 */
  {
    { Bad_Opcode },
    { Bad_Opcode },
    { MOD_TABLE (MOD_0F73_REG_2) },
    { MOD_TABLE (MOD_0F73_REG_3) },
    { Bad_Opcode },
    { Bad_Opcode },
    { MOD_TABLE (MOD_0F73_REG_6) },
    { MOD_TABLE (MOD_0F73_REG_7) },
  },
  /* REG_0FA6 */
  {
    { "montmul",	{ { OP_0f07, 0 } } },
    { "xsha1",		{ { OP_0f07, 0 } } },
    { "xsha256",	{ { OP_0f07, 0 } } },
  },
  /* REG_0FA7 */
  {
    { "xstore-rng",	{ { OP_0f07, 0 } } },
    { "xcrypt-ecb",	{ { OP_0f07, 0 } } },
    { "xcrypt-cbc",	{ { OP_0f07, 0 } } },
    { "xcrypt-ctr",	{ { OP_0f07, 0 } } },
    { "xcrypt-cfb",	{ { OP_0f07, 0 } } },
    { "xcrypt-ofb",	{ { OP_0f07, 0 } } },
  },
  /* REG_0FAE */
  {
    { MOD_TABLE (MOD_0FAE_REG_0) },
    { MOD_TABLE (MOD_0FAE_REG_1) },
    { MOD_TABLE (MOD_0FAE_REG_2) },
    { MOD_TABLE (MOD_0FAE_REG_3) },
    { MOD_TABLE (MOD_0FAE_REG_4) },
    { MOD_TABLE (MOD_0FAE_REG_5) },
    { MOD_TABLE (MOD_0FAE_REG_6) },
    { MOD_TABLE (MOD_0FAE_REG_7) },
  },
  /* REG_0FBA */
  {
    { Bad_Opcode },
    { Bad_Opcode },
    { Bad_Opcode },
    { Bad_Opcode },
    { "btQ",	{ Ev, Ib } },
    { "btsQ",	{ Ev, Ib } },
    { "btrQ",	{ Ev, Ib } },
    { "btcQ",	{ Ev, Ib } },
  },
  /* REG_0FC7 */
  {
    { Bad_Opcode },
    { "cmpxchg8b", { { CMPXCHG8B_Fixup, q_mode } } },
    { Bad_Opcode },
    { Bad_Opcode },
    { Bad_Opcode },
    { Bad_Opcode },
    { MOD_TABLE (MOD_0FC7_REG_6) },
    { MOD_TABLE (MOD_0FC7_REG_7) },
  },
  /* REG_VEX_0F71 */
  {
    { Bad_Opcode },
    { Bad_Opcode },
    { MOD_TABLE (MOD_VEX_0F71_REG_2) },
    { Bad_Opcode },
    { MOD_TABLE (MOD_VEX_0F71_REG_4) },
    { Bad_Opcode },
    { MOD_TABLE (MOD_VEX_0F71_REG_6) },
  },
  /* REG_VEX_0F72 */
  {
    { Bad_Opcode },
    { Bad_Opcode },
    { MOD_TABLE (MOD_VEX_0F72_REG_2) },
    { Bad_Opcode },
    { MOD_TABLE (MOD_VEX_0F72_REG_4) },
    { Bad_Opcode },
    { MOD_TABLE (MOD_VEX_0F72_REG_6) },
  },
  /* REG_VEX_0F73 */
  {
    { Bad_Opcode },
    { Bad_Opcode },
    { MOD_TABLE (MOD_VEX_0F73_REG_2) },
    { MOD_TABLE (MOD_VEX_0F73_REG_3) },
    { Bad_Opcode },
    { Bad_Opcode },
    { MOD_TABLE (MOD_VEX_0F73_REG_6) },
    { MOD_TABLE (MOD_VEX_0F73_REG_7) },
  },
  /* REG_VEX_0FAE */
  {
    { Bad_Opcode },
    { Bad_Opcode },
    { MOD_TABLE (MOD_VEX_0FAE_REG_2) },
    { MOD_TABLE (MOD_VEX_0FAE_REG_3) },
  },
  /* REG_XOP_LWPCB */
  {
    { "llwpcb", { { OP_LWPCB_E, 0 } } },
    { "slwpcb",	{ { OP_LWPCB_E, 0 } } },
  },
  /* REG_XOP_LWP */
  {
    { "lwpins", { { OP_LWP_E, 0 }, Ed, Iq } },
    { "lwpval",	{ { OP_LWP_E, 0 }, Ed, Iq } },
  },
};
